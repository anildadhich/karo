@include('control_panel.layouts.head')
<body data-sidebar="dark">
@include('control_panel.layouts.loder')
<div id="layout-wrapper">
    @include('control_panel.layouts.header')
    @include('control_panel.layouts.left_menu')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Manage Category</h4>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        @if(Session::has('Failed'))
                        <div class="alert alert-danger">
                            <strong>Failed ! </strong> {{Session::get('Failed')}}
                        </div>
                        @endif
                        <div class="card">
                            <div class="card-body">
                                <form class="custom-validation" action="{{url('admin_panel/category_save')}}" id="general_form" method="post" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="input[lCatgIdNo]" value="{{isset($oGetRec) ? base64_encode($oGetRec->lCatg_IdNo) : base64_encode(0)}}">
                                    <div class="row">
                                        <div class="col-lg-2"><label class="text-left control-label col-form-label">Title</label></div>
                                        <div class="form-group col-md-4">
                                            <input type="text" class="form-control" required placeholder="Title" name="input[sCatgName]" value="{{isset($oGetRec) ? $oGetRec->sCatg_Name : ''}}" onkeypress="return LenCheck(event, this.value, '50')"/>
                                        </div>
                                        <div class="col-lg-2"><label class="text-left control-label col-form-label">Icon</label></div>
                                        <div class="form-group col-md-4">
                                            <input type="file" {{ isset($oGetRec) ? '' : 'required'}} name="sIcnName">
                                        </div>
                                    </div>
                                    <div class="row" style="margin: 10px 5px 20px 5px;">
                                        @foreach($oGetTst as $aRec)
                                        <div class="custom-control custom-checkbox mb-2 col-lg-3">
                                            <input type="checkbox" class="custom-control-input"  id="customCheck{{$aRec->lTst_IdNo}}" name="sTstIds[]" value="{{$aRec->lTst_IdNo}}" {{ isset($oGetRec) ? in_array($aRec->lTst_IdNo, explode(",",$oGetRec->sTst_Ids)) ? 'checked=""' : '' : ''}}>
                                            <label class="custom-control-label" for="customCheck{{$aRec->lTst_IdNo}}">{{$aRec->sTst_Name}}</label>
                                        </div>
                                        @endforeach
                                    </div>
                                    <div class="form-group mb-0">
                                        <div>
                                            <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                                Submit
                                            </button>
                                            <button type="reset" class="btn btn-secondary waves-effect">
                                                Cancel
                                            </button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>
            </div>
        </div>
        @include('control_panel.layouts.footer')
    </div>
</div>
</body>
</html>
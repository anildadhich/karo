@include('control_panel.layouts.head')
<body>
<div id="preloader">
    @include('control_panel.layouts.loder')
</div>
<div class="account-pages my-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 col-lg-6 col-xl-5">
                <div class="login-error d-none"></div>
                <div class="card overflow-hidden">
                    <div class="bg-soft-primary">
                        <div class="row">
                            <div class="col-7">
                                <div class="text-primary p-4">
                                    <h5 class="text-primary">Welcome Karo !</h5>
                                    <p>Sign in to continue to Karo.</p>
                                </div>
                            </div>
                            <div class="col-5 align-self-end">
                                <img src="control_panel/assets/images/profile-img.png" alt="" class="img-fluid">
                            </div>
                        </div>
                    </div>
                    <div class="card-body pt-0"> 
                        <div class="p-2">
                            <form method="post" action="{{url('admin_panel/company/login')}}" class="form-horizontal" autocomplete="off" id="login_form">
                                <input name="csrf-token" type="hidden" value="{{ csrf_token() }}">
                                <div class="form-group">
                                    <label for="email" for="username">E-Mail Address</label>
                                    <input class="form-control required" placeholder="Enter Email Address" autofocus="autofocus" name="input[sCompEmail]" type="email" value="" autocomplete="off">
                                </div>

                                <div class="form-group">
                                    <label for="password" for="userpassword">Password</label>
                                    <input class="form-control required" placeholder="Enter Password" name="input[sCompPass]" type="password" value="" autocomplete="off">
                                </div>
                                <div class="mt-3">
                                    <button class="btn btn-primary btn-block waves-effect waves-light" type="submit">Login</button>
                                </div>
                                <div class="mt-4 text-center">
                                    <a href="control_panel/password/reset" class="text-muted"><i class="mdi mdi-lock mr-1"></i>Forgot Your Password?</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="mt-5 text-center">
                    <p>© <script>document.write(new Date().getFullYear())</script> karo.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="control_panel/assets/js/jquery-3.4.1.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $("#login_form").on("submit", function() {
        $(".required").removeClass("is-invalid");
        var required = $(this).find(".required").filter(function() {
          return this.value == '';
        });

        if(required.length > 0) 
        {
            required.addClass("is-invalid");
        } 
        else 
        {
            $('#loadingBox').removeClass('d-none');
            $('.login-error').addClass('d-none').removeClass('alert alert-danger');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('input[name="csrf-token"]').attr('value')
                },
                url:  $(this).attr("action"),
                type: "POST",
                data: $(this).serialize(),
                success: function(res) 
                {
                    response = JSON.parse(res);
                    if(response.Status) 
                    {
                        $('#loadingBox').addClass('d-none');
                        $('.login-error').removeClass('d-none alert-danger').addClass('alert alert-success').html(response.Message);
                        setTimeout(function(){
                            location.reload();
                        }, 2000);
                    } 
                    else 
                    {
                        $('#loadingBox').addClass('d-none');
                        $('.login-error').removeClass('d-none alert-success').addClass('alert alert-danger').html(response.Message);
                    }
                }
            });
        }
        return false;
    });
});
</script>
<script src="control_panel/assets/libs/bootstrap/bootstrap.min.js"></script>
<script src="control_panel/assets/libs/metismenu/metismenu.min.js"></script>
<script src="control_panel/assets/libs/simplebar/simplebar.min.js"></script>
<script src="control_panel/assets/libs/node-waves/node-waves.min.js"></script>
<script src="control_panel/assets/js/app.min.js"></script>  
</body>
</html>

<div class="loading_bg loader-block d-none" id="loadingBox">
    <div class="loading_popup">
        <div><img src="control_panel/assets/images/loder.gif" width="90"></div>
        <strong class="loading_text">Processing, Please wait....</strong>
    </div>
</div>
<div class="vertical-menu">
    <div data-simplebar class="h-100">
        <div id="sidebar-menu">
            <ul class="metismenu list-unstyled" id="side-menu">
                <li>
                    <a href="{{url('admin_panel')}}" class="waves-effect">
                        <i class="bx bx-home-circle"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="bx bx-layout"></i>
                        <span>Test Master</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{url('admin_panel/test')}}">Create Test</a></li>
                        <li><a href="{{url('admin_panel/test_list')}}">View / Alter Test</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="bx bx-layout"></i>
                        <span>Category Test Master</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="admin_panel/category">Create Category Test</a></li>
                        <li><a href="admin_panel/category_list">View / Alter Category Test</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="bx bx-layout"></i>
                        <span>Package Master</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="admin_panel/package">Create Package</a></li>
                        <li><a href="admin_panel/package_list">View / Alter Package</a></li>
                    </ul>
                </li>
                <li>
                    <a href="{{url('admin_panel/user')}}" class="waves-effect">
                        <i class="bx bxs-user-detail"></i>
                        <span>User Master</span>
                    </a>
                </li>
                <li>
                    <a href="{{url('admin_panel/perception')}}" class="waves-effect">
                        <i class="bx bxs-user-detail"></i>
                        <span>User Prescription</span>
                    </a>
                </li>
                <li>
                    <a href="{{url('admin_panel/feedback')}}" class="waves-effect">
                        <i class="bx bxs-user-detail"></i>
                        <span>User Feedback</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
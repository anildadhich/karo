<header id="page-topbar"><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <div class="navbar-header">
            <div class="d-flex">
                <div class="navbar-brand-box">
                    <a href="{{url('control_panel/home')}}" class="logo logo-dark">
                        <span class="logo-sm">
                            <img src="control_panel/assets/images/logo.svg" alt="" height="42">
                        </span>
                        <span class="logo-lg">
                            <img src="control_panel/assets/images/logo.svg" alt="" height="40">
                        </span>
                    </a>
                    <a href="{{url('control_panel/home')}}" class="logo logo-light">
                        <span class="logo-sm">
                            <img src="control_panel/assets/images/logo.svg" alt="" height="42">
                        </span>
                        <span class="logo-lg">
                            <img src="control_panel/assets/images/logo.svg" alt="" height="40">
                        </span>
                    </a>
                </div>
                <button type="button" class="btn btn-sm px-3 font-size-16 header-item waves-effect" id="vertical-menu-btn">
                    <i class="fa fa-fw fa-bars"></i>
                </button>
            </div>
            <div class="d-flex">
                <div class="dropdown d-inline-block">
                    <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                         <img class="rounded-circle header-profile-user" src="{{url('/public/profile_pic/default.png')}}" alt="Karo">
                        <span class="d-none d-xl-inline-block ml-1">Karo</span>
                        <i class="mdi mdi-chevron-down d-none d-xl-inline-block"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <!-- item-->
                        <!--<a class="dropdown-item" href=""><i class="bx bx-user font-size-16 align-middle mr-1"></i> Profile</a>-->
                        <!--<a class="dropdown-item" href=""><i class="bx bx-user font-size-16 align-middle mr-1"></i> Change Password</a> -->
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item text-danger" href="{{url('control_panel/company/logout')}}"><i class="bx bx-power-off font-size-16 align-middle mr-1 text-danger"></i> Logout </a>
                        <form id="logout-form" action="control_panel/logout" method="POST" style="display: none;">
                            <input type="hidden" name="_token" value="8VbdUUyvg8rmqKLTuIQOs1FhSdeSYlwz7sXkq6SA">                    </form>
                    </div>
                </div>
            </div>
        </div>
    </header>
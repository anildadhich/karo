<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Terms & Condition</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                margin: 20px 50px;
            }

            .content {
                text-align: left;
            }

            .title {
                font-size: 25px;
                text-aling:right;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            p{
                color:#000000;
                line-height: 28px;
                font-size:17px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    <b>Privacy and Policy</b>
                </div>
                <p>
                    1. If you’re doctor is not satisfied with the reports we can re check, without any charges or you can
opt for 100 % refund.<br/>
2. Individual laboratory / radiology investigation are never conclusive but should be correlated
along with other relevent clinical examination to achieve final diagnosis.<br/>
3. The result of a laboratory in investigation /radiology investigation are dependent on the quality of
the sample an condition co –operation of the patient as well as the assay procedures used.<br/>
4. The reported results are for information and for interpretation of the doctor only.<br/>
5. Should be reconfirmed if there is any unexceptional abnormality in results.<br/>
6. This is a professional opinion, not a final diagnosis. this should be interpreted in the clinical
background &amp; other diagnostic modules. all subject to jaipur jurisdiction only.<br/>
7. Histo-pathological specimen is preserved for 15 day only. slides ,block and cytology FNAC are
preserved for 1 month only.<br/>
8. Value out of reference range requires reconfirmation before starting any medical treatment.re-
testing in needful if you suspect any quality shortcomings.<br/>
9. Results of tests may vary from laboratory to laboratory and also in some parameters time to time
for the same patient.<br/>
10. in cash of collected specimen (s), which are referred to Dr. Navneet Imaging &amp; Path Lab from
referral center,it is presumed that patient demographic are verified and confirmed at the point of
generation of the said specimen(s).<br/>
11. Any query from the referring doctor with reference to this should be directed to Dr. Navneet
Imaging Path Lab Jaipur between -2:00 P.M. to 5:00 P.M. on Phone:- 0141-2733716<br/>
12. This report is not valid for any medico-legal purposes.<br/>
                </p>
            </div>
        </div>
    </body>
</html>

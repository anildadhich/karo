
@include('control_panel.layouts.head')
<body data-sidebar="dark">
@include('control_panel.layouts.loder')
<div id="layout-wrapper">
    @include('control_panel.layouts.header')
    @include('control_panel.layouts.left_menu')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Manage User</h4>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table m-0" style="white-space: nowrap;">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Mobile No</th>
                                                <th>DOB</th>
                                                <th>Gender</th>
                                                <th>Ref Code</th>
                                                <th>Address</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($oGetRec as $aRec)
                                            <tr>
                                                <td>{{$aRec->sUser_Name}}</td>
                                                <td>{{$aRec->sUser_Email}}</td>
                                                <td>{{$aRec->sUser_Mobile}}</td>
                                                <td>{{date('d F, Y', strtotime($aRec->sUser_Dob))}}</td>
                                                <td>{{array_search($aRec->nUser_Gndr, config('constant.GENDER'))}}</td>
                                                <td>{{$aRec->sRef_Code}}</td>
                                                <td style="word-wrap: inherit;">{{$aRec->sAdrs_Line}}, {{$aRec->sAdrs_Area}}, {{$aRec->sState_Name}}, {{$aRec->sCity_Name}}, {{$aRec->sPin_Cod}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('control_panel.layouts.footer')
    </div>
</div>
</body>
</html>
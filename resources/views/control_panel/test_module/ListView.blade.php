@include('control_panel.layouts.head')
<body data-sidebar="dark">
@include('control_panel.layouts.loder')
<div id="layout-wrapper">
    @include('control_panel.layouts.header')
    @include('control_panel.layouts.left_menu')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Manage Test</h4>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        @if(Session::has('Success'))
                        <div class="alert alert-success">
                            <strong>Success ! </strong> {{Session::get('Success')}}
                        </div>
                        @endif
                        @if(Session::has('Failed'))
                        <div class="alert alert-danger">
                            <strong>Failed ! </strong> {{Session::get('Failed')}}
                        </div>
                        @endif
                        <div class="card">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form class="custom-validation"  action="{{url('admin_panel/test_list')}}" method="get">
                                        <div class="col-lg-2"><label class="text-left control-label col-form-label">Test Title</label></div>
                                        <div class="form-group col-md-4">
                                            <input type="text" class="form-control" required placeholder="Test Title" name="sTstName" value="{{$request['sTstName']}}"/>
                                        </div>
                                        <div class="col-lg-2">
                                            <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                                Search
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table m-0">
                                        <thead>
                                            <tr>
                                                <th>Title</th>
                                                <th>Gender</th>
                                                <th>Age Slab</th>
                                                <th>Amount</th>
                                                <th>Popular</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($oGetRec as $aRec)
                                            <tr>
                                                <td>{{$aRec->sTst_Name}}</td>
                                                <td>{{array_search($aRec->nTst_Gndr, config('constant.GENDER'))}}</td>
                                                <td>{{$aRec->nAge_Frm}} - {{$aRec->nAge_To}}</td>
                                                <td><strike>&#x20B9; {{$aRec->sActul_Amo}}</strike> &#x20B9; {{$aRec->sSale_Amo}}</td>
                                                <td>{{$aRec->nTst_Catg == config('constant.TEST_CATG.POPULAR') ? 'Yes' : 'No' }}</td>
                                                <td style="padding-left: 25px;">
                                                    @if($aRec->nBlk_UnBlk == config('constant.STATUS.UNBLOCK'))
                                                    <img src="control_panel/assets/images/show.png" onclick="ChngStatus('{{base64_encode($aRec->lTst_IdNo)}}','{{base64_encode('mst_test')}}','{{base64_encode('lTst_IdNo')}}','{{base64_encode(config('constant.STATUS.BLOCK'))}}')" style="cursor: pointer;">
                                                    @else
                                                    <img src="control_panel/assets/images/hide.png" onclick="ChngStatus('{{base64_encode($aRec->lTst_IdNo)}}','{{base64_encode('mst_test')}}','{{base64_encode('lTst_IdNo')}}','{{base64_encode(config('constant.STATUS.UNBLOCK'))}}')" style="cursor: pointer;">
                                                    @endif

                                              </td>
                                                <td>
                                                    <a href="{{url('/admin_panel/test')}}/{{base64_encode($aRec->lTst_IdNo)}}">
                                                        <button type="button" class="btn btn-success btn-sm" title="Edit Record" alt="Edit Record"><i class="fas fa-edit"></i></button>
                                                    </a>
                                                    <button type="button" class="btn btn-danger btn-sm" title="Delete Record" alt="Delete Record" onclick="DelRec('{{base64_encode($aRec->lTst_IdNo)}}','{{base64_encode('mst_test')}}','{{base64_encode('lTst_IdNo')}}')"><i class="fas fa-trash-alt"></i></button>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-sm-12 col-lg-12 pull-right text-right">
                                    {{$oGetRec->appends($request->all())->render()}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('control_panel.layouts.footer')
    </div>
</div>
</body>
</html>
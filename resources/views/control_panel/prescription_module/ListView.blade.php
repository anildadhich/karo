@include('control_panel.layouts.head')
<body data-sidebar="dark">
@include('control_panel.layouts.loder')
<div id="layout-wrapper">
    @include('control_panel.layouts.header')
    @include('control_panel.layouts.left_menu')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Manage Prescription</h4>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table m-0">
                                        <thead>
                                            <tr>
                                                <th>Customer</th>
                                                <th>PS No</th>
                                                <th>Patient Name</th>
                                                <th>Dr. Name</th>
                                                <th>File</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($oGetRec as $aRec)
                                            <tr>
                                                <td>{{$aRec->sUser_Name}}</td>
                                                <td>{{$aRec->sPs_No}}</td>
                                                <td>{{$aRec->sPtnt_Name}}</td>
                                                <td>{{$aRec->sDr_Name}}</td>
                                                <td><a href="{{config('constant.PUBLIC_URL')}}/prescription_doc/{{$aRec->sPrscrpt_File}}" target="_blank">Download File</a></td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('control_panel.layouts.footer')
    </div>
</div>
</body>
</html>
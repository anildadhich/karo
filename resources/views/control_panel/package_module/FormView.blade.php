@include('control_panel.layouts.head')
<body data-sidebar="dark">
@include('control_panel.layouts.loder')
<div id="layout-wrapper">
    @include('control_panel.layouts.header')
    @include('control_panel.layouts.left_menu')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Manage Package</h4>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        @if(Session::has('Failed'))
                        <div class="alert alert-danger">
                            <strong>Failed ! </strong> {{Session::get('Failed')}}
                        </div>
                        @endif
                        <div class="card">
                            <div class="card-body">
                                <form class="custom-validation" action="{{url('admin_panel/package_save')}}" id="general_form" method="post" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="input[lPckgIdNo]" value="{{isset($oGetRec) ? base64_encode($oGetRec->lPckg_IdNo) : base64_encode(0)}}">
                                    <div class="row">
                                        <div class="col-lg-2"><label class="text-left control-label col-form-label">Title</label></div>
                                        <div class="form-group col-md-4">
                                            <input type="text" class="form-control" required placeholder="Title" name="input[sPckgName]" value="{{isset($oGetRec) ? $oGetRec->sPckg_Name : ''}}" onkeypress="return LenCheck(event, this.value, '50')"/>
                                        </div>
                                        <div class="col-lg-2"><label class="text-left control-label col-form-label">Recommended For</label></div>
                                        <div class="form-group col-md-4">
                                            <select class="form-control" required name="input[nPckgGndr]">
                                                <option value="">== Select Option ==</option>
                                                @foreach(config('constant.GENDER') As $sValue => $nKey)
                                                <option {{isset($oGetRec) ? $oGetRec->nPckg_Gndr == $nKey ? 'selected' : '' : ''}} value="{{$nKey}}">{{$sValue}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2"><label class="text-left control-label col-form-label">Age</label></div>
                                        <div class="form-group col-md-2">
                                            <input type="text" class="form-control" required placeholder="Age From" name="input[nAgeFrm]" value="{{isset($oGetRec) ? $oGetRec->nAge_Frm : ''}}" onkeypress="return IsNumber(event, this.value, '2')"/>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <input type="text" class="form-control" required placeholder="Age To" name="input[nAgeTo]" value="{{isset($oGetRec) ? $oGetRec->nAge_To : ''}}" onkeypress="return IsNumber(event, this.value, '3')"/>
                                        </div>
                                        <div class="col-lg-2"><label class="text-left control-label col-form-label">Amount</label></div>
                                        <div class="form-group col-md-2">
                                            <input type="text" class="form-control" required placeholder="Actual Amount" name="input[sActulAmo]" value="{{isset($oGetRec) ? $oGetRec->sActul_Amo : ''}}" onkeypress="return IsNumber(event, this.value, '5')"/>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <input type="text" class="form-control" required placeholder="Sale Amount" name="input[sSaleAmo]" value="{{isset($oGetRec) ? $oGetRec->sSale_Amo : ''}}" onkeypress="return IsNumber(event, this.value, '5')"/>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2"><label class="text-left control-label col-form-label">Description</label></div>
                                        <div class="form-group col-md-10">
                                            <textarea required class="form-control" rows="5" name="input[sPckgDtl]" onkeypress="return LenCheck(event, this.value, '150')">{{isset($oGetRec) ? $oGetRec->sPckg_Dtl : ''}}</textarea>
                                        </div>
                                    </div>
                                    <div class="row" style="margin: 10px 5px 20px 5px;">
                                        @foreach($oGetTst as $aRec)
                                        <div class="custom-control custom-checkbox mb-2 col-lg-3">
                                            <input type="checkbox" class="custom-control-input"  id="customCheck{{$aRec->lTst_IdNo}}" name="sTstIds[]" value="{{$aRec->lTst_IdNo}}" {{ isset($oGetRec) ? in_array($aRec->lTst_IdNo, explode(",",$oGetRec->sTst_Ids)) ? 'checked=""' : '' : ''}}>
                                            <label class="custom-control-label" for="customCheck{{$aRec->lTst_IdNo}}">{{$aRec->sTst_Name}}</label>
                                        </div>
                                        @endforeach
                                    </div>
                                    <div class="form-group mb-0">
                                        <div>
                                            <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                                Submit
                                            </button>
                                            <button type="reset" class="btn btn-secondary waves-effect">
                                                Cancel
                                            </button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>
            </div>
        </div>
        @include('control_panel.layouts.footer')
    </div>
</div>
</body>
</html>
@include('control_panel.layouts.head')
<body data-sidebar="dark">
@include('control_panel.layouts.loder')
<div id="layout-wrapper">
    @include('control_panel.layouts.header')
    @include('control_panel.layouts.left_menu')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Manage Technician</h4>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        @if(Session::has('Success'))
                        <div class="alert alert-success">
                            <strong>Success ! </strong> {{Session::get('Success')}}
                        </div>
                        @endif
                        @if(Session::has('Failed'))
                        <div class="alert alert-danger">
                            <strong>Failed ! </strong> {{Session::get('Failed')}}
                        </div>
                        @endif
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table m-0">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Mobile</th>
                                                <th>City</th>
                                                <th>Area</th>
                                                <th>DOB</th>
                                                <th>Image</th>
                                                <th>ID Proof</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($oGetRec as $aRec)
                                            <tr>
                                                <td>{{$aRec->sTech_Name}}</td>
                                                <td>{{$aRec->sTech_Email}}</td>
                                                <td>{{$aRec->sTech_Mobile}}</td>
                                                <td>{{$aRec->sCity_Name}}</td>
                                                <td>{{$aRec->sArea_Name}}</td>
                                                <td>{{date('d F, Y', strtotime($aRec->sTech_Dob))}}</td>
                                                <td><img src="{{config('constant.PUBLIC_URL')}}/technician_pic/{{$aRec->sTech_Pic}}" width="50"></td>
                                                <td><a href="{{config('constant.PUBLIC_URL')}}/technician_proof/{{$aRec->sProf_Frnt}}" target="_blank"> Front</a> | <a href="{{config('constant.PUBLIC_URL')}}/technician_proof/{{$aRec->sProf_Bck}}" target="_blank"> Back</a></td>
                                                <td style="padding-left: 25px;">
                                                    @if($aRec->nBlk_UnBlk == config('constant.STATUS.UNBLOCK'))
                                                    <img src="control_panel/assets/images/show.png" onclick="ChngStatus('{{base64_encode($aRec->lTech_IdNo)}}','{{base64_encode('tech_mst')}}','{{base64_encode('lTech_IdNo')}}','{{base64_encode(config('constant.STATUS.BLOCK'))}}')" style="cursor: pointer;">
                                                    @else
                                                    <img src="control_panel/assets/images/hide.png" onclick="ChngStatus('{{base64_encode($aRec->lTech_IdNo)}}','{{base64_encode('tech_mst')}}','{{base64_encode('lTech_IdNo')}}','{{base64_encode(config('constant.STATUS.UNBLOCK'))}}')" style="cursor: pointer;">
                                                    @endif
                                                </td>
                                                <td>
                                                    <a href="{{url('/admin_panel/technician')}}/{{base64_encode($aRec->lTech_IdNo)}}">
                                                        <button type="button" class="btn btn-success btn-sm" title="Edit Record" alt="Edit Record"><i class="fas fa-edit"></i></button>
                                                    </a>
                                                    <button type="button" class="btn btn-danger btn-sm" title="Delete Record" alt="Delete Record" onclick="DelRec('{{base64_encode($aRec->lTech_IdNo)}}','{{base64_encode('tech_mst')}}','{{base64_encode('lTech_IdNo')}}')"><i class="fas fa-trash-alt"></i></button>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('control_panel.layouts.footer')
    </div>
</div>
</body>
</html>
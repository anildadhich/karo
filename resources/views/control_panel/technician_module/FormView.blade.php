@include('control_panel.layouts.head')
<link href="control_panel/assets/css/grid-form.css" rel="stylesheet" type="text/css" />
<body data-sidebar="dark">
@include('control_panel.layouts.loder')
<div id="layout-wrapper">
    @include('control_panel.layouts.header')
    @include('control_panel.layouts.left_menu')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Manage Technician</h4>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        @if(Session::has('Failed'))
                        <div class="alert alert-danger">
                            <strong>Failed ! </strong> {{Session::get('Failed')}}
                        </div>
                        @endif
                        <div class="card">
                            <div class="card-body">
                                <form class="custom-validation" action="{{url('admin_panel/technician_save')}}" id="general_form" method="post" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="lTechIdNo" value="{{isset($oGetRec) ? base64_encode($oGetRec->lTech_IdNo) : base64_encode(0)}}" id="lTstIdNo">
                                    <div class="row">
                                        <div class="col-lg-2"><label class="text-left control-label col-form-label">Name</label></div>
                                        <div class="form-group col-md-4">
                                            <input type="text" class=" form-control @error('sTechName') is-invalid @enderror" required placeholder="Name" name="sTechName" value="{{ old('sTechName', isset($oGetRec) ? $oGetRec->sTech_Name : '') }}" onkeypress="return LenCheck(event, this.value, '30')"/>
                                            @error('sTechName') <div class="invalid-feedback"><span>{{$errors->first('sTechName')}}</span></div>@enderror
                                        </div>
                                        <div class="col-lg-2"><label class="text-left control-label col-form-label">Gender</label></div>
                                        <div class="form-group col-md-4">
                                            <select class=" form-control @error('nTechGndr') is-invalid @enderror" required name="nTechGndr">
                                                <option value="">== Select Option ==</option>
                                                @foreach(config('constant.USER_GENDER') As $sValue => $nKey)
                                                <option  {{ old('nTechGndr', !empty($oGetRec) ? $oGetRec->nTech_Gndr : '') == $nKey ? 'selected' : ''}} value="{{$nKey}}">{{$sValue}}</option>
                                                @endforeach
                                            </select>
                                            @error('nTechGndr') <div class="invalid-feedback"><span>{{$errors->first('nTechGndr')}}</span></div>@enderror
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2"><label class="text-left control-label col-form-label">Mobile Number</label></div>
                                        <div class="form-group col-md-4">
                                            <input type="text" class=" form-control @error('sTechMobile') is-invalid @enderror" required placeholder="Mobile Number" name="sTechMobile" value="{{ old('sTechMobile', isset($oGetRec) ? $oGetRec->sTech_Mobile : '') }}" onkeypress="return IsNumber(event, this.value, '10')"/>
                                            @error('sTechMobile') <div class="invalid-feedback"><span>{{$errors->first('sTechMobile')}}</span></div>@enderror
                                        </div>
                                        <div class="col-lg-2"><label class="text-left control-label col-form-label">Email Id</label></div>
                                        <div class="form-group col-md-4">
                                            <input type="text" class=" form-control @error('sTechEmail') is-invalid @enderror" required placeholder="Email Id" name="sTechEmail" value="{{ old('sTechEmail', isset($oGetRec) ? $oGetRec->sTech_Email : '') }}" onkeypress="return LenCheck(event, this.value, '50')"/>
                                            @error('sTechEmail') <div class="invalid-feedback"><span>{{$errors->first('sTechEmail')}}</span></div>@enderror
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2"><label class="text-left control-label col-form-label">Date of birth</label></div>
                                        <div class="form-group col-md-4">
                                            <input type="date" class=" form-control @error('sTechDob') is-invalid @enderror" required name="sTechDob" value="{{ old('sTechDob', isset($oGetRec) ? $oGetRec->sTech_Dob : '') }}"/>
                                            @error('sTechDob') <div class="invalid-feedback"><span>{{$errors->first('sTechDob')}}</span></div>@enderror
                                        </div>
                                        <div class="col-lg-2"><label class="text-left control-label col-form-label">Address</label></div>
                                        <div class="form-group col-md-4">
                                            <input type="text" class=" form-control @error('sAdrsLine') is-invalid @enderror" required placeholder="Address" name="sAdrsLine" value="{{ old('sAdrsLine', isset($oGetRec) ? $oGetRec->sAdrs_Line : '') }}" onkeypress="return LenCheck(event, this.value, '150')"/>
                                            @error('sAdrsLine') <div class="invalid-feedback"><span>{{$errors->first('sAdrsLine')}}</span></div>@enderror
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2"><label class="text-left control-label col-form-label">Area Name</label></div>
                                        <div class="form-group col-md-4">
                                            <input type="text" class=" form-control @error('sAreaName') is-invalid @enderror" required placeholder="Area Name" name="sAreaName" value="{{ old('sAreaName', isset($oGetRec) ? $oGetRec->sArea_Name : '') }}"  onkeypress="return LenCheck(event, this.value, '30')"/>
                                            @error('sAreaName') <div class="invalid-feedback"><span>{{$errors->first('sAreaName')}}</span></div>@enderror
                                        </div>
                                        <div class="col-lg-2"><label class="text-left control-label col-form-label">City Name</label></div>
                                        <div class="form-group col-md-4">
                                            <input type="text" class=" form-control @error('sCityName') is-invalid @enderror" required placeholder="City Name" name="sCityName" value="{{ old('sCityName', isset($oGetRec) ? $oGetRec->sCity_Name : '') }}" onkeypress="return LenCheck(event, this.value, '30')"/>
                                            @error('sCityName') <div class="invalid-feedback"><span>{{$errors->first('sCityName')}}</span></div>@enderror
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2"><label class="text-left control-label col-form-label">State Name</label></div>
                                        <div class="form-group col-md-4">
                                            <input type="text" class=" form-control @error('sStateName') is-invalid @enderror" required placeholder="State Name" name="sStateName" value="{{ old('sStateName', isset($oGetRec) ? $oGetRec->sState_Name : '') }}"  onkeypress="return LenCheck(event, this.value, '30')"/>
                                            @error('sStateName') <div class="invalid-feedback"><span>{{$errors->first('sStateName')}}</span></div>@enderror
                                        </div>
                                        <div class="col-lg-2"><label class="text-left control-label col-form-label">Pin Code</label></div>
                                        <div class="form-group col-md-4">
                                            <input type="text" class=" form-control @error('sPinCod') is-invalid @enderror" required placeholder="Pin Code" name="sPinCod" value="{{ old('sPinCod', isset($oGetRec) ? $oGetRec->sPin_Cod : '') }}" onkeypress="return IsNumber(event, this.value, '6')"/>
                                            @error('sPinCod') <div class="invalid-feedback"><span>{{$errors->first('sPinCod')}}</span></div>@enderror
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2"><label class="text-left control-label col-form-label">Profile Image</label></div>
                                        <div class="form-group col-md-4">
                                            <input type="file" {{ isset($oGetRec) ? '' : 'required'}} name="sTechPic" accept="image/x-png,image/jpeg,image/jpg"/>
                                            @error('sTechPic') <div class="invalid-feedback"><span>{{$errors->first('sTechPic')}}</span></div>@enderror
                                        </div>
                                        <div class="col-lg-2"><label class="text-left control-label col-form-label">Aadhar Front</label></div>
                                        <div class="form-group col-md-4">
                                            <input type="file" {{ isset($oGetRec) ? '' : 'required'}} name="sProfFrnt" accept="image/x-png,image/jpeg,image/jpg"/>
                                            @error('sProfFrnt') <div class="invalid-feedback"><span>{{$errors->first('sProfFrnt')}}</span></div>@enderror
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2"><label class="text-left control-label col-form-label">Aadhar Back</label></div>
                                        <div class="form-group col-md-4">
                                            <input type="file" {{ isset($oGetRec) ? '' : 'required'}} name="sProfBck" accept="image/x-png,image/jpeg,image/jpg"/>
                                            @error('sProfBck') <div class="invalid-feedback"><span>{{$errors->first('sProfBck')}}</span></div>@enderror
                                        </div>
                                    </div>
                                    <div class="form-group mb-0">
                                        <div>
                                            <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                                Submit
                                            </button>
                                            <button type="reset" class="btn btn-secondary waves-effect">
                                                Cancel
                                            </button>
                                        </div> 
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>
            </div>
        </div>
        @include('control_panel.layouts.footer')
    </div>
</div>
</body>
</html>
<script type="text/javascript">
$(document).ready(function() {
    var lTstIdNo = $('#lTstIdNo').val();
    GetPreDtl(lTstIdNo);
});
</script>
<script type="text/javascript" src="control_panel/assets/js/form-grid.js"></script>
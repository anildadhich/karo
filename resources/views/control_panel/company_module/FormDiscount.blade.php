@include('control_panel.layouts.head')
<link href="control_panel/assets/css/grid-form.css" rel="stylesheet" type="text/css" />
<body data-sidebar="dark">
@include('control_panel.layouts.loder')
<div id="layout-wrapper">
    @include('control_panel.layouts.header')
    @include('control_panel.layouts.left_menu')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Manage Discount</h4>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        @if(Session::has('Failed'))
                        <div class="alert alert-danger">
                            <strong>Failed ! </strong> {{Session::get('Failed')}}
                        </div>
                        @endif
                        @if(Session::has('Success'))
                        <div class="alert alert-success">
                            <strong>Success ! </strong> {{Session::get('Success')}}
                        </div>
                        @endif
                        <div class="card">
                            <div class="card-body">
                                <form class="custom-validation" action="{{url('admin_panel/company_discount/save')}}" id="general_form" method="post" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="row">
                                        <div class="col-lg-2"><label class="text-left control-label col-form-label">Popular Category Off (%)</label></div>
                                        <div class="form-group col-md-4">
                                            <input type="text" class="form-control" required placeholder="Popular Category Discount" name="input[nPoplrCatgOff]" value="{{$oGetRec->nPoplr_Catg_Off}}" onkeypress="return IsNumber(event, this.value, '2')"/>
                                        </div>
                                        <div class="col-lg-2"><label class="text-left control-label col-form-label">Popular Test Off (%)</label></div>
                                        <div class="form-group col-md-4">
                                            <input type="text" class="form-control" required placeholder="Test Discount" name="input[nCatgOff]" value="{{$oGetRec->nCatg_Off}}" onkeypress="return IsNumber(event, this.value, '2')"/>
                                        </div>
                                    </div>
                                    <div class="form-group mb-0">
                                        <div>
                                            <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                                Update
                                            </button>
                                            <button type="reset" class="btn btn-secondary waves-effect" onclick="history.back()">
                                                Cancel
                                            </button>
                                        </div> 
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>
            </div>
        </div>
        @include('control_panel.layouts.footer')
    </div>
</div>
</body>
</html>
<script type="text/javascript">
$(document).ready(function() {
    var lTstIdNo = $('#lTstIdNo').val();
    GetPreDtl(lTstIdNo);
});
</script>
<script type="text/javascript" src="control_panel/assets/js/form-grid.js"></script>
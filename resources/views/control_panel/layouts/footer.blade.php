<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <script>document.write(new Date().getFullYear())</script> © Karo.
            </div>
            <div class="col-sm-6">
                <div class="text-sm-right d-none d-sm-block">
                    
                </div>
            </div>
        </div>
    </div>
</footer>            
<script type="text/javascript">
var APP_URL = "{{url('')}}";
</script>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="control_panel/assets/libs/bootstrap/bootstrap.min.js"></script>
<script src="control_panel/assets/libs/metismenu/metismenu.min.js"></script>
<script src="control_panel/assets/libs/simplebar/simplebar.min.js"></script>
<script src="control_panel/assets/libs/node-waves/node-waves.min.js"></script>
<script src="control_panel/assets/js/app.min.js"></script>  
<script src="control_panel/assets/js/form-validation.min.js"></script>  
<script src="control_panel/assets/js/common-script.js"></script>  
<div class="vertical-menu">
    <div data-simplebar class="h-100">
        <div id="sidebar-menu">
            <ul class="metismenu list-unstyled" id="side-menu">
                <li>
                    <a href="{{url('admin_panel')}}" class="waves-effect">
                        <i class="bx bx-home-circle"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="bx bx-layout"></i>
                        <span>Test Master</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{url('admin_panel/test')}}">Create Test</a></li>
                        <li><a href="{{url('admin_panel/test_list')}}">View / Alter Test</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="bx bx-layout"></i>
                        <span>Category Test Master</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="admin_panel/category">Create Category Test</a></li>
                        <li><a href="admin_panel/category_list">View / Alter Category Test</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="bx bx-layout"></i>
                        <span>Package Master</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="admin_panel/package">Create Package</a></li>
                        <li><a href="admin_panel/package_list">View / Alter Package</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="bx bx-layout"></i>
                        <span>Technician Master</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="admin_panel/technician">Create Technician</a></li>
                        <li><a href="admin_panel/technician_list">View / Alter Technician</a></li>
                    </ul>
                </li>
                <li>
                    <a href="{{url('admin_panel/user')}}" class="waves-effect">
                        <i class="bx bxs-user-detail"></i>
                        <span>User Master</span>
                    </a>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="bx bxs-user-detail"></i>
                        <span>Manage Order</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{url('admin_panel/pending_order')}}">New Order</a></li>
                        <li><a href="{{url('admin_panel/completed_order')}}">Completed Order</a></li>
                        <li><a href="{{url('admin_panel/cancel_order')}}">Cancel Order</a></li>
                    </ul>
                </li>
                <li>
                    <a href="{{url('admin_panel/collection')}}" class="waves-effect">
                        <i class="bx bxs-user-detail"></i>
                        <span>Cash Collection</span>
                    </a>
                </li>
                <li>
                    <a href="{{url('admin_panel/perception')}}" class="waves-effect">
                        <i class="bx bxs-user-detail"></i>
                        <span>User Prescription</span>
                    </a>
                </li>
                <li>
                    <a href="{{url('admin_panel/feedback')}}" class="waves-effect">
                        <i class="bx bxs-user-detail"></i>
                        <span>User Feedback</span>
                    </a>
                </li>
                <li>
                    <a href="{{url('admin_panel/company_discount')}}" class="waves-effect">
                        <i class="bx bxs-user-detail"></i>
                        <span>Category Discount</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
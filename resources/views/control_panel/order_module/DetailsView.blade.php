@include('control_panel.layouts.head')
<body data-sidebar="dark">
@include('control_panel.layouts.loder')
<div id="layout-wrapper">
    @include('control_panel.layouts.header')
    @include('control_panel.layouts.left_menu')
    <div class="main-content">
    <div class="page-content">
            <div class="container-fluid">
  <div class="card">
<div class="card-header">
<strong>Date: {{ date('d M, Y', strtotime($oGetOrd->sCrt_DtTm)) }}</strong> 
<span class="float-right"> <strong>Order No:</strong> {{$oGetOrd->sOrd_No}}</span>
</div>
<div class="card-body">
<div class="row mb-4">
<div class="col-sm-4">
<h6 class="mb-3">To:</h6>
<div>
<strong>{{$oGetOrd->sUser_Name}}</strong>
</div>
<div>{{$oGetOrd->sUser_Adrs}}</div>
<div>Phone: {{$oGetOrd->sMobile_No}}</div>
</div>
</div>

<div class="table-responsive-sm">
<table class="table table-striped">
<thead>
<tr>
<th>Item Name</th>
<th>Description</th>
<th align="right">Item Amount</th>
</tr>
</thead>
<tbody>
@foreach($oGetDtlRec as $aRec)
<tr>
    <td>{{$aRec->sItem_Name}}</td>
    <td>{{substr($aRec->sItem_Dtl,0,100)}}</td>
    <td align="right">{{number_format($aRec->sItem_Amo,2)}}</td>
</tr>
@endforeach
</tbody>
</table>
</div>
<div class="row">
<div class="col-lg-4 col-sm-5">

</div>

<div class="col-lg-4 col-sm-5 ml-auto">
<table class="table table-clear">
<tbody>
<tr>
<td class="left">
<strong>Sub Amount</strong>
</td>
<td class="right" align="right">
<strong>{{number_format($oGetOrd->sSub_Amo,2)}}</strong>
</td>
</tr>
<tr>
<td class="left">
<strong>Discount %</strong>
</td>
<td class="right" align="right">
<strong>{{number_format($oGetOrd->sDis_Per,2)}}</strong>
</td>
</tr>
@if(!empty($oGetOrd->sWlt_Amo))
<tr>
<td class="left">
<strong>Wallet Amount</strong>
</td>
<td class="right" align="right">
<strong>{{empty($oGetOrd->sWlt_Amo) ? 0 : number_format($oGetOrd->sWlt_Amo,2)}}</strong>
</td>
</tr>
@endif
<tr>
<td class="left">
<strong>Total</strong>
</td>
<td class="right" align="right">
<strong>{{$oGetOrd->sTtl_Amo}}</strong>
</td>
</tr>
</tbody>
</table>

</div>

</div>

</div>
</div>
</div>
</div>



        @include('control_panel.layouts.footer')
    </div>
</div>
</body>
</html>
@php
$TechOrder = new \App\Model\TechOrder;
@endphp
@include('control_panel.layouts.head')
<body data-sidebar="dark">
@include('control_panel.layouts.loder')
<div id="layout-wrapper">
    @include('control_panel.layouts.header')
    @include('control_panel.layouts.left_menu')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Manage New Order</h4>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        @if(Session::has('Success'))
                        <div class="alert alert-success">
                            <strong>Success ! </strong> {{Session::get('Success')}}
                        </div>
                        @endif
                        @if(Session::has('Failed'))
                        <div class="alert alert-danger">
                            <strong>Success ! </strong> {{Session::get('Failed')}}
                        </div>
                        @endif
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table m-0" style="white-space: nowrap;">
                                        <thead>
                                            <tr>
                                                <th>Order No.</th>
                                                <th>User Name</th>
                                                <th>Mobile No.</th>
                                                <th>Amount</th>
                                                <th>Mode</th>
                                                <th>Collect (Home)</th>
                                                <th>Order Status</th>
                                                <th>Date</th>
                                                <th>TL Status</th>
                                                <th>Report</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($oGetRec as $aRec)
                                            @php
                                                $oGetDtl = $TechOrder->TechOrdDtl($aRec->lOrd_Hd_IdNo);
                                            @endphp
                                            <tr>
                                                <td>{{$aRec->sOrd_No}}</td>
                                                <td>{{$aRec->sUser_Name}}</td>
                                                <td>{{$aRec->sMobile_No}}</td>
                                                <td>{{$aRec->sTtl_Amo}}</td>
                                                <td>{{array_search($aRec->nPay_Mod, config('constant.PAY_MTHD'))}}</td>
                                                <td align="center">{{array_search($aRec->nOrd_Type, config('constant.CLCT_STATUS'))}}</td>
                                                <td>{{array_search($aRec->nOrd_Status, config('constant.ORD_STATUS'))}}</td>
                                                <td>{{date('d F, Y', strtotime($aRec->sCrt_DtTm))}}</td>
                                                <td>
                                                    @if($aRec->nOrd_Status == config('constant.ORD_STATUS.ONGOING'))
                                                        @if(isset($oGetDtl))
                                                            @if($oGetDtl->nCurr_Status == config('constant.TECH_ORD_STATUS.REJECTED'))
                                                                <a href="#" title="Allot To Technician" alt="Allot To Technician" data-id="{{base64_encode($aRec->lOrd_Hd_IdNo)}}" data-toggle="modal" data-target="#TechModel"><b>Allocate Again</b></a>
                                                            @else
                                                                {{ucfirst(strtolower(array_search($oGetDtl->nCurr_Status, config('constant.TECH_ORD_STATUS'))))}}
                                                            @endif
                                                        @else
                                                            <a href="#" title="Allot To Technician" alt="Allot To Technician" data-id="{{base64_encode($aRec->lOrd_Hd_IdNo)}}" data-toggle="modal" data-target="#TechModel"><b>Allocate</b></a>
                                                        @endif
                                                    @else
                                                        ========
                                                    @endif

                                                </td>
                                                <td>
                                                    @if($aRec->nOrd_Status != config('constant.ORD_STATUS.CANCELLED'))
                                                        @if(empty($aRec->sFile_Name))
                                                            Pending
                                                        @else
                                                            <a href="{{config('constant.PUBLIC_URL')}}/user_report/{{$aRec->sFile_Name}}" target="_blank"> Get Report</a>
                                                        @endif
                                                    @else
                                                        ========
                                                    @endif
                                                </td>
                                                <td>
                                                <a href="{{url('admin_panel/order_detail')}}?lRecIdNo={{base64_encode($aRec->lOrd_Hd_IdNo)}}" target="_blank">
                                                    <button type="button" class="btn btn-success btn-sm" title="View Details" alt="View Details"><i class="fas fa-eye"></i></button>
                                                </a>
                                                    @if($aRec->nOrd_Status == config('constant.ORD_STATUS.ONGOING'))
                                                        <button type="button" class="btn btn-success btn-sm" title="Upload Report" data-id="{{base64_encode($aRec->lOrd_Hd_IdNo)}}" data-toggle="modal" data-target="#RprtModel"><i class="fas fa-upload"></i></button>
                                                        <button type="button" class="btn btn-danger btn-sm" title="Cancel Order" data-id="{{base64_encode($aRec->lOrd_Hd_IdNo)}}" onclick="CnclOrd('{{base64_encode($aRec->lOrd_Hd_IdNo)}}')"><i class="fas fa-times"></i></button>
                                                    @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('control_panel.layouts.footer')
    </div>
</div>
</body>

<!-- Modal for Technician -->
<div class="modal fade" id="TechModel" tabindex="-1" role="dialog" aria-labelledby="TechModelLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="TechModelLabel">Allot To Technician</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{url('admin_panel/allocate/order')}}" method="post" id="general_form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="input[lOrdHdIdNo]" id="lOrdHdIdNo" value="">
                    <div class="form-group col-md-12">
                        <select class="form-control required" name="input[lTechIdNo]" required>
                            <option value="">== Select Technician ==</option>
                            @foreach($oGetTech as $aRec)
                                <option value="{{$aRec->lTech_IdNo}}">{{$aRec->sTech_Name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <input class="form-control required" type="text" name="input[sCmntDtl]" placeholder="Comment" required="">
                    </div>
                    <div class="modal-footer">
                        <a type="button" class="btn btn-secondary" data-dismiss="modal">Close</a>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal for Report -->
<div class="modal fade" id="RprtModel" tabindex="-1" role="dialog" aria-labelledby="RprtModelLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="TechModelLabel">Upload Report</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{url('admin_panel/upload/report')}}" method="post" id="general_form" enctype='multipart/form-data'>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="input[lOrdHdIdNo]" id="lOrdHdIdNo" value="">
                    <div class="form-group col-md-12">
                        <input type="file" name="sFileName" required class="required">
                    </div>
                    <div class="modal-footer">
                        <a type="button" class="btn btn-secondary" data-dismiss="modal">Close</a>
                        <button type="submit" class="btn btn-primary">Upload</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
$('#TechModel').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var recipient = button.data('id')
  var modal = $(this)
  modal.find('.modal-body #lOrdHdIdNo').val(recipient)
})

$('#RprtModel').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var recipient = button.data('id')
  var modal = $(this)
  modal.find('.modal-body #lOrdHdIdNo').val(recipient)
})

function CnclOrd(lOrdHdIdNo)
{
    if(confirm("Are you sure to cancel this order ?") == true)
    {
        window.location=APP_URL+"/admin_panel/order/cancel?lRecIdNo="+lOrdHdIdNo;
    }
}
</script>
</html>
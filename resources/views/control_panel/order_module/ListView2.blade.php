@php
$TechOrder = new \App\Model\TechOrder;
@endphp
@include('control_panel.layouts.head')
<body data-sidebar="dark">
@include('control_panel.layouts.loder')
<div id="layout-wrapper">
    @include('control_panel.layouts.header')
    @include('control_panel.layouts.left_menu')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Manage Canceled Order</h4>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table m-0" style="white-space: nowrap;">
                                        <thead>
                                            <tr>
                                                <th>Order No.</th>
                                                <th>User Name</th>
                                                <th>Mobile No.</th>
                                                <th>Amount</th>
                                                <th>Mode</th>
                                                <th>Collect (Home)</th>
                                                <th>Date</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($oGetRec as $aRec)
                                            <tr>
                                                <td>{{$aRec->sOrd_No}}</td>
                                                <td>{{$aRec->sUser_Name}}</td>
                                                <td>{{$aRec->sMobile_No}}</td>
                                                <td>{{$aRec->sTtl_Amo}}</td>
                                                <td>{{array_search($aRec->nPay_Mod, config('constant.PAY_MTHD'))}}</td>
                                                <td align="center">{{array_search($aRec->nOrd_Type, config('constant.CLCT_STATUS'))}}</td>
                                                <td>{{date('d F, Y', strtotime($aRec->sCrt_DtTm))}}</td>
                                                <td>
                                                    <a href="{{url('admin_panel/order_detail')}}?lRecIdNo={{base64_encode($aRec->lOrd_Hd_IdNo)}}" target="_blank">
                                                        <button type="button" class="btn btn-success btn-sm" title="View Details" alt="View Details"><i class="fas fa-eye"></i></button>
                                                    </a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('control_panel.layouts.footer')
    </div>
</div>
</body>
</html>
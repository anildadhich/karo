@php
$TechOrder = new \App\Model\TechOrder;
@endphp
@include('control_panel.layouts.head')
<body data-sidebar="dark">
@include('control_panel.layouts.loder')
<div id="layout-wrapper">
    @include('control_panel.layouts.header')
    @include('control_panel.layouts.left_menu')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Manage Collection</h4>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table m-0" style="white-space: nowrap;">
                                        <thead>
                                            <tr>
                                                <th>Order No.</th>
                                                <th>Technician Name</th>
                                                <th>Order Amount</th>
                                                <th>Collect Amount</th>
                                                <th>Due Amount</th>
                                                <th>Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(count($oGetRec) > 0)
                                                @foreach($oGetRec as $aRec)
                                                <tr>
                                                    <td>{{$aRec->sOrd_No}}</td>
                                                    <td>{{$aRec->sTech_Name}}</td>
                                                    <td>{{$aRec->sTtl_Amo}}</td>
                                                    <td>{{empty($aRec->sClct_Amo) ? 0 : $aRec->sClct_Amo}}</td>
                                                    <td>{{empty($aRec->sClct_Amo) ? $aRec->sTtl_Amo-0 : $aRec->sTtl_Amo-$aRec->sClct_Amo}}</td>
                                                    <td>{{date('d F, Y h:i A', strtotime($aRec->sClct_DtTm))}}</td>
                                                </tr>
                                                @endforeach
                                            @else
                                                <tr><td colspan="4" align="center"><strong>No Record's Found</strong></td></tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('control_panel.layouts.footer')
    </div>
</div>
</body>
</html>
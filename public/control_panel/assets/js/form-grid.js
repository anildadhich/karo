function GnrtRaw(oDtData = '') 
{
    content = '';
    content += '<div class="row GridDiv">';
        content += '<table border="0" cellpadding="0" cellspacing="0" align="left" width="80%" id="GridForm">';
        content += '<thead>';
        content += '<tr>';
        content += '<td width="7%"></td>';
        content += '<td width="35%">Parameters Title</td>';
        content += '<td width="58%">Parameters Detail</td>';
        content += '</tr>';
        content += '</thead>';
        content += '<tbody id="GridRow">';
        if(oDtData == '') {
            content += '<tr id="Row_1">';
            content += '<td onclick="CrtRow()" class="text-center" style="font-size: 18px; cursor: pointer;"><b>+</b></td>';
            content += '<td><input type="text" name="input[sPrmtrName_1]" class="form-control grid-box"></td>';
            content += '<td><input type="text" name="input[sPrmtrDtl_1]" class="form-control grid-box"></td>';
            content += '</tr>';
        } else {
            oDtData.forEach(function (DtDataVal, key) {
                key = parseInt(key+1);
                content += '<tr id="Row_'+key+'">';
                if(key != 1) {
                    content += '<td onclick="SelectRow('+key+')" class="text-center" style="font-size: 18px; cursor: pointer;"><b>-</b></td>';
                } else {
                    content += '<td onclick="CrtRow()" class="text-center" style="font-size: 18px; cursor: pointer;"><b>+</b></td>';
                }
                content += '<td><input type="text" name="input[sPrmtrName_'+key+']" value="'+DtDataVal['sPrmtr_Name']+'" class="form-control grid-box" required></td>';
                content += '<td><input type="text" name="input[sPrmtrDtl_'+key+']" value="'+DtDataVal['sPrmtr_Dtl']+'" class="form-control grid-box" required></td>';
                content += '</tr>';
            });
        }
        content += '</tbody>';
        content += '</table>';
        if(oDtData != '') {
            content += '<input type="hidden" name="input[toalrec]" id="toal_records" value="'+oDtData.length+'">';
        } else {
            content += '<input type="hidden" name="input[toalrec]" id="toal_records" value="1">';
        }
        $('#GridView').html(content);
}

var nRowId;
function CrtRow()
{
    total = $("#toal_records").val();
    next_no = parseInt(total)+1;
    newdiv = document.createElement('tr');
    divid = "Row_"+next_no;
    newdiv.setAttribute('id', divid);
    content = '';
    content+= '<tr>';
    content+= '<td onclick="SelectRow('+next_no+')" class="text-center" style="font-size: 18px; cursor: pointer;"><b>-</b></td>';
    content+= '<td><input type="text" name="input[sPrmtrName_'+next_no+']" class="form-control grid-box"></td>';
    content+= '<td><input type="text" name="input[sPrmtrDtl_'+next_no+']" class="form-control grid-box"></td>';
    content+= '</tr>';
    newdiv.innerHTML = content;
    $("#toal_records").val(next_no);
    $("#GridRow").append(newdiv);
}

function SelectRow(nRow)
{
    var count = $('#GridRow').children('tr').length;
    if(count > 1) {
        $('tbody tr td').css('background-color', '#ffffff');
        $('tr .form-control').css('background-color', '#ffffff');
        $('#Row_'+nRow+' td').css('background-color', '#DEDBDB');
        $('#Row_'+nRow+' .form-control').css('background-color', '#DEDBDB');
        $('#Row_'+nRow).addClass('active');
        nRowId  = "Row_" + nRow;
        if(confirm("Are you sure to delete this row") == true) {
            var row = $('#'+nRowId);
            row.remove();
        }
    }
}

document.onkeyup = function(eEvnt) 
{
    if (eEvnt.which == 46) { //Delete
        var count = $('#GridRow').children('tr').length;
        if(count > 1) {
            if(confirm("Are you sure to delete this row") == true) {
                var row = $('#'+nRowId);
                row.remove();
            }
        }
    }
}
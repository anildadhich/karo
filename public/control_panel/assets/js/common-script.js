function DelRec(lRecIdNo, sTblName, sFldName)
{
    if(lRecIdNo != '')
    {
        if(confirm("Are you sure to delete this record ?") == true)
        {
            window.location = "admin_panel/delete/"+lRecIdNo+"/"+sTblName+"/"+sFldName;
        }
    }
}

function ChngStatus(lRecIdNo, sTblName, sFldName, nStatus)
{
    if(lRecIdNo != '')
    {
        if(confirm("Are you sure to change status for this record ?") == true)
        {
            window.location = "admin_panel/status/"+lRecIdNo+"/"+sTblName+"/"+sFldName+"/"+nStatus;
        }
    }
}

setTimeout(function() {
    $('.alert').fadeOut('slow');
}, 3000);

function GetPreDtl(lTstIdNo)
{
    if(atob(lTstIdNo) != 0)
    {
        $.ajax({
            url:APP_URL+"/admin_panel/get_prmtr/"+lTstIdNo,
            success:function(response)
            {
                GnrtRaw(response);
            }
        });
    }
    else
    {
       GnrtRaw(); 
    }
}
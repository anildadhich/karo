<?php
return [
	"STATUS"	=> [
		"BLOCK"	=> 409,
		"UNBLOCK"	=> 410,
	],

	"DEL_STATUS"	=> [
		"DELETED"		=> 409,
		"NON_DELETED"	=> 410,
	],

	"CLCT_STATUS"	=> [
		"YES"		=> 801,
		"NO"	=> 802,
	],

	"TECH_STATUS"	=> [
		"ONLINE"		=> 101,
		"OFFLINE"	=> 102,
	],

	"GENDER"	=> [
		"Male"			=> 101,
		"Female"		=> 102,
		"Male, Female"	=> 103,
		"Child"	=> 104,
		"Male, Female, Child"	=> 105,
	],

	"USER_GENDER"	=> [
		"Male"			=> 201,
		"Female"		=> 202,
	],

	"ORD_STATUS"	=> [
		"ONGOING"		=> 601,
		"COMPLETE"		=> 602,
		"CANCELLED"		=> 603,
	],

	"TECH_ORD_STATUS"	=> [
		"PENDING"		=> 501,
		"ACCEPTED"		=> 502,
		"REJECTED"		=> 503,
		"COLLECTED"		=> 504,
		"DELIVERED"		=> 505,
	],

	"TEST_CATG"	=> [
		"POPULAR"		=> 101,
		"NON_POPULAR"		=> 102,
	],

	"PAY_MTHD"	=> [
		"ONLINE"	=> 201,
		"CASH"		=> 202,
	],

	"MEMBER"	=> [
		"Friend"			=> 1001,
		"Grand Father"		=> 1002,
		"Grand Mother"		=> 1003,
		"Father"			=> 1004,
		"Mother"			=> 1005,
		"Wife"				=> 1006,
		"Son / Daughter"	=> 1007,
		"Brother"			=> 1008,
		"Sister"			=> 1009,
		"Uncle"				=> 1010,
		"Aunty"				=> 1011,
		"Other"				=> 1012,
	],

	'VLDT_MSG' => [
		'required' 			=> 'Required filed missing',
        'unique' 			=> 'Linked with another account',
        'alpha' 			=> 'Only alphabetic characters allowed',
        'digits' 			=> ':digits numeric characters allowed',
        'min'				=> 'To sort, at least :min characters',
        'max'				=> 'To long, max :max characters',
        'between' 			=> 'Distance must be at least 0 - 10',
        'accepted' 			=> 'Accept Terms & Conditions',
        'integer' 			=> 'Must be an integer',
        'mimes' 			=> 'Only jpeg, jpg, png image allowed',
        'sTechName.regex'	=> 'Special and numeric characters not allowed',
        'sTechPic.max'		=> 'Max 2MB allowed',
        'sProfFrnt.max'		=> 'Max 2MB allowed',
        'sProfBck.max'		=> 'Max 2MB allowed',
	],

	"PUBLIC_URL"		=> "https://karolab.i4acmmosmedia.com/public",
]
?>
<?php

namespace App\Http\Controllers\TechnicianController;

use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use App\Model\Technician;
use App\Model\TechOrder;
use DB;

class TechController extends Controller
{
	public function __construct() 
	{
		$this->Technician 	= new Technician;
		$this->TechOrder 	= new TechOrder;
		 // parent::__construct();
		header("Content-Type: application/json");
		$valid_passwords = array ("karo" => "026866326a9d1d2b23226e4e5317569f");
		$valid_users = array_keys($valid_passwords);

		$user = request()->server('PHP_AUTH_USER');
		$pass = request()->server('PHP_AUTH_PW');

		$validated = (in_array($user, $valid_users)) && ($pass == $valid_passwords[$user]);

		if (!$validated) {
		  header('WWW-Authenticate: Basic realm="My Realm"');
		  header('HTTP/1.0 401 Unauthorized');
		  $re = array(
		  	"status" 	=> false,
		  	"message"	=> "You're not authorized to access."
		  );
		  echo json_encode($re, JSON_PRETTY_PRINT);
		  die;
		 }
	}

	public function TechLogin(Request $request)
	{
		try
		{
			if(!isset($request['sTechMobile']) || !empty($request['sTechMobile']))
			{
				$oGetTech	= $this->Technician->ChkUser($request);
				if(isset($oGetTech) && !empty($oGetTech->lTech_IdNo))
				{
					$aTknArr 	= $this->DvcArr($request['sDvicTokn']);
					$nRow		= $this->Technician->UpDtRecrd($aTknArr, $oGetTech->lTech_IdNo);
					$nLoginOTP	= rand(100001,999999);
					Controller::SendSms($request['sTechMobile'], $nLoginOTP);
					$aRes = array(
						"ResponseCode"	=> 200,
						"Status"		=> True,
						"Message"		=> "LOGIN SUCCESSFULLY...",
						"Data"			=> ['lTechIdNo'	=> $oGetTech->lTech_IdNo, "nLoginOTP" => $nLoginOTP]
					);
				}
				else
				{
					$aRes = array(
						"ResponseCode"	=> 200,
						"Status"		=> False,
						"Message"		=> "WE COULD NOT FIND ACCOUNT WITH THAT INFO..."
					);
				}
			}
			else
			{
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> False,
					"Message"		=> "REQUIRED FIELD MISSING..."
				);
			}
		}
		catch (\Exception $e)
    	{
    		$aRes = array(
				"ResponseCode"	=> 400,
				"Message"		=> $e->getMessage()
			);
    	}
		return json_encode($aRes, JSON_PRETTY_PRINT);
	}

	public function TechLogout(Request $request)
	{
		try
		{
			$lTechIdNo 	= $request['lTechIdNo'];
			$aTknArr 	= $this->DvcArr($request['sDvicTokn']);
			$nRow		= $this->Technician->UpDtRecrd($aTknArr, $lTechIdNo);
			if($nRow == 1)
			{
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> True,
					"Message"		=> "LOGOUT SUCCESSFULLY...",
				);
			}
			else
			{
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> False,
					"Message"		=> "DEVICE TOKEN DID NOT UPDATED, TRY AGAIN..."
				);		
			}
		}
		catch (\Exception $e)
    	{
    		$aRes = array(
				"ResponseCode"	=> 400,
				"Message"		=> $e->getMessage()
			);
    	}
		return json_encode($aRes, JSON_PRETTY_PRINT);
	}

	public function DvcArr($sDvicTokn)
	{
		$aComnArr = array(
			"sDvic_Tokn" => !empty($sDvicTokn) ? $sDvicTokn : NULL,
		);
		return $aComnArr;
	}

	public function TechProfile(Request $request)
	{
		try
		{
			$Validate = Validator::make($request->all(),[
							'lTechIdNo' => 'required',
						]);

			if($Validate->fails())
			{
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> False,
					"Message"		=> "REQUIRED FIELD MISSING..."
				);		
			}
			else
			{
				$lTechIdNo  	= $request['lTechIdNo'];
				$oGetOrd		= $this->TechOrder->PndgOrd($lTechIdNo);
				$oGetTech		= $this->Technician->Profile($lTechIdNo);
				if(!isset($oGetTech) && empty($oGetTech->lTech_IdNo))
				{
					$aRes = array(
						"ResponseCode"	=> 200,
						"Status"		=> False,
						"Message"		=> "WE COUND NOT FOUND ANY PROFILE..."
					);
				}
				else
				{
					$aProfileData = ["sTechName" => $oGetTech->sTech_Name, "sTechEmail" => $oGetTech->sTech_Email, "sTechMobile" => $oGetTech->sTech_Mobile, "nTechGndr" => $oGetTech->nTech_Gndr, "sTechPic" => config('constant.PUBLIC_URL').'/technician_pic/'.$oGetTech->sTech_Pic, "sTechDob"  => $oGetTech->sTech_Dob, "nTechStatus" => $oGetTech->nTech_Status, "sTechAdrs" => $oGetTech->sAdrs_Line.", ".$oGetTech->sCity_Name.", ".$oGetTech->sState_Name.", ".$oGetTech->sPin_Cod, "sProfFrnt" => config('constant.PUBLIC_URL').'/technician_proof/'.$oGetTech->sProf_Frnt, "sProfBck" => config('constant.PUBLIC_URL').'/technician_proof/'.$oGetTech->sProf_Bck, 'nPndgOrd' => count($oGetOrd)];

					$aRes = array(
						"ResponseCode"	=> 200,
						"Status"		=> True,
						"Message"		=> "PROFILE GET SUCCESSFULLY...",
						"Data"			=> $aProfileData
					);
				}
			}
		}
		catch (\Exception $e)
    	{
    		$aRes = array(
				"ResponseCode"	=> 400,
				"Message"		=> $e->getMessage()
			);
    	}
		return json_encode($aRes, JSON_PRETTY_PRINT);
	}

	public function UpDtStatus(Request $request)
	{
		try
		{
			$Validate = Validator::make($request->all(),[
							'lTechIdNo' => 'required',
							'nTechStatus' => 'required',
						]);

			if($Validate->fails())
			{
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> False,
					"Message"		=> "REQUIRED FIELD MISSING..."
				);		
			}
			else
			{
				$aHdArr 	= $this->StatusArr($request['nTechStatus']);
				$nUpDtRaw 	= $this->Technician->UpDtRecrd($aHdArr, $request['lTechIdNo']);
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> True,
					"Message"		=> "STATUS CHANGED SUCCESSFULLY..."
				);				
			}
		}
		catch (\Exception $e)
    	{
    		$aRes = array(
				"ResponseCode"	=> 400,
				"Message"		=> $e->getMessage()
			);
    	}
		return json_encode($aRes, JSON_PRETTY_PRINT);
	}

	public function StatusArr($nTechStatus)
	{
		$aComnArr = array(
			"nTech_Status"	=> $nTechStatus,
		);
		return $aComnArr;
	}

	public function RsndOtp(Request $request)
	{
		try
		{
			$oGetTech	= $this->Technician->ChkUser($request);
			if(isset($oGetTech) && !empty($oGetTech->lTech_IdNo))
			{
				$nMobileOTP	= rand(111111,999999);
				Controller::SendSms($request['sTechMobile'], $nMobileOTP);
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> True,
					"Message"		=> "OTP SEND SUCCESSFULLY...",
					"Data"			=> $nMobileOTP
				);
			}
			else
			{
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> False,
					"Message"		=> "WE COULD NOT FOUNT ANY ACCOUNT WITH THAT INFO..."
				);		
			}
		}
		catch (\Exception $e)
    	{
    		$aRes = array(
				"ResponseCode"	=> 400,
				"Message"		=> $e->getMessage()
			);
    	}
		return json_encode($aRes, JSON_PRETTY_PRINT);
	}
}
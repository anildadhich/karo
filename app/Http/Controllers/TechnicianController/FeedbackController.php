<?php

namespace App\Http\Controllers\TechnicianController;

use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use App\Model\FeedbackTech;
use DB;

class FeedbackController extends Controller
{
	public function __construct() 
	{
		$this->FeedbackTech 	= new FeedbackTech;
		 // parent::__construct();
		header("Content-Type: application/json");
		$valid_passwords = array ("karo" => "026866326a9d1d2b23226e4e5317569f");
		$valid_users = array_keys($valid_passwords);

		$user = request()->server('PHP_AUTH_USER');
		$pass = request()->server('PHP_AUTH_PW');

		$validated = (in_array($user, $valid_users)) && ($pass == $valid_passwords[$user]);

		if (!$validated) {
		  header('WWW-Authenticate: Basic realm="My Realm"');
		  header('HTTP/1.0 401 Unauthorized');
		  $re = array(
		  	"status" 	=> false,
		  	"message"	=> "You're not authorized to access."
		  );
		  echo json_encode($re, JSON_PRETTY_PRINT);
		  die;
		}
	}

	public function SaveCntrl(Request $request)
	{
		try
		{
			$validator = Validator::make($request->all(), [
	            'lTechIdNo' 	=> 'required',
		        'sFdbckDtl' 	=> 'required',
	        ]);

			if($validator->fails())
	    	{
	    		$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> False,
					"Message"		=> "REQUIRED FIELD MISSING...",
				);
	    	}
	    	else
	    	{
				\DB::beginTransaction();
					$aHdArr	= $this->HdArr($request);
					$nRow	= $this->FeedbackTech->InsrtRecrd($aHdArr);
					if($nRow > 0)
					{
						$aRes = array(
							"ResponseCode"	=> 200,
							"Status"		=> True,
							"Message"		=> "FEEDBACK SEND SUCCESSFULLY...",
						);
					}
					else
					{
						$aRes = array(
							"ResponseCode"	=> 200,
							"Status"		=> False,
							"Message"		=> "WE HAVE SOME TECHNICIAL ISSUE, TRY AGAIN...",
						);	
					}
				\DB::commit();
			}
		}
		catch (\Exception $e)
    	{
    		\DB::rollback();
    		$aRes = array(
				"ResponseCode"	=> 400,
				"Message"		=>$e->getMessage()
			);
    	}
		return json_encode($aRes, JSON_PRETTY_PRINT);
	}

	public function HdArr($request)
    {
        $aComnArr = array(
            "lTech_IdNo"    => $request['lTechIdNo'],
            "sFdbck_Dtl"    => $request['sFdbckDtl'],
        );
        return $aComnArr;
    }
}
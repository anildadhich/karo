<?php

namespace App\Http\Controllers\TechnicianController;

use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use App\Model\TechOrder;
use App\Model\OrdHd;
use App\Model\OrdDt;
use App\Model\Technician;
use DB;

class OrderController extends Controller
{
	public function __construct() 
	{
		$this->OrdHd 	= new OrdHd;
		$this->OrdDt 	= new OrdDt;
		$this->TechOrder 	= new TechOrder;
		$this->Technician 	= new Technician;
		 // parent::__construct();
		header("Content-Type: application/json");
		$valid_passwords = array ("karo" => "026866326a9d1d2b23226e4e5317569f");
		$valid_users = array_keys($valid_passwords);

		$user = request()->server('PHP_AUTH_USER');
		$pass = request()->server('PHP_AUTH_PW');

		$validated = (in_array($user, $valid_users)) && ($pass == $valid_passwords[$user]);

		if (!$validated) {
		  header('WWW-Authenticate: Basic realm="My Realm"');
		  header('HTTP/1.0 401 Unauthorized');
		  $re = array(
		  	"status" 	=> false,
		  	"message"	=> "You're not authorized to access."
		  );
		  echo json_encode($re, JSON_PRETTY_PRINT);
		  die;
		 }
	}

	public function PndgOrd(Request $request)
	{
		try
		{
			$Validate = Validator::make($request->all(),[
							'lTechIdNo' => 'required',
						]);

			if($Validate->fails())
			{
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> False,
					"Message"		=> "REQUIRED FIELD MISSING..."
				);		
			}
			else
			{
				$lTechIdNo  	= $request['lTechIdNo'];
				$oGetOrd		= $this->TechOrder->PndgOrd($lTechIdNo);
				foreach($oGetOrd as $aRec)
				{
					$aRecSet[] = ["lOrdHdIdNo" => $aRec->lOrd_Hd_IdNo, "sOrdNo" => $aRec->sOrd_No, "sUserName" => $aRec->sUser_Name, "sUserAdrs" => $aRec->sUser_Adrs, "sCmntDtl" => $aRec->sCmnt_Dtl, "sCrtDtTm" => date('D, F d, Y h:i A', strtotime($aRec->sCrt_DtTm))];
				}

				if(isset($aRecSet))
				{
					$aRes = array(
						"ResponseCode"	=> 200,
						"Status"		=> True,
						"Message"		=> "PENDING ORDER LISTED SUCCESFULLY...",
						"Data"			=> $aRecSet
					);	
				}
				else
				{
					$aRes = array(
						"ResponseCode"	=> 200,
						"Status"		=> False,
						"Message"		=> "WE COUND NOT FOUND ANY ORDER..."
					);
				}
			}
		}
		catch (\Exception $e)
    	{
    		$aRes = array(
				"ResponseCode"	=> 400,
				"Message"		=> $e->getMessage()
			);
    	}
		return json_encode($aRes, JSON_PRETTY_PRINT);
	}

	public function ActnOrd(Request $request)
	{
		try
		{
			$Validate = Validator::make($request->all(),[
							'lOrdHdIdNo' => 'required',
							'lTechIdNo' => 'required',
							'nCurrStatus' => 'required',
						]);

			if($Validate->fails())
			{
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> False,
					"Message"		=> "REQUIRED FIELD MISSING..."
				);		
			}
			else
			{
				$oGetOrd	= $this->OrdHd->OrdDtl($request['lOrdHdIdNo']);
				$aHdArr 	= $this->ComnArr($request['nCurrStatus']);
				if($request['nCurrStatus'] == config('constant.TECH_ORD_STATUS.ACCEPTED'))
				{
					$this->AccptRjtArr($aHdArr);
					$this->AccptArr($aHdArr, $request['sArvdTime']);
                    $oGetTech 	= $this->Technician->Profile($request['lTechIdNo']);
					$sTitle = 'Order accepted';
					$sBody	= 'Your order ('.$oGetOrd->sOrd_No.') accetped by Technician:- '.$oGetTech->sTech_Name.' ('.$oGetTech->sTech_Mobile.') and arrived your location @ '.date('d F, Y h:i A', strtotime($request['sArvdTime'])).'';
				}
				else if($request['nCurrStatus'] == config('constant.TECH_ORD_STATUS.REJECTED'))
				{
					$this->AccptRjtArr($aHdArr);
				}
				else if($request['nCurrStatus'] == config('constant.TECH_ORD_STATUS.COLLECTED'))
				{
					$this->ClctArr($aHdArr, $request['sClctAmo']);
					$sTitle = 'Sample collected';
					$sBody	= 'Your order ('.$oGetOrd->sOrd_No.') sample collected by technician';
				}
				else
				{
					$this->DlvrdArr($aHdArr, $request['sTechFdbck']);
					$sTitle = 'Sample Received ';
					$sBody	= 'Your order ('.$oGetOrd->sOrd_No.') sample received';
				}

				$nUpDtRaw = $this->TechOrder->UpDtOrd($aHdArr, $request['lOrdHdIdNo'], $request['lTechIdNo']);
				if($nUpDtRaw > 0) 
				{
					if($request['nCurrStatus'] != config('constant.TECH_ORD_STATUS.REJECTED'))
					{
						Controller::SendNotification($oGetOrd->sDvic_Tokn, $sTitle, $sBody);
						Controller::SendSms($oGetOrd->sMobile_No, $sBody);
					}
					$aRes = array(
						"ResponseCode"	=> 200,
						"Status"		=> True,
						"Message"		=> "ORDER ".array_search($request['nCurrStatus'], config('constant.TECH_ORD_STATUS'))." SUCCESSFULLY..."
					);	
				}
				else
				{
					$aRes = array(
						"ResponseCode"	=> 200,
						"Status"		=> False,
						"Message"		=> "WE HAVE SOME TECHNICIAL ISSUES..."
					);		
				}
			}
		}
		catch (\Exception $e)
    	{
    		$aRes = array(
				"ResponseCode"	=> 400,
				"Message"		=> $e->getMessage()
			);
    	}
    	return json_encode($aRes, JSON_PRETTY_PRINT);
	}

	public function OrdLst(Request $request)
	{
		try 
		{
			$Validate = Validator::make($request->all(),[
							'lTechIdNo' => 'required',
						]);

			if($Validate->fails())
			{
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> False,
					"Message"		=> "REQUIRED FIELD MISSING..."
				);		
			}
			else
			{
				$lTechIdNo		= $request['lTechIdNo'];
				$oGetOnGoing 	= $this->TechOrder->OngoigOrd($lTechIdNo);
				foreach($oGetOnGoing as $aRec) 
				{

					$aRecSet['ONGOING'][] = ["lOrdHdIdNo" => $aRec->lOrd_Hd_IdNo, "sOrdNo" => $aRec->sOrd_No, "sUserName" => $aRec->sUser_Name, "sUserAdrs" => $aRec->sUser_Adrs, "nCurrStatus" => $aRec->nCurr_Status, "sCmntDtl" => $aRec->sCmnt_Dtl, "sCrtDtTm" => date('D, F d, Y h:i A', strtotime($aRec->sCrt_DtTm))];
				}

				$oGetDlvrd 		= $this->TechOrder->DlvrdOrd($lTechIdNo);
				foreach($oGetDlvrd as $aRec) 
				{

					$aRecSet['DELIVERED'][] = ["lOrdHdIdNo" => $aRec->lOrd_Hd_IdNo, "sOrdNo" => $aRec->sOrd_No, "sUserName" => $aRec->sUser_Name, "sUserAdrs" => $aRec->sUser_Adrs, "sCmntDtl" => $aRec->sCmnt_Dtl, "sCrtDtTm" => date('D, F d, Y h:i A', strtotime($aRec->sCrt_DtTm))];
				}

				if(isset($aRecSet))
				{
					$aRes = array(
						"ResponseCode"	=> 200,
						"Status"		=> True,
						"Message"		=> "ORDER LISTED SUCCESFULLY...",
						"Data"			=> $aRecSet
					);	
				}
				else
				{
					$aRes = array(
						"ResponseCode"	=> 200,
						"Status"		=> False,
						"Message"		=> "WE COUND NOT FOUND ANY ORDER..."
					);
				}
			}
		} 
		catch (\Exception $e) 
		{
			$aRes = array(
				"ResponseCode"	=> 400,
				"Message"		=> $e->getMessage()
			);
		}
		return json_encode($aRes, JSON_PRETTY_PRINT);
	}

	public function ComnArr($nCurrStatus)
	{
		$aComnArr = array(
			"nCurr_Status" 		=> $nCurrStatus,
		);
		return $aComnArr;
	}

	public function AccptRjtArr(&$aHdArr)
	{
		$aHdArr['sAcpt_Rjct_DtTm']	= date('Y-m-d H:i:s');
	}

	public function AccptArr(&$aHdArr, $sArvdTime)
	{
		$aHdArr['sArvd_Time'] = $sArvdTime;
	}

	public function ClctArr(&$aHdArr, $sClctAmo)
	{
		$aHdArr['sClct_Amo'] 	= $sClctAmo;
		$aHdArr['sClct_DtTm']	= date('Y-m-d H:i:s');
	}

	public function DlvrdArr(&$aHdArr, $sTechFdbck)
	{
		$aHdArr['sDlvrd_DtTm']	= date('Y-m-d H:i:s');
		$aHdArr['sTech_Fdbck']	= $sTechFdbck;
	}

	public function OrdDtl(Request $request)
	{
		try
		{
			$Validate = Validator::make($request->all(),[
							'lTechIdNo' => 'required',
							'lOrdHdIdNo' => 'required',
						]);

			if($Validate->fails())
			{
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> False,
					"Message"		=> "REQUIRED FIELD MISSING..."
				);		
			}
			else
			{
				$oGetOrd  	= $this->TechOrder->OrdDtl($request['lOrdHdIdNo'], $request['lTechIdNo']);
				$aRecSet	= ['sOrdNo' => $oGetOrd->sOrd_No, "sUserName" => $oGetOrd->sUser_Name, "sMobileNo" => $oGetOrd->sMobile_No, "sUserAdrs" => $oGetOrd->sUser_Adrs, "sTtlAmo" => $oGetOrd->sTtl_Amo, "sPayMod" => array_search($oGetOrd->nPay_Mod, config('constant.PAY_MTHD')), "sArvdTime" => date('d F, Y h:i A', strtotime($oGetOrd->sArvd_Time)), "sClctAmo" => !empty($oGetOrd->sClct_Amo) ? $oGetOrd->sClct_Amo : 0, "sCmntDtl" => $oGetOrd->sCmnt_Dtl, "sClctDtTm" => !empty($oGetOrd->sClct_DtTm) ? date('d F, Y h:i A', strtotime($oGetOrd->sClct_DtTm)) : '', 'nCurrStatus' => $oGetOrd->nCurr_Status];

				$oGetOrdDt	= $this->OrdDt->OrdDtLst($request['lOrdHdIdNo']);
				foreach($oGetOrdDt as $aRec)
				{
					$aRecSet['aTstLst'][] = ["sTstName" => $aRec->sItem_Name];
				}
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> True,
					"Message"		=> "DETAILS LISTED SUCCESFULLY...",
					"Data"			=> $aRecSet
				);
			}			
		}
		catch(\Exception $e)
		{
			$aRes = array(
				"ResponseCode"	=> 400,
				"Message"		=> $e->getMessage()
			);
		}
		return json_encode($aRes, JSON_PRETTY_PRINT);
	}
}
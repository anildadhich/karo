<?php

namespace App\Http\Controllers\ApiController;

use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use App\Model\Feedback;
use DB;

class FeedbackController extends Controller
{
	public function __construct() 
	{
		$this->Feedback 	= new Feedback;
		 // parent::__construct();
		header("Content-Type: application/json");
		$valid_passwords = array ("karo" => "026866326a9d1d2b23226e4e5317569f");
		$valid_users = array_keys($valid_passwords);

		$user = request()->server('PHP_AUTH_USER');
		$pass = request()->server('PHP_AUTH_PW');

		$validated = (in_array($user, $valid_users)) && ($pass == $valid_passwords[$user]);

		if (!$validated) {
		  header('WWW-Authenticate: Basic realm="My Realm"');
		  header('HTTP/1.0 401 Unauthorized');
		  $re = array(
		  	"status" 	=> false,
		  	"message"	=> "You're not authorized to access."
		  );
		  echo json_encode($re, JSON_PRETTY_PRINT);
		  die;
		}
	}

	public function SaveCntrl(Request $request)
	{
		try
		{
			$validator = Validator::make($request->all(), [
	            'lUserIdNo' 	=> 'required',
		        'sFdbckDtl' 	=> 'required',
	        ]);

			if($validator->fails())
	    	{
	    		$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> False,
					"Message"		=> "REQUIRED FIELD MISSING...",
				);
	    	}
	    	else
	    	{
				\DB::beginTransaction();
					$aHdArr			= $this->HdArr($request);
					$this->Feedback->InsrtRecrd($aHdArr);
					$aRes = array(
						"ResponseCode"	=> 200,
						"Status"		=> True,
						"Message"		=> "FEEDBACK SEND SUCCESSFULLY...",
					);
				\DB::commit();
			}
		}
		catch (\Exception $e)
    	{
    		\DB::rollback();
    		$aRes = array(
				"ResponseCode"	=> 400,
			);
    	}
		return json_encode($aRes, JSON_PRETTY_PRINT);
	}

	public function HdArr($request)
    {
        $aComnArr = array(
            "lUser_IdNo"    => $request['lUserIdNo'],
            "sFdbck_Dtl"    => $request['sFdbckDtl'],
            "nBlk_UnBlk"    => config('constant.STATUS.BLOCK'),
        );
        return $aComnArr;
    }

    public function FdbckLst()
    {
    	try
    	{	
    		$oGetFdbck = $this->Feedback->FdbckAll(config('constant.STATUS.UNBLOCK'));
    		foreach($oGetFdbck as $aRec)
    		{
    			$sUserPic = empty($aRec->sUser_Pic) ? 'default.png' : $aRec->sUser_Pic;
    			$aRecSet[] = ['sUserName' => $aRec->sUser_Name, 'sUserPic' => config('constant.PUBLIC_URL').'/profile_pic/'.$sUserPic, 'sFdbck_Dtl' => $aRec->sFdbck_Dtl];
    		}
    		if(isset($aRecSet))
    		{
    			$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> True,
					"Message"		=> "TOP STROTY LISTED SUCCESSFULLY...",
					"Data"			=> $aRecSet
				);
    		}
    		else
    		{
    			$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> False,
					"Message"		=> "WE COULD NOT FIND ANY TOP STROTY..."
				);	
    		}
    	}
    	catch (\Exception $e)
    	{
    		$aRes = array(
				"ResponseCode"	=> 400,
			);
    	}
		return json_encode($aRes, JSON_PRETTY_PRINT);
    }
}
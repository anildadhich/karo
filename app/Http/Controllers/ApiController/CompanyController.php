<?php

namespace App\Http\Controllers\ApiController;

use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use App\Model\Company;
use DB;

class CompanyController extends Controller
{
	public function __construct() 
	{
		$this->Company 	= new Company;
		 // parent::__construct();
		header("Content-Type: application/json");
		$valid_passwords = array ("karo" => "026866326a9d1d2b23226e4e5317569f");
		$valid_users = array_keys($valid_passwords);

		$user = request()->server('PHP_AUTH_USER');
		$pass = request()->server('PHP_AUTH_PW');

		$validated = (in_array($user, $valid_users)) && ($pass == $valid_passwords[$user]);

		if (!$validated) {
		  header('WWW-Authenticate: Basic realm="My Realm"');
		  header('HTTP/1.0 401 Unauthorized');
		  $re = array(
		  	"status" 	=> false,
		  	"message"	=> "You're not authorized to access."
		  );
		  echo json_encode($re, JSON_PRETTY_PRINT);
		  die;
		}
	}

	public function CompRec(Request $request)
	{
		try
		{
			$oGetComp	= $this->Company->CompRec();
			$aRes = array(
				"ResponseCode"	=> 200,
				"Status"		=> True,
				"Message"		=> "RECORD GOT SUCCESSFULLY...",
				"Data"			=> ["lCompMobile" => $oGetComp->lComp_Mobile, "nPoplrCatgOff" => $oGetComp->nPoplr_Catg_Off, "nCatgOff" => $oGetComp->nCatg_Off, "sLabName" => $oGetComp->sLab_Name, "sPhoneNo" => $oGetComp->sPhone_No, "sLabAddress" => $oGetComp->sLab_Address, "sWrkTm" => $oGetComp->sWrk_Tm, "sEmailId" => $oGetComp->sEmail_Id]
			);
		}
		catch (\Exception $e)
    	{
    		\DB::rollback();
    		$aRes = array(
				"ResponseCode"	=> 400,
			);
    	}
		return json_encode($aRes, JSON_PRETTY_PRINT);
	}
}
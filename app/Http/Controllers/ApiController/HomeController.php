<?php

namespace App\Http\Controllers\ApiController;

use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use App\Model\Package;
use App\Model\Category;
use App\Model\Feedback;
use App\Model\Company;
use App\Model\Test;
use App\Model\User;
use App\Model\Parameter;
use DB;

class HomeController extends Controller
{
	public function __construct() 
	{
		$this->Package 		= new Package;
		$this->Category 	= new Category;
		$this->Feedback 	= new Feedback;
		$this->Company 		= new Company;
		$this->Test 		= new Test;
		$this->User 		= new User;
		$this->Parameter 	= new Parameter;
		 // parent::__construct();
		header("Content-Type: application/json");
		$valid_passwords = array ("karo" => "026866326a9d1d2b23226e4e5317569f");
		$valid_users = array_keys($valid_passwords);

		$user = request()->server('PHP_AUTH_USER');
		$pass = request()->server('PHP_AUTH_PW');

		$validated = (in_array($user, $valid_users)) && ($pass == $valid_passwords[$user]);

		if (!$validated) {
		  header('WWW-Authenticate: Basic realm="My Realm"');
		  header('HTTP/1.0 401 Unauthorized');
		  $re = array(
		  	"status" 	=> false,
		  	"message"	=> "You're not authorized to access."
		  );
		  echo json_encode($re, JSON_PRETTY_PRINT);
		  die;
		}
	}

	public function HomeRec(Request $request)
	{
		try 
		{
			$oGetPckg	= $this->Package->PckgList(config('constant.STATUS.UNBLOCK'));
			if(count($oGetPckg) > 0)
			{
				foreach($oGetPckg As $aRes)
				{
					$nSaveUpTo  = round((($aRes->sActul_Amo-$aRes->sSale_Amo)/$aRes->sActul_Amo)*100);
					$aRecords['PckgLst'][] 	= ["lPckgIdNo" => $aRes->lPckg_IdNo, "sPckgName" => $aRes->sPckg_Name, "nPckGndr" => array_search($aRes->nPckg_Gndr, config('constant.GENDER')), "sAgeRtio" => $aRes->nAge_Frm."-".$aRes->nAge_To." Yr", "sActulAmo" => $aRes->sActul_Amo, "sSaleAmo" => $aRes->sSale_Amo, "sPckgDtl" => nl2br(substr($aRes->sPckg_Dtl, 0,150)), "nSaveUpTo" => $nSaveUpTo];
				}
			}

			$oGetTest	= $this->Test->TstLst(config('constant.STATUS.UNBLOCK'), config('constant.TEST_CATG.POPULAR'));
			if(count($oGetTest) > 0)
			{
				foreach($oGetTest as $nKey => $aRes)
				{
				    $aTstPrmt = NULL;
					$oTstPrmtr = $this->Parameter->PrmtrLst($aRes->lTst_IdNo);
					foreach($oTstPrmtr as $aRecPrmtr)
					{
						$aTstPrmt[] = ["sPrmtrName" => $aRecPrmtr->sPrmtr_Name, "sPrmtrDtl" => $aRecPrmtr->sPrmtr_Dtl];
					}
					$sTstPic = empty($aRes->sTst_Pic) ? 'default.png' : $aRes->sTst_Pic;
					$nSaveUpTo  = round((($aRes->sActul_Amo-$aRes->sSale_Amo)/$aRes->sActul_Amo)*100);
					$aRecords['PopularTst'][] = ["lTstIdNo" => $aRes->lTst_IdNo, "sTstName" => $aRes->sTst_Name, "nTstGndr" => array_search($aRes->nTst_Gndr, config('constant.GENDER'), True), "sAgeRtio" => $aRes->nAge_Frm."-".$aRes->nAge_To." Yr", "sActulAmo" => $aRes->sActul_Amo, "sSaleAmo" => $aRes->sSale_Amo, "sTstDtl" => nl2br($aRes->sTst_Dtl), "sTstPic" => config('constant.PUBLIC_URL').'/test_pic/'.$sTstPic, "nSaveUpTo" => $nSaveUpTo, "sTstType" => $aRes->sTst_Type, "sTstInfo" => $aRes->sTst_Info, "sRptDelv" => $aRes->sRpt_Delv, "nTtlPrmtr" => count($oTstPrmtr), "aTstPrmt" => $aTstPrmt];
				}
			}

			$oGetCatg	= $this->Category->CatgList(config('constant.STATUS.UNBLOCK'));
			if(count($oGetCatg) > 0)
			{
				foreach($oGetCatg as $aRec)
				{

					$aRecords['CatgLst'][] = ["lCatgIdNo" => $aRec->lCatg_IdNo, "sCatgName" => $aRec->sCatg_Name, "sIcnName" => config('constant.PUBLIC_URL').'/category_icon/'.$aRec->sIcn_Name];
				}
			}

			$oGetFdbck = $this->Feedback->FdbckAll(config('constant.STATUS.UNBLOCK'));
			if(count($oGetFdbck) > 0)
			{
				foreach($oGetFdbck as $aRec)
				{
					$sUserPic = empty($aRec->sUser_Pic) ? 'default.png' : $aRec->sUser_Pic;
	    			$aRecords['TopStrs'][] = ['sUserName' => $aRec->sUser_Name, 'sUserPic' => config('constant.PUBLIC_URL').'/profile_pic/'.$sUserPic, 'sFdbck_Dtl' => $aRec->sFdbck_Dtl];	
	    		}
			}

			$oGetComp	= $this->Company->CompRec();
			$aRecords['CompData'] = ["lCompMobile" => $oGetComp->lComp_Mobile, "nPoplrCatgOff" => $oGetComp->nPoplr_Catg_Off, "nCatgOff" => $oGetComp->nCatg_Off, "sLabName" => $oGetComp->sLab_Name, "sPhoneNo" => $oGetComp->sPhone_No, "sLabAddress" => $oGetComp->sLab_Address, "sWrkTm" => $oGetComp->sWrk_Tm, "sEmailId" => $oGetComp->sEmail_Id];

			$lUserIdNo = $request['lUserIdNo'];
			$aProfileData	= $this->User->Profile($lUserIdNo);
			$aRecords['UserProfile'] = $aProfileData;

 			$oGetWalt = $this->User->GetWalt($lUserIdNo);
 			$aRecords['WaltAmo'] = isset($oGetWalt) ? $oGetWalt[0]->TtlWallet-$oGetWalt[0]->TtlRedm : 0;

			$aRes = array(
				"ResponseCode"	=> 200,
				"Status"		=> True,
				"Message"		=> "DETAIL GET SUCCESSFULLY...",
				"Data"			=> $aRecords
			);
		} 
		catch (\Exception $e)
    	{
    		$aRes = array(
				"ResponseCode"	=> 400,
				"Message"		=> $e->getMessage(),
				
			);
    	}
		return json_encode($aRes, JSON_PRETTY_PRINT);
	}
}
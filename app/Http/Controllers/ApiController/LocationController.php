<?php

namespace App\Http\Controllers\ApiController;

use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use App\Model\State;
use App\Model\City;
use DB;

class LocationController extends Controller
{
	public function __construct() 
	{
		$this->State 	= new State;
		$this->City 	= new City;
		 // parent::__construct();
		header("Content-Type: application/json");
		$valid_passwords = array ("karo" => "026866326a9d1d2b23226e4e5317569f");
		$valid_users = array_keys($valid_passwords);

		$user = request()->server('PHP_AUTH_USER');
		$pass = request()->server('PHP_AUTH_PW');

		$validated = (in_array($user, $valid_users)) && ($pass == $valid_passwords[$user]);

		if (!$validated) {
		  header('WWW-Authenticate: Basic realm="My Realm"');
		  header('HTTP/1.0 401 Unauthorized');
		  $re = array(
		  	"status" 	=> false,
		  	"message"	=> "You're not authorized to access."
		  );
		  echo json_encode($re, JSON_PRETTY_PRINT);
		  die;
		}
	}

	public function StateList()
	{
		try
		{
			$oGetState	= $this->State->GetState();
			foreach($oGetState As $aRes)
			{
				$aRecord[] = ["lStateIdNo" => $aRes->lState_IdNo, "lStateName" => $aRes->sState_Name];
			}
			if(isset($aRecord))
			{
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> True,
					"Message"		=> "STATE LISTED SUCCESSFULLY...",
					"Data"			=> $aRecord
				);
			}
			else
			{
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> False,
					"Message"		=> "WE COULD NOT FOUND ANY STATE..."
				);
			}
		}
		catch (\Exception $e)
		{
			$aRes = array(
				"ResponseCode"	=> 400,
			);
		}
		return json_encode($aRes, JSON_PRETTY_PRINT);
	}

	public function CityList(Request $request)
	{
		try
		{
			$lStateIdNo = $request['lStateIdNo'];
			$oGetCity	= $this->City->GetCity($lStateIdNo);
			foreach($oGetCity As $aRes)
			{
				$aRecord[] = ["lCityIdNo" => $aRes->lCity_IdNo, "lCityName" => $aRes->sCity_Name];
			}
			if(isset($aRecord))
			{
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> True,
					"Message"		=> "CITY LISTED SUCCESSFULLY...",
					"Data"			=> $aRecord
				);
			}
			else
			{
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> False,
					"Message"		=> "WE COULD NOT FOUND ANY CITY..."
				);
			}
		}
		catch (\Exception $e)
		{
			$aRes = array(
				"ResponseCode"	=> 400,
			);
		}
		return json_encode($aRes, JSON_PRETTY_PRINT);
	}
}
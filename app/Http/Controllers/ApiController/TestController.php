<?php

namespace App\Http\Controllers\ApiController;

use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use App\Model\Test;
use App\Model\Parameter;
use DB;

class TestController extends Controller
{
	public function __construct() 
	{
		$this->Test 	= new Test;
		$this->Parameter 	= new Parameter;
		 // parent::__construct();
		header("Content-Type: application/json");
		$valid_passwords = array ("karo" => "026866326a9d1d2b23226e4e5317569f");
		$valid_users = array_keys($valid_passwords);

		$user = request()->server('PHP_AUTH_USER');
		$pass = request()->server('PHP_AUTH_PW');

		$validated = (in_array($user, $valid_users)) && ($pass == $valid_passwords[$user]);

		if (!$validated) {
		  header('WWW-Authenticate: Basic realm="My Realm"');
		  header('HTTP/1.0 401 Unauthorized');
		  $re = array(
		  	"status" 	=> false,
		  	"message"	=> "You're not authorized to access."
		  );
		  echo json_encode($re, JSON_PRETTY_PRINT);
		  die;
		}
	}

	public function PopTstList()
	{
		try
		{
			$oGetTest	= $this->Test->TstLst(config('constant.STATUS.UNBLOCK'), config('constant.TEST_CATG.POPULAR'));
			foreach($oGetTest as $nKey => $aRes)
			{
				$oTstPrmtr = $this->Parameter->PrmtrLst($aRes->lTst_IdNo);
				$sTstPic = empty($aRes->sTst_Pic) ? 'default.png' : $aRes->sTst_Pic;
				$nSaveUpTo  = round((($aRes->sActul_Amo-$aRes->sSale_Amo)/$aRes->sActul_Amo)*100);
				$aRecords[] = ["lTstIdNo" => $aRes->lTst_IdNo, "sTstName" => $aRes->sTst_Name, "nTstGndr" => array_search($aRes->nTst_Gndr, config('constant.GENDER'), True), "sAgeRtio" => $aRes->nAge_Frm."-".$aRes->nAge_To." Yr", "sActulAmo" => $aRes->sActul_Amo, "sSaleAmo" => $aRes->sSale_Amo, "sTstDtl" => nl2br($aRes->sTst_Dtl), "sTstPic" => config('constant.PUBLIC_URL').'/test_pic/'.$sTstPic, "nSaveUpTo" => $nSaveUpTo, "sTstType" => $aRes->sTst_Type, "sTstInfo" => $aRes->sTst_Info, "sRptDelv" => $aRes->sRpt_Delv, 'nTtlPrmtr' => count($oTstPrmtr)];
				foreach($oTstPrmtr as $aRecPrmtr)
				{
					$aRecords[$nKey]['aTstPrmt'][] = ["sPrmtrName" => $aRecPrmtr->sPrmtr_Name, "sPrmtrDtl" => $aRecPrmtr->sPrmtr_Dtl];
				}
			}
			if(isset($aRecords))
			{
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> True,
					"Message"		=> "POPULAR TEST LISTED SUCCESSFULLY...",
					"Data"			=> $aRecords
				);
			}
			else
			{
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> False,
					"Message"		=> "WE COULD NOT FOUND ANY TEST..."
				);
			}
		}
		catch (\Exception $e)
		{
			$aRes = array(
				"ResponseCode"	=> 400,
			);
		}
		return json_encode($aRes, JSON_PRETTY_PRINT);
	}

	public function SrchTst()
	{
		try
		{
			$oGetTest = $this->Test->SrchTst();
			foreach($oGetTest as $nKey => $aRec)
			{
				$oTstPrmtr = $this->Parameter->PrmtrLst($aRec->lTst_IdNo);
				$nSaveUpTo  = round((($aRec->sActul_Amo-$aRec->sSale_Amo)/$aRec->sActul_Amo)*100);
				$aRecSet[] = ["lTstIdNo" => $aRec->lTst_IdNo, "sTstName" => $aRec->sTst_Name, "nTstGndr" => array_search($aRec->nTst_Gndr, config('constant.GENDER')), "sAgeRtio" => $aRec->nAge_Frm."-".$aRec->nAge_To." Yr", "sActulAmo" => $aRec->sActul_Amo, "sSaleAmo" => $aRec->sSale_Amo, "sTstDtl" => nl2br($aRec->sTst_Dtl), "nSaveUpTo" => $nSaveUpTo, "sTstType" => $aRec->sTst_Type, "sTstInfo" => $aRec->sTst_Info, "sRptDelv" => $aRec->sRpt_Delv, 'nTtlPrmt' => count($oTstPrmtr)];
				foreach($oTstPrmtr as $aRecPrmtr)
				{
					$aRecSet[$nKey]['aTstPrmt'][] = ["sPrmtrName" => $aRecPrmtr->sPrmtr_Name, "sPrmtrDtl" => $aRecPrmtr->sPrmtr_Dtl];
				}
			}
			if(isset($aRecSet))
			{
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> True,
					"Message"		=> "TEST LISTED SUCCESSFULLY...",
					"Data"			=> $aRecSet
				);
			}
			else
			{
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> False,
					"Message"		=> "TEST NOT FOUND..."
				);	
			}
		}
		catch (\Exception $e)
		{
			$aRes = array(
				"ResponseCode"	=> 400,
				"Message"		=> $e->getMessage()
			);
		}
		return json_encode($aRes, JSON_PRETTY_PRINT);
	}

	public function TstPrmtr(Request $request)
	{
		try
		{
			$lTstIdNo = $request['lTstIdNo'];
			if(empty($lTstIdNo))
			{
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> False,
					"Message"		=> "UNAUTHORIZED ACCESS..."
				);	
			}
			else
			{
				$oGetTst 	= $this->Test->TstDtl($lTstIdNo);
				$oGetPrmtr	= $this->Parameter->PrmtrLst($lTstIdNo);
				$aRecSet = ["sTstName" => $oGetTst->sTst_Name, 'nTtlPrmtr' => count($oGetPrmtr)];
				foreach($oGetPrmtr as $aRec)
				{
					$aRecSet['PrmtrLst'][] = ["sPrmtrName" => $aRec->sPrmtr_Name, "sPrmtrDtl" => $aRec->sPrmtr_Dtl];
				}

				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> True,
					"Message"		=> "PARAMETER LISTED SUCCESSFYLLY...",
					"Data"			=> $aRecSet
				);	
			}
		}
		catch (\Exception $e)
		{
			$aRes = array(
				"ResponseCode"	=> 400,
			);
		}
		return json_encode($aRes, JSON_PRETTY_PRINT);
	}
}
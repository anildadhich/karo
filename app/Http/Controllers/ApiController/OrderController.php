<?php

namespace App\Http\Controllers\ApiController;

use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use App\Model\OrdHd;
use App\Model\OrdDt;
use App\Model\Report;
use App\Model\User;
use DB;

class OrderController extends Controller
{
	public function __construct() 
	{
		$this->OrdHd 	= new OrdHd;
		$this->OrdDt 	= new OrdDt;
		$this->Report 	= new Report;
		$this->User 	= new User;
		 // parent::__construct();
		header("Content-Type: application/json");
		$valid_passwords = array ("karo" => "026866326a9d1d2b23226e4e5317569f");
		$valid_users = array_keys($valid_passwords);

		$user = request()->server('PHP_AUTH_USER');
		$pass = request()->server('PHP_AUTH_PW');

		$validated = (in_array($user, $valid_users)) && ($pass == $valid_passwords[$user]);

		if (!$validated) {
		  header('WWW-Authenticate: Basic realm="My Realm"');
		  header('HTTP/1.0 401 Unauthorized');
		  $re = array(
		  	"status" 	=> false,
		  	"message"	=> "You're not authorized to access."
		  );
		  echo json_encode($re, JSON_PRETTY_PRINT);
		  die;
		}
	}

	public function SaveCntrl(Request $request)
	{
		try
		{
			$aOrdData	= json_decode($request['jOrdData']);
			\DB::beginTransaction();
				$aHdArr			= $this->HdArr($aOrdData);
				$ySaveStatus	= $this->OrdHd->InsrtRecrd($aHdArr, $lOrdHdIdNo);
				if(!empty($lOrdHdIdNo))
				{
					foreach($aOrdData->aCartData as $aDtRec) 
					{
						$aDtArr	= $this->DtArr($aDtRec, $lOrdHdIdNo);
						$this->OrdDt->InsrtRecrd($aDtArr);
					}
				}
			\DB::commit();
			$oGetUser = $this->User->GetUser($aOrdData->lUserIdNo);
			$sTitle = 'Order Placed';
			$sBody	= 'Your Order has been Booked';
			$sMessage = 'You have an new order : '.$aOrdData->sOrdNo;
			Controller::SendNotification($oGetUser->sDvic_Tokn, $sTitle, $sBody);
			Controller::SendSms('7230007555',$sMessage);
			Controller::SendSms('8209485383',$sMessage);
			$aRes = array(
				"ResponseCode"	=> 200,
				"Status"		=> True,
				"Message"		=> 'ORDER PLACED SUCCESSFULLY...',
				"Data"			=> ['lOrdHdIdNo' => $lOrdHdIdNo]
			);
		}
		catch (\Exception $e)
    	{
    		\DB::rollback();
    		$aRes = array(
				"ResponseCode"	=> 400,
				"Message"		=> $e->getMessage()
			);
    	}
		return json_encode($aRes, JSON_PRETTY_PRINT);
	}

	public function HdArr($aOrdData)
    {
        $aComnHdr = array(
            "sOrd_No"       => $aOrdData->sOrdNo,
            "lUser_IdNo"    => $aOrdData->lUserIdNo,
            "sUser_Name"    => $aOrdData->sUserName,
            "sMobile_No"    => $aOrdData->sMobileNo,
            "sUser_Adrs"    => $aOrdData->sUserAdrs,
            "sSub_Amo"      => $aOrdData->sSubAmo,
            "sDis_Per"      => $aOrdData->sDisPer,
            "sWlt_Amo"      => $aOrdData->sWltAmo,
            "sTtl_Amo"      => $aOrdData->sTtlAmo,
            "nPay_Mod"      => $aOrdData->nPayMod,
            "sTrans_Id"     => $aOrdData->sTransId,
            "nOrd_Type"     => $aOrdData->nOrdType,
            "nOrd_Status"   => config('constant.ORD_STATUS.ONGOING'),
        );       
        return $aComnHdr; 
    }

    public function DtArr($aDtRec, $lOrdHdIdNo)
    {
        $aComnHdr = array(
            "lOrd_Hd_IdNo"    => $lOrdHdIdNo,
            "lItm_IdNo"       => $aDtRec->lItmIdNo,
            "nItem_Type"      => $aDtRec->nItemTye,
            "sItem_Amo"       => $aDtRec->sItemAmo,
            "sItem_Dtl"       => substr($aDtRec->sItemDtl,0,149),
            "sItem_Name"      => $aDtRec->sItemName,
        );       
        return $aComnHdr; 
    }

	public function OrdLst(Request $request)
	{
		try
		{
			$lUserIdNo = $request['lUserIdNo'];
			if(!empty($lUserIdNo) && isset($lUserIdNo))
			{
				$oGetOrd = $this->OrdHd->OrdLst($lUserIdNo, config('constant.ORD_STATUS.ONGOING'));
				foreach ($oGetOrd as $aRec) 
				{
					$sTstDtl 	= '';
					$oGetOrdDt 	= $this->OrdDt->OrdDtLst($aRec->lOrd_Hd_IdNo);
					foreach($oGetOrdDt as $aRecDt) 
					{
						$sTstDtl .= $aRecDt->sItem_Name.", ";
					}

					$aRecSet['ONGOING'][] = ['lOrdHdIdNo' => $aRec->lOrd_Hd_IdNo, 'sOrdNo' => $aRec->sOrd_No, "sTstDtl" => substr($sTstDtl, 0, -2), "sCrtDtTm" => date('D, F d, Y h:i A', strtotime($aRec->sCrt_DtTm))];
				}
				$oGetOrd = $this->OrdHd->OrdLst($lUserIdNo, config('constant.ORD_STATUS.COMPLETE'));
				foreach ($oGetOrd as $aRec) 
				{
					$sTstDtl 	= '';
					$oGetOrdDt 	= $this->OrdDt->OrdDtLst($aRec->lOrd_Hd_IdNo);
					foreach($oGetOrdDt as $aRecDt) 
					{
						$sTstDtl .= $aRecDt->sItem_Name.", ";
					}

					$aRecSet['COMPLETE'][] = ['lOrdHdIdNo' => $aRec->lOrd_Hd_IdNo, 'sOrdNo' => $aRec->sOrd_No, "sTstDtl" => substr($sTstDtl, 0, -2), "sCrtDtTm" => date('D, F d, Y h:i A', strtotime($aRec->sCrt_DtTm))];
				}
				$oGetOrd = $this->OrdHd->OrdLst($lUserIdNo, config('constant.ORD_STATUS.CANCELLED'));
				foreach ($oGetOrd as $aRec) 
				{
					$sTstDtl 	= '';
					$oGetOrdDt 	= $this->OrdDt->OrdDtLst($aRec->lOrd_Hd_IdNo);
					foreach($oGetOrdDt as $aRecDt) 
					{
						$sTstDtl .= $aRecDt->sItem_Name.", ";
					}

					$aRecSet['CANCELLED'][] = ['lOrdHdIdNo' => $aRec->lOrd_Hd_IdNo, 'sOrdNo' => $aRec->sOrd_No, "sTstDtl" => substr($sTstDtl, 0, -2), "sCrtDtTm" => date('D, F d, Y h:i A', strtotime($aRec->sCrt_DtTm))];
				}
				if(isset($aRecSet))
				{
					$aRes = array(
						"ResponseCode"	=> 200,
						"Status"		=> True,
						"Message"		=> "ORDER LISTED SUCCESSFULLY...",
						"Data"			=> $aRecSet
					);			
				}
				else
				{
					$aRes = array(
						"ResponseCode"	=> 200,
						"Status"		=> False,
						"Message"		=> "NO ORDER FOUND..."
					);				
				}
			}
			else
			{
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> False,
					"Message"		=> "UNAUTHORIZED ACCESS..."
				);	
			}
		}
		catch (\Exception $e)
    	{
    		$aRes = array(
				"ResponseCode"	=> 400,
			);
    	}
		return json_encode($aRes, JSON_PRETTY_PRINT);
	}

	public function OrdHstry(Request $request)
	{
		try
		{
			$lOrdHdIdNo = $request['lOrdHdIdNo'];
			if(!empty($lOrdHdIdNo) && isset($lOrdHdIdNo))
			{
				$oGetOrdHd = $this->OrdHd->OrdDtl($lOrdHdIdNo);
				$aRecSet   = ['sOrdNo' => $oGetOrdHd->sOrd_No, "sUserName" => $oGetOrdHd->sUser_Name, "sUserAdrs" => $oGetOrdHd->sUser_Adrs, "sMobileNo" => $oGetOrdHd->sMobile_No, "sSubAmo" => $oGetOrdHd->sSub_Amo, "sDisPer" => $oGetOrdHd->sDis_Per, "sWltAmo" => $oGetOrdHd->sWlt_Amo, "sTtlAmo" => $oGetOrdHd->sTtl_Amo, "nPayMod" => array_search($oGetOrdHd->nPay_Mod, config('constant.PAY_MTHD')), "nOrdType" => array_search($oGetOrdHd->nOrd_Type, config('constant.CLCT_STATUS')), "sCrtDtTm" => date('D, F d, Y h:i A', strtotime($oGetOrdHd->sCrt_DtTm))];

				$oGetOrdDt = $this->OrdDt->OrdDtl($lOrdHdIdNo);

				foreach ($oGetOrdDt as $aRec) 
				{
					$aRecSet['ItemList'][] = ["sItemName" => $aRec->sItem_Name, "sItemAmo" => $aRec->sItem_Amo];
				}
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> True,
					"Message"		=> "ORDER DETAIL GOT SUCCESSFULLY...",
					"Data"			=> $aRecSet
				);
			}
			else
			{
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> False,
					"Message"		=> "UNAUTHORIZED ACCESS..."
				);	
			}
		}
		catch (\Exception $e)
    	{
    		$aRes = array(
				"ResponseCode"	=> 400,
			);
    	}
		return json_encode($aRes, JSON_PRETTY_PRINT);
	}

	public function RprtLst(Request $request)
	{
		try 
		{
			$lUserIdNo = $request['lUserIdNo'];
			if(empty($lUserIdNo))
			{
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> False,
					"Message"		=> "UNAUTHORIZED ACCESS..."
				);		
			}
			else
			{
				$oGetRpt = $this->Report->RptLst($lUserIdNo);
				if(count($oGetRpt))
				{
					foreach($oGetRpt as $nKeyRpt => $aRec)
					{
						$oGetOrdDt = $this->OrdDt->OrdDtLst($aRec->lOrd_Hd_IdNo);
						$aRecSet[] = ["sOrdNo" => $aRec->sOrd_No, "sUserName" => $aRec->sUser_Name, "sRprtFile" => config('constant.PUBLIC_URL').'/user_report/'.$aRec->sRprt_File, "sCrtDtTm" => date('D, F d, Y h:i A', strtotime($aRec->sCrt_DtTm))];
						foreach($oGetOrdDt as $nDtKey => $aRecDt)
						{
							$aRecSet[$nKeyRpt]['TstLst'][] = ['sTstName' => $aRecDt->sItem_Name];
						}
					}
					$aRes = array(
						"ResponseCode"	=> 200,
						"Status"		=> True,
						"Message"		=> "REPORT LITSED SUCCESSFULLY...",
						"Data"			=> $aRecSet
					);				
				}
				else
				{
					$aRes = array(
						"ResponseCode"	=> 200,
						"Status"		=> False,
						"Message"		=> "REPORT NOT FOUND..."
					);					
				}
			}
		} 
		catch (\Exception $e)
    	{
    		$aRes = array(
				"ResponseCode"	=> 400,
			);
    	}
		return json_encode($aRes, JSON_PRETTY_PRINT);
	}
}
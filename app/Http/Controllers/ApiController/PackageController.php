<?php

namespace App\Http\Controllers\ApiController;

use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use App\Model\Package;
use App\Model\Test;
use App\Model\Parameter;
use DB;

class PackageController extends Controller
{
	public function __construct() 
	{
		$this->Package 		= new Package;
		$this->Test 		= new Test;
		$this->Parameter 	= new Parameter;


		header("Content-Type: application/json");
		$valid_passwords = array ("karo" => "026866326a9d1d2b23226e4e5317569f");
		$valid_users = array_keys($valid_passwords);

		$user = request()->server('PHP_AUTH_USER');
		$pass = request()->server('PHP_AUTH_PW');

		$validated = (in_array($user, $valid_users)) && ($pass == $valid_passwords[$user]);

		if (!$validated) {
		  header('WWW-Authenticate: Basic realm="My Realm"');
		  header('HTTP/1.0 401 Unauthorized');
		  $re = array(
		  	"status" 	=> false,
		  	"message"	=> "You're not authorized to access."
		  );
		  echo json_encode($re, JSON_PRETTY_PRINT);
		  die;
		}
	}

	public function PckgList()
	{
		try
		{
			$oGetPckg	= $this->Package->PckgList(config('constant.STATUS.UNBLOCK'));
			if(count($oGetPckg) > 0)
			{
				foreach($oGetPckg As $aRes)
				{
					$nSaveUpTo  = round((($aRes->sActul_Amo-$aRes->sSale_Amo)/$aRes->sActul_Amo)*100);
					$aRecord[] 	= ["lPckgIdNo" => $aRes->lPckg_IdNo, "sPckgName" => $aRes->sPckg_Name, "nPckGndr" => array_search($aRes->nPckg_Gndr, config('constant.GENDER')), "sAgeRtio" => $aRes->nAge_Frm."-".$aRes->nAge_To." Yr", "sActulAmo" => $aRes->sActul_Amo, "sSaleAmo" => $aRes->sSale_Amo, "sPckgDtl" => nl2br(substr($aRes->sPckg_Dtl, 0,100)), "nSaveUpTo" => $nSaveUpTo];
				}

				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> True,
					"Message"		=> "PACKAGE LISTED SUCCESSFULLY...",
					"Data"			=> $aRecord
				);	
			}
			else
			{
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> False,
					"Message"		=> "WE COULD NOT FOND ANY PACKAGE..."
				);
			}
		}
		catch (\Exception $e)
    	{
    		$aRes = array(
				"ResponseCode"	=> 400,
			);
    	}
		return json_encode($aRes, JSON_PRETTY_PRINT);
	}

	public function PckgDlt(Request $request)
	{
		$lPckgIdNo = $request['lPckgIdNo'];
		if(!isset($lPckgIdNo) && empty($lPckgIdNo))
		{
			$aRes = array(
				"ResponseCode"	=> 200,
				"Status"		=> False,
				"Message"		=> "UNAUTHOROIZED ACCESS..."
			);
		}
		else
		{
			try
			{
				$oGetPckg = $this->Package->PckgDtl($lPckgIdNo);

				$nTtlTst = 0;
				$sTstIds = explode(",", $oGetPckg->sTst_Ids);
				$aRecord = array(
					"sPckgName"	=> $oGetPckg->sPckg_Name, 
					"sPckgDtl"	=> nl2br($oGetPckg->sPckg_Dtl),
					"nTtlTst"	=> count(explode(",", $oGetPckg->sTst_Ids))
				);	
				foreach ($sTstIds as $lTstIdNo) 
				{
					$oGetTst 	= $this->Test->TstDtl($lTstIdNo);
					if(isset($oGetTst) && !empty($oGetTst->lTst_IdNo))
					{
						$oGetPrmtr	= $this->Parameter->PrmtrLst($lTstIdNo);
						$aRecord['TstList'][] = array(
							"lTstIdNo"	=> $oGetTst->lTst_IdNo, 
							"sTstName"	=> $oGetTst->sTst_Name, 
							"nTtlPrmtr"	=> count($oGetPrmtr), 
						);
					}
				}			

				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> True,
					"Message"		=> "PACKAGE DETAIL LISTED SUCCESSFULLY...",
					"Data"			=> $aRecord
				);
			}
			catch (\Exception $e)
	    	{
	    		$aRes = array(
					"ResponseCode"	=> 400,
				);
	    	}
	    	return json_encode($aRes, JSON_PRETTY_PRINT);
		}
	}
}
<?php

namespace App\Http\Controllers\ApiController;

use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use App\Model\Member;
use DB;

class MemberController extends Controller
{
	public function __construct() 
	{
		$this->Member 	= new Member;
		 // parent::__construct();
		header("Content-Type: application/json");
		$valid_passwords = array ("karo" => "026866326a9d1d2b23226e4e5317569f");
		$valid_users = array_keys($valid_passwords);

		$user = request()->server('PHP_AUTH_USER');
		$pass = request()->server('PHP_AUTH_PW');

		$validated = (in_array($user, $valid_users)) && ($pass == $valid_passwords[$user]);

		if (!$validated) {
		  header('WWW-Authenticate: Basic realm="My Realm"');
		  header('HTTP/1.0 401 Unauthorized');
		  $re = array(
		  	"status" 	=> false,
		  	"message"	=> "You're not authorized to access."
		  );
		  echo json_encode($re, JSON_PRETTY_PRINT);
		  die;
		}
	}

	public function RelList()
	{
		try
		{
			foreach (config('constant.MEMBER') as $sRelName => $lRelIdNo) 
			{
				$aRecSet[] = ["lRelIdNo" => $lRelIdNo, "sRelName" => $sRelName];
			}

			$aRes = array(
				"ResponseCode"	=> 200,
				"Status"		=> True,
				"Message"		=> "RELATION LISTED SUCCESSFULLY...",
				"Data"			=> $aRecSet
			);
		}
		catch (\Exception $e)
    	{
    		$aRes = array(
				"ResponseCode"	=> 400,
			);
    	}
    	return json_encode($aRes, JSON_PRETTY_PRINT);
	}

	public function SaveCntrl(Request $request)
	{
		try
		{
			$validator = Validator::make($request->all(), [
	            'sMembrName' 	=> 'required',
		        'sMembrMobile' 	=> 'required',
		        'nMembrAge' 	=> 'required',
		        'lRelIdNo' 		=> 'required',
		        'sAdrsLine'		=> 'required',
				'sAdrsArea'		=> 'required',
				'sStateName'	=> 'required',
				'sCityName'		=> 'required',
				'sPinCod'		=> 'required',
	        ]);

	        if($validator->fails())
	    	{
	    		$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> False,
					"Message"		=> "REQUIRED FIELD MISSING...",
				);
	    	}
	    	else
	    	{
				\DB::beginTransaction();
					$lMembrIdNo = isset($request['lMembrIdNo']) ? $request['lMembrIdNo'] : 0; 
					$aHdArr  = $this->HdArr($request);
	        		if($lMembrIdNo == 0)
	        		{
	        			$this->InsArr($aHdArr, $request['lUserIdNo']);
	                	$ySaveStatus = $this->Member->InsrtRecrd($aHdArr);

	                    $aRes = array(
							"ResponseCode"	=> 200,
							"Status"		=> True,
							"Message"		=> "MEMBER SAVED SUCCESSFULLY..."
						);
	        		}
	        		else
	        		{
	        			$this->Member->UpDtRecrd($aHdArr, $lMembrIdNo);	
	                	$aRes = array(
							"ResponseCode"	=> 200,
							"Status"		=> True,
							"Message"		=> "MEMBER UPDATED SUCCESSFULLY..."
						);
	        		}
				\DB::commit();
			}
		}
		catch (\Exception $e)
    	{
    		\DB::rollBack();
    		$aRes = array(
				"ResponseCode"	=> 400,
			);
    	}
		return json_encode($aRes, JSON_PRETTY_PRINT);
	}

	public function HdArr($request)
    {
    	$aComnArr = array(
            "sMembr_Name"    	=> $request['sMembrName'],
			"sMembr_Mobile"	 	=> $request['sMembrMobile'],
			"nMembr_Age"	 	=> $request['nMembrAge'],
			"lRel_IdNo"	     	=> $request['lRelIdNo'],
			"sAdrs_Line"    	=> $request['sAdrsLine'],
			"sAdrs_Area"		=> $request['sAdrsArea'],
			"sState_Name"		=> $request['sStateName'],
			"sCity_Name"		=> $request['sCityName'],
			"sPin_Cod"			=> $request['sPinCod'],
		);
		return $aComnArr;
    }

    public function InsArr(&$aHdArr, $lUserIdNo)
    {
    	$aHdArr['lUser_IdNo']  	= $lUserIdNo;
        $aHdArr['nDel_Status']  = config('constant.DEL_STATUS.NON_DELETED');
    }

	public function DelMembr(Request $request)
	{
		try
		{
			\DB::beginTransaction();
				$yDelStatus = $this->Member->DelMembr($request['lMembrIdNo']);
				if($yDelStatus)
				{
					$aRes = array(
						"ResponseCode"	=> 200,
						"Status"		=> True,
						"Message"		=> "MEMBER DELETED SUCCESSFULLY...",
					);
				}
				else
				{
					$aRes = array(
						"ResponseCode"	=> 200,
						"Status"		=> False,
						"Message"		=> "WE HAVE SOME TECHNICIAL ISSUES..."
					);
				}
			\DB::commit();
		}
		catch (\Exception $e)
    	{
    		\DB::rollBack();
    		$aRes = array(
				"ResponseCode"	=> 400,
			);
    	}
		return json_encode($aRes, JSON_PRETTY_PRINT);
	}

	public function GetMembr(Request $request)
	{
		try
		{
			$oGetMembr = $this->Member->GetMembr($request['lMembrIdNo']);
			if(isset($oGetMembr) && !empty($oGetMembr->lMembr_IdNo))
			{

				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> True,
					"Message"		=> "MEMBER GET SUCCESSFULLY...",
					"Data"			=> ["lMembrIdNo" => $oGetMembr->lMembr_IdNo, "sMembrName" => $oGetMembr->sMembr_Name, "sMembrMobile" => $oGetMembr->sMembr_Mobile, "nMembrAge" => $oGetMembr->nMembr_Age, "lRelIdNo" => $oGetMembr->lRel_IdNo, "sAdrsLine" => $oGetMembr->sAdrs_Line, "sAdrsArea" => $oGetMembr->sAdrs_Area, "sCityName" => $oGetMembr->sCity_Name, "sStateName"	=> $oGetMembr->sState_Name, "sPinCod" => $oGetMembr->sPin_Cod]
				);
			}
			else
			{
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> False,
					"Message"		=> "WE COULD NOT FOUND ANY MEMBER..."
				);
			}
		}
		catch (\Exception $e)
    	{
    		$aRes = array(
				"ResponseCode"	=> 400,
			);
    	}
    	return json_encode($aRes, JSON_PRETTY_PRINT);
	}

	public function MembrList(Request $request)
	{
		try
		{
			$oGetMembr = $this->Member->MembrList($request['lUserIdNo']);
			foreach($oGetMembr As $aRes) 
			{
				$aRecSet[] = array(
					"lMembrIdNo"	=> $aRes->lMembr_IdNo,
					"sMembrName"	=> $aRes->sMembr_Name,
					"sMembrMobile"	=> $aRes->sMembr_Mobile,
					"nMembrAge"		=> $aRes->nMembr_Age,
					"sRelName"		=> array_search($aRes->lRel_IdNo, config('constant.MEMBER')),
					"sMembrAdrs"	=> $aRes->sAdrs_Line.", ".$aRes->sAdrs_Area.", ".$aRes->sCity_Name.", ".$aRes->sState_Name." ".$aRes->sPin_Cod,
				);
			}
			if(isset($aRecSet))
			{
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> True,
					"Message"		=> "MEMBER LISTED SUCCESSFULLY...",
					"Data"			=> $aRecSet
				);
			}
			else
			{
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> False,
					"Message"		=> "WE COULD NOT FOUND ANY MEMBER..."
				);
			}
		}
		catch (\Exception $e)
    	{
    		$aRes = array(
				"ResponseCode"	=> 400,
			);
    	}
    	return json_encode($aRes, JSON_PRETTY_PRINT);
	}
}
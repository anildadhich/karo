<?php

namespace App\Http\Controllers\ApiController;

use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use App\Model\Page;
use DB;

class PageController extends Controller
{
	public function __construct() 
	{
		$this->Page 	= new Page;
		 // parent::__construct();
		header("Content-Type: application/json");
		$valid_passwords = array ("karo" => "026866326a9d1d2b23226e4e5317569f");
		$valid_users = array_keys($valid_passwords);

		$user = request()->server('PHP_AUTH_USER');
		$pass = request()->server('PHP_AUTH_PW');

		$validated = (in_array($user, $valid_users)) && ($pass == $valid_passwords[$user]);

		if (!$validated) {
		  header('WWW-Authenticate: Basic realm="My Realm"');
		  header('HTTP/1.0 401 Unauthorized');
		  $re = array(
		  	"status" 	=> false,
		  	"message"	=> "You're not authorized to access."
		  );
		  echo json_encode($re, JSON_PRETTY_PRINT);
		  die;
		}
	}

	public function PageDtl()
	{
		try 
		{
			$lPageIdNo 	= 1;
			$oGetPage 	= $this->Page->PageDtl($lPageIdNo);
			$aRes = array(
				"ResponseCode"	=> 200,
				"Status"		=> True,
				"Message"		=> "DETAIL GET SUCCESSFULLY...",
				"Data"			=> nl2br(html_entity_decode($oGetPage->sPage_Dtl, ENT_COMPAT, 'UTF-8'))
			);
		} 
		catch (\Exception $e)
    	{
    		$aRes = array(
				"ResponseCode"	=> 400,
			);
    	}
		return json_encode($aRes, JSON_PRETTY_PRINT);
	}
}
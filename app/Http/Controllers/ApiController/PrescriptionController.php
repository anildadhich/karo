<?php

namespace App\Http\Controllers\ApiController;

use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use App\Model\Prescription;
use DB;

class PrescriptionController extends Controller
{
	public function __construct() 
	{
		$this->Prescription 	= new Prescription;
		 // parent::__construct();
		header("Content-Type: application/json");
		$valid_passwords = array ("karo" => "026866326a9d1d2b23226e4e5317569f");
		$valid_users = array_keys($valid_passwords);

		$user = request()->server('PHP_AUTH_USER');
		$pass = request()->server('PHP_AUTH_PW');

		$validated = (in_array($user, $valid_users)) && ($pass == $valid_passwords[$user]);

		if (!$validated) {
		  header('WWW-Authenticate: Basic realm="My Realm"');
		  header('HTTP/1.0 401 Unauthorized');
		  $re = array(
		  	"status" 	=> false,
		  	"message"	=> "You're not authorized to access."
		  );
		  echo json_encode($re, JSON_PRETTY_PRINT);
		  die;
		}
	}

	public function SaveCntrl(Request $request)
	{
		try
		{
			$validator = Validator::make($request->all(), [
	            'lUserIdNo' 	=> 'required',
		        'sPtntName' 	=> 'required',
		        'sDrName' 		=> 'required',
		        'sPrscrptFile' 	=> 'required',
	        ]);

			if($validator->fails())
	    	{
	    		$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> False,
					"Message"		=> "REQUIRED FIELD MISSING...",
				);
	    	}
	    	else
	    	{
	            \DB::beginTransaction();
	            	$sPrscrptFile   = $request->file('sPrscrptFile');
			        $sStorePath     = 'public/prescription_doc/';
			        $sPicName       = time().'.'.$sPrscrptFile->getClientOriginalExtension();
			        $sPrscrptFile->move($sStorePath,$sPicName);

					$aHdArr 		= $this->HdArr($request, $sPicName);
					$this->Prescription->InsrtRecrd($aHdArr);

					$aRes = array(
						"ResponseCode"	=> 200,
						"Status"		=> True,
						"Message"		=> "PRESCRIPTION UPLOADED SUCCESSFULLY...",
					);       	

	            \DB::commit();
			}
		}
		catch (\Exception $e)
    	{
    		\DB::rollback();
    		$aRes = array(
				"ResponseCode"	=> 400,
			);
    	}
		return json_encode($aRes, JSON_PRETTY_PRINT);
	}

	public function HdArr($request, $sPicName)
    {
        $aComnHdr = array(
            "sPs_No"        => rand(11111,99999),
            "lUser_IdNo"    => $request['lUserIdNo'],
            "sPtnt_Name"    => $request['sPtntName'],
            "sDr_Name"      => $request['sDrName'],
            "sPrscrpt_File" => $sPicName,
        );       
        return $aComnHdr; 
    } 

	public function PresLst(Request $request)
	{
		try
		{
			if(empty($request['lUserIdNo']))
			{
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> False,
					"Message"		=> "REQUIRED FIELD MISSING...",
				);
			}
			else
			{
				$oGetPrp = $this->Prescription->PresLst($request['lUserIdNo']);
				foreach($oGetPrp as $aRec) 
				{
					$aRecord[] = ["sPsNo" => $aRec->sPs_No, "sPtntName"  => $aRec->sPtnt_Name, "sDrName" => $aRec->sDr_Name, "sPrscrptFile" => config('constant.PUBLIC_URL').'/prescription_doc/'.$aRec->sPrscrpt_File, "sCrtDtTm" => date('D, F d, Y h:i A', strtotime($aRec->sCrt_DtTm))];
				}
	            if(isset($aRecord))
	            {
	            	$aRes = array(
						"ResponseCode"	=> 200,
						"Status"		=> True,
						"Message"		=> "PRESCRIPTION LISTED SUCCESSFULLY...",
						"Data"			=> $aRecord
					);       	
	            }
	            else
	            {
	            	$aRes = array(
						"ResponseCode"	=> 200,
						"Status"		=> False,
						"Message"		=> "NO ANY PRESCRIPTION FOUND...",
					);       		
	            }
			}
		}
		catch (\Exception $e)
    	{
    		$aRes = array(
				"ResponseCode"	=> 400,
			);
    	}
		return json_encode($aRes, JSON_PRETTY_PRINT);
	}
}
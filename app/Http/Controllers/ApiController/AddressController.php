<?php

namespace App\Http\Controllers\ApiController;

use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use App\Model\Address;
use DB;

class AddressController extends Controller
{
	public function __construct() 
	{
		$this->Address 	= new Address;
		 // parent::__construct();
		header("Content-Type: application/json");
		$valid_passwords = array ("karo" => "026866326a9d1d2b23226e4e5317569f");
		$valid_users = array_keys($valid_passwords);

		$user = request()->server('PHP_AUTH_USER');
		$pass = request()->server('PHP_AUTH_PW');

		$validated = (in_array($user, $valid_users)) && ($pass == $valid_passwords[$user]);

		if (!$validated) {
		  header('WWW-Authenticate: Basic realm="My Realm"');
		  header('HTTP/1.0 401 Unauthorized');
		  $re = array(
		  	"status" 	=> false,
		  	"message"	=> "You're not authorized to access."
		  );
		  echo json_encode($re, JSON_PRETTY_PRINT);
		  die;
		}
	}

	public function SaveAdrs(Request $request)
	{
		try
		{
			$yAdrsStatus = $this->Address->SaveAdrs($request);
			if($yAdrsStatus)
			{
				$aRes = array(
					"Status"	=> True,
					"Message"	=> "ADDRESS SAVED SUCCESSFULLY...",
				);
			}
			else
			{
				$aRes = array(
					"Status"	=> False,
					"Message"	=> "WE HAVE SOME TECHNICIAL ISSUES..."
				);
			}
		}
		catch (\Exception $e)
    	{
    		$aRes = array(
				"ResponseCode"	=> 400,
			);
    	}
		return json_encode($aRes, JSON_PRETTY_PRINT);
	}

	public function DelAdrs(Request $request)
	{
		try
		{
			$lAdrsIdNo 	= $request['lAdrsIdNo'];
			$yDelStatus = $this->Address->DelAdrs($lAdrsIdNo);
			if($yDelStatus)
			{
				$aRes = array(
					"Status"	=> True,
					"Message"	=> "ADDRESS DELETED SUCCESSFULLY...",
				);
			}
			else
			{
				$aRes = array(
					"Status"	=> False,
					"Message"	=> "WE HAVE SOME TECHNICIAL ISSUES..."
				);
			}
		}
		catch (\Exception $e)
    	{
    		$aRes = array(
				"ResponseCode"	=> 400,
			);
    	}
		return json_encode($aRes, JSON_PRETTY_PRINT);
	}
}
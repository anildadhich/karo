<?php

namespace App\Http\Controllers\ApiController;

use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use App\Model\User;
use DB;

class UserController extends Controller
{
	public function __construct() 
	{
		$this->User 	= new User;
		 // parent::__construct();
		header("Content-Type: application/json");
		$valid_passwords = array ("karo" => "026866326a9d1d2b23226e4e5317569f");
		$valid_users = array_keys($valid_passwords);

		$user = request()->server('PHP_AUTH_USER');
		$pass = request()->server('PHP_AUTH_PW');

		$validated = (in_array($user, $valid_users)) && ($pass == $valid_passwords[$user]);

		if (!$validated) {
		  header('WWW-Authenticate: Basic realm="My Realm"');
		  header('HTTP/1.0 401 Unauthorized');
		  $re = array(
		  	"status" 	=> false,
		  	"message"	=> "You're not authorized to access."
		  );
		  echo json_encode($re, JSON_PRETTY_PRINT);
		  die;
		}
	}

	public function UserLogin(Request $request)
	{
		try
		{
			$oGetUser	= $this->User->ChkUser($request);
			if(isset($oGetUser) && !empty($oGetUser->lUser_IdNo))
			{
				$aTknArr 	= $this->DvcArr($request['sDvicTokn']);
				$nRow		= $this->User->UpDtRecrd($aTknArr, $oGetUser->lUser_IdNo);
				$nLoginOTP	= rand(100001,999999);
				Controller::SendSms($request['sUserMobile'], $nLoginOTP);
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> True,
					"Message"		=> "LOGIN SUCCESSFULLY...",
					"Data"			=> ['lUserIdNo'	=> $oGetUser->lUser_IdNo, "nLoginOTP" => $nLoginOTP]
				);
			}
			else
			{
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> False,
					"Message"		=> "WE COULD NOT FIND ACCOUNT WITH THAT INFO..."
				);
			}
		}
		catch (\Exception $e)
    	{
    		$aRes = array(
				"ResponseCode"	=> 400,
			);
    	}

		return json_encode($aRes, JSON_PRETTY_PRINT);
	}

	public function UserLogout(Request $request)
	{
		try
		{
			$lUserIdNo 	= $request['lUserIdNo'];
			$aTknArr 	= $this->DvcArr($request['sDvicTokn']);
			$nRow		= $this->User->UpDtRecrd($aTknArr, $lUserIdNo);
			if($nRow == 1)
			{
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> True,
					"Message"		=> "LOGOUT SUCCESSFULLY...",
				);
			}
			else
			{
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> False,
					"Message"		=> "DEVICE TOKEN DID NOT UPDATED, TRY AGAIN..."
				);		
			}
		}
		catch (\Exception $e)
    	{
    		$aRes = array(
				"ResponseCode"	=> 400,
				"Message"		=> $e->getMessage()
			);
    	}
		return json_encode($aRes, JSON_PRETTY_PRINT);
	}

	public function DvcArr($sDvicTokn)
	{
		$aComnArr = array(
			"sDvic_Tokn" => !empty($sDvicTokn) ? $sDvicTokn : NULL,
		);
		return $aComnArr;
	}

	public function VrfyRefrl(Request $request)
	{
		try
		{
			$yRefStatus  = $this->User->IsRefExst($request['sRefCode'], $lUserIdNo);
			if($yRefStatus)
			{
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> True,
					"Message"		=> "WE FIND REFREL CODE...",
					"Data"			=> $lUserIdNo
				);
			}
			else
			{
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> False,
					"Message"		=> "WE COULD NOT FIND REFREL CODE..."
				);
			}
		}
		catch (\Exception $e)
    	{
    		$aRes = array(
				"ResponseCode"	=> 400,
			);
    	}
    	return json_encode($aRes, JSON_PRETTY_PRINT);
	}

	public function SaveCntrl(Request $request)
	{
		try
		{
			$validator = Validator::make($request->all(), [
	            'sUserName' 	=> 'required',
		        'sUserEmail' 	=> 'required',
		        'sUserMobile' 	=> 'required',
		        'sUserDob' 		=> 'required',
		        'sAdrsLine'		=> 'required',
				'sAdrsArea'		=> 'required',
				'sStateName'	=> 'required',
				'sCityName'		=> 'required',
				'sPinCod'		=> 'required',
	        ]);

			if($validator->fails())
	    	{
	    		$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> False,
					"Message"		=> "REQUIRED FIELD MISSING...",
				);
	    	}
	    	else
	    	{
	    		$lUserIdNo = isset($request['lUserIdNo']) ? $request['lUserIdNo'] : 0; 
	    		$yEmailStatus = $this->User->IsEmailExst($request['sUserEmail'], $lUserIdNo);
	    		if($yEmailStatus)
                {
                	$aRes = array(
						"ResponseCode"	=> 200,
						"Status"		=> False,
						"Message"		=> "EMAIL LINKED WITH ANOTHER ACCOUNT...",
					);
                }
                else
                {
                	$yMobileStatus = $this->User->IsMobileExst($request['sUserMobile'], $lUserIdNo);
                	if($yMobileStatus)
                	{
                		$aRes = array(
							"ResponseCode"	=> 200,
							"Status"		=> False,
							"Message"		=> "MOBILE LINKED WITH ANOTHER ACCOUNT...",
						);
                	}
                	else
                	{
	                	\DB::beginTransaction();
	                		$aHdArr  = $this->HdArr($request);
	                		if($lUserIdNo == 0)
	                		{
	                			$this->InsArr($aHdArr, $request['nUserGndr']);
		                    	$ySaveStatus = $this->User->InsrtRecrd($aHdArr, $lUserIdNo);
		                    	if($ySaveStatus && !empty($request['lRefByIdNo']))
		                    	{
		                    		$aRefArr  = $this->RefArr($request['lRefByIdNo'], $lUserIdNo);
		                    		$this->User->InsrtRefrl($aRefArr);
		                    	}
	                    		$nLoginOTP  = rand(111111,999999);
                                Controller::SendSms($request['sUserMobile'], $nLoginOTP);
		                        $aRes = array(
									"ResponseCode"	=> 200,
									"Status"		=> True,
									"Message"		=> "PROFILE CREATED SUCCESSFULLY...",
									"Data"			=> ['lUserIdNo'  => $lUserIdNo, "nLoginOTP" => $nLoginOTP]
								);
	                		}
	                		else
	                		{
	                			$this->User->UpDtRecrd($aHdArr, $lUserIdNo);	
                				$aProfileData	= $this->User->Profile($lUserIdNo);
	                        	$aRes = array(
									"ResponseCode"	=> 200,
									"Status"		=> True,
									"Message"		=> "PROFILE UPDATED SUCCESSFULLY...",
									"Data"			=> $aProfileData
								);
	                		}
		                \DB::commit();
		            }
                }
	    	}
		}
		catch (\Exception $e)
    	{
    		\DB::rollback();
    		$aRes = array(
				"ResponseCode"	=> 400,
			);
    	}
		return json_encode($aRes, JSON_PRETTY_PRINT);
	}

	public function HdArr($request)
    {
        $aComnHdr = array(
            "sUser_Name"    => $request['sUserName'],
            "sUser_Email"   => $request['sUserEmail'],
            "sUser_Mobile"  => $request['sUserMobile'],
            "sUser_Dob"     => $request['sUserDob'],
            "sAdrs_Line"    => $request['sAdrsLine'],
			"sAdrs_Area"	=> $request['sAdrsArea'],
			"sState_Name"	=> $request['sStateName'],
			"sCity_Name"	=> $request['sCityName'],
			"sPin_Cod"		=> $request['sPinCod'],
        );       
        return $aComnHdr; 
    }

    public function InsArr(&$aHdArr, $nUserGndr)
    {
        $aHdArr['nUser_Gndr']       = $nUserGndr;
        $aHdArr['sRef_Code']   		= $this->GnrtRefCode();
        $aHdArr['sBlk_UnBlk']       = config('constant.STATUS.UNBLOCK');
        $aHdArr['sDel_Status']      = config('constant.DEL_STATUS.NON_DELETED');
    }

    public function RefArr($lRefByIdNo, $lRefToIdNo)
    {
    	$aRefArr = array(
    		"lRef_By_IdNo"	=> $lRefByIdNo,
    		"lRef_To_IdNo"	=> $lRefToIdNo,
    		"sRef_Amo"		=> '100',
    	);
    	return $aRefArr;
    }

    public function GnrtRefCode() 
    { 
        $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
        return substr(str_shuffle($str_result),  
                           0, 6); 
    }

	public function UpDtPic(Request $request)
	{
		try
		{
			$lUserIdNo 	= $request['lUserIdNo'];
			$sUserPic 	= $request->file('sUserPic');
			if(!isset($sUserPic) && empty($sUserPic))
			{
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> False,
					"Message"		=> "REQUIRED FIELD MISSING...",
				);
			}
			else
			{
				$oGetUser = $this->User->GetUser($lUserIdNo);
				if(isset($oGetUser) && !empty($oGetUser->sUser_Pic))
				{
					if(file_exists('public/profile_pic/'.$oGetUser->sUser_Pic)) 
					{
                    	@unlink('public/profile_pic/'.$oGetUser->sUser_Pic);
                	}
				}

				$sStorePath 	= 'public/profile_pic/';
	            $sPicName 		= time().'.'.$sUserPic->getClientOriginalExtension();
	            $sUserPic->move($sStorePath,$sPicName);
	            \DB::beginTransaction();
	            	$nRow = $this->User->UpDtPic($sPicName, $lUserIdNo);
	            \DB::commit();
	            if($nRow > 0)
	            {
	            	$oGetUser	= $this->User->UserPic($request['lUserIdNo']);
				    $sUserPic = empty($oGetUser->sUser_Pic) ? 'default.png' : $oGetUser->sUser_Pic;
	            	$aRes = array(
						"ResponseCode"	=> 200,
						"Status"		=> True,
						"Message"		=> "PROFILE IMAGE UPDATED SUCCESSFULLY...",
						"Data"			=> config('constant.PUBLIC_URL').'/profile_pic/'.$sUserPic
					);       	
	            }
	            else
	            {
	            	$aRes = array(
						"ResponseCode"	=> 200,
						"Status"		=> False,
						"Message"		=> "NO ANY CHANGE FOUND...",
					);       		
	            }
			}
		}
		catch (\Exception $e)
    	{
    		\DB::rollback();
    		$aRes = array(
				"ResponseCode"	=> 400,
			);
    	}
		return json_encode($aRes, JSON_PRETTY_PRINT);
	}

	public function UserProfile(Request $request)
	{
		try
		{
			$lUserIdNo  	= $request['lUserIdNo'];
			$aProfileData	= $this->User->Profile($lUserIdNo);
			if(empty($aProfileData))
			{
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> False,
					"Message"		=> "WE COUND NOT FOUND ANY PROFILE..."
				);
			}
			else
			{
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> True,
					"Message"		=> "PROFILE GET SUCCESSFULLY...",
					"Data"			=> $aProfileData
				);
			}
		}
		catch (\Exception $e)
    	{
    		$aRes = array(
				"ResponseCode"	=> 400,
			);
    	}
		return json_encode($aRes, JSON_PRETTY_PRINT);
	}

	public function UserWalt(Request $request)
	{
		$lUserIdNo = $request['lUserIdNo'];
		try
		{
			$oGetWalt = $this->User->GetWalt($lUserIdNo);
			$aRes = array(
				"ResponseCode"	=> 200,
				"Status"		=> True,
				"Message"		=> 'WALLET AMOUNT GOT SUCCESSFULLY...',
				"Data"			=> isset($oGetWalt) ? $oGetWalt[0]->TtlWallet-$oGetWalt[0]->TtlRedm : 0,
			);
		}
		catch (\Exception $e)
    	{
    		$aRes = array(
				"ResponseCode"	=> 400,
			);
    	}
		return json_encode($aRes, JSON_PRETTY_PRINT);
	}

	public function RsndOtp(Request $request)
	{
		try
		{
			$oGetUser	= $this->User->ChkUser($request);
			if(isset($oGetUser) && !empty($oGetUser->lUser_IdNo))
			{
				$nMobileOTP	= rand(111111,999999);
				Controller::SendSms($request['sUserMobile'], $nMobileOTP);
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> True,
					"Message"		=> "OTP SEND SUCCESSFULLY...",
					"Data"			=> $nMobileOTP
				);
			}
			else
			{
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> False,
					"Message"		=> "WE COULD NOT FOUNT ANY ACCOUNT WITH THAT INFO..."
				);		
			}
		}
		catch (\Exception $e)
    	{
    		$aRes = array(
				"ResponseCode"	=> 400,
				"Message"		=> $e->getMessage()
			);
    	}
		return json_encode($aRes, JSON_PRETTY_PRINT);
	}
}
<?php

namespace App\Http\Controllers\ApiController;

use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use App\Model\Category;
use App\Model\Test;
use App\Model\Parameter;
use DB;

class CategoryController extends Controller
{
	public function __construct() 
	{
		$this->Category 	= new Category;
		$this->Parameter 	= new Parameter;
		$this->Test = new Test;

		header("Content-Type: application/json");
		$valid_passwords = array ("karo" => "026866326a9d1d2b23226e4e5317569f");
		$valid_users = array_keys($valid_passwords);

		$user = request()->server('PHP_AUTH_USER');
		$pass = request()->server('PHP_AUTH_PW');

		$validated = (in_array($user, $valid_users)) && ($pass == $valid_passwords[$user]);

		if (!$validated) {
		  header('WWW-Authenticate: Basic realm="My Realm"');
		  header('HTTP/1.0 401 Unauthorized');
		  $re = array(
		  	"status" 	=> false,
		  	"message"	=> "You're not authorized to access."
		  );
		  echo json_encode($re, JSON_PRETTY_PRINT);
		  die;
		}
	}

	public function CatgList()
	{
		try
		{
			$oGetCatg	= $this->Category->CatgList(config('constant.STATUS.UNBLOCK'));
			foreach($oGetCatg as $aRec)
			{

				$aRecords[] = ["lCatgIdNo" => $aRec->lCatg_IdNo, "sCatgName" => $aRec->sCatg_Name, "sIcnName" => config('constant.PUBLIC_URL').'/category_icon/'.$aRec->sIcn_Name];
			}

			if(isset($aRecords))
			{
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> True,
					"Message"		=> "CATEGORY LISTED SUCCESSFULLY...",
					"Data"			=> $aRecords
				);
			}
			else
			{
				$aRes = array(
					"ResponseCode"	=> 200,
					"Status"		=> False,
					"Message"		=> "WE COULD NOT FOND ANY CATEGORY..."
				);	
			}
		}
		catch (\Exception $e)
    	{
    		$aRes = array(
				"ResponseCode"	=> 400,
			);
    	}
		return json_encode($aRes, JSON_PRETTY_PRINT);
	}

	public function TstList(Request $request)
	{
		$lCatgIdNo = $request['lCatgIdNo'];
		if(!isset($lCatgIdNo) && empty($lCatgIdNo))
		{
			$aRes = array(
				"ResponseCode"	=> 200,
				"Status"		=> False,
				"Message"		=> "UNAUTHORIZED ACCESS..."
			);	
		}
		else
		{
			try
			{
				$oGetCatg = $this->Category->CatgDtl($lCatgIdNo);
				$sTstIds  = explode(",", $oGetCatg->sTst_Ids);
				$aRecords = ["sCatgName" => $oGetCatg->sCatg_Name];
				foreach($sTstIds as $lTstIdNo)
				{
				    $aTstPrmt = NULL;
				    $oGetTst    = $this->Test->TstDtl($lTstIdNo);
				    $oTstPrmtr 	= $this->Parameter->PrmtrLst($lTstIdNo);
				    foreach($oTstPrmtr as $aRecPrmtr)
					{
						$aTstPrmt[] = ["sPrmtrName" => $aRecPrmtr->sPrmtr_Name, "sPrmtrDtl" => $aRecPrmtr->sPrmtr_Dtl];
					}
					$nSaveUpTo  = round((($oGetTst->sActul_Amo-$oGetTst->sSale_Amo)/$oGetTst->sActul_Amo)*100);
					$aRecords['TstList'][] = ["lTstIdNo" => $oGetTst->lTst_IdNo, "sTstName" => $oGetTst->sTst_Name, "nTstGndr" => array_search($oGetTst->nTst_Gndr, config('constant.GENDER')), "sAgeRtio" => $oGetTst->nAge_Frm."-".$oGetTst->nAge_To." Yr", "sActulAmo" => $oGetTst->sActul_Amo, "sSaleAmo" => $oGetTst->sSale_Amo, "sTstDtl" => nl2br($oGetTst->sTst_Dtl), "nSaveUpTo" => $nSaveUpTo, "sTstType" => $oGetTst->sTst_Type, "sTstInfo" => $oGetTst->sTst_Info, "sRptDelv" => $oGetTst->sRpt_Delv, 'nTtlPrmtr' => count($oTstPrmtr), "aTstPrmt" => $aTstPrmt];
					
				}

				if(isset($aRecords))
				{
					$aRes = array(
						"ResponseCode"	=> 200,
						"Status"		=> True,
						"Message"		=> "CATEGORY TEST LISTED SUCCESSFULLY...",
						"Data"			=> $aRecords
					);
				}
				else
				{
					$aRes = array(
						"ResponseCode"	=> 200,
						"Status"		=> False,
						"Message"		=> "WE COULD NOT FOND ANY TEST..."
					);	
				}
			}
			catch (\Exception $e)
	    	{
	    		$aRes = array(
					"ResponseCode"	=> 400,
					"Message"		=> $e->getMessage()
				);
	    	}
	    }
		return json_encode($aRes, JSON_PRETTY_PRINT);
	}
}
<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Artisan;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public function SendSms($sMblNum, $sMsg)
    {
        $USER_NAME      = "20200777";
        $PASSWORD       = "99GPWSS9";
        $SENDER_ID      = "TRANLP";
        $PRIORITY       = 1;
        $MOBILE_NO      = $sMblNum;
        $MESSAGE        = $sMsg;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://164.52.195.161/API/SendMsg.aspx?",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "uname=$USER_NAME&pass=$PASSWORD&send=$SENDER_ID&dest=$MOBILE_NO&msg=$MESSAGE&priority=$PRIORITY",
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
    }

    public function SendNotification($sToken, $sTitle, $sBody, $sServerKey = '')
    {
        if(empty($sServerKey))
        {
            $sServerKey = 'AAAAks8z1dE:APA91bGd7ILdZ2NMMblmhrOTkNVOjtt0rFYoVZpHgIr1OYdD3HYPV3fv8yUVjQSvPoa-b4mMuq-Wnoov6NQF86wy5z2qINmw4FSr5H2mIXa0S0dFODEB_6_3diP1wJ4SJRt_SQ_vVbTn';
        }
        $url = 'https://fcm.googleapis.com/fcm/send'; 
        $headers = array
        (
            'Authorization: key='.$sServerKey, 
             'Content-Type: application/json',
        );  

        $data = array(
            "to" => $sToken,
            "notification" => 
                    array(
                        "title" => $sTitle,
                        "body"  => $sBody,
                        "sound" => 'default',
                        //"icon" => "icon.png", 
                        //"click_action" => "http://shareurcodes.com"
                    )
            );                                                                                                                              
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
}

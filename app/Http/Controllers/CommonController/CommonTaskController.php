<?php

namespace App\Http\Controllers\CommonController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Administrator;
use DB;

class CommonTaskController extends Controller
{
    public function __construct()
    {
        //$this->middleware(Administrator::class);
    }

    public function DelRec($lRecIdNo, $sTblName, $sFldName)
    {
        $lRecIdNo   = base64_decode($lRecIdNo);
        $sTblName   = base64_decode($sTblName);
        $sFldName   = base64_decode($sFldName);

        $oGetRec    = DB::table($sTblName)->Where($sFldName,$lRecIdNo)->first();
        if(isset($oGetRec) && !empty($oGetRec->$sFldName))
        {
            $aValue = array(
                "nDel_Status"   => 409,
            );
            DB::table($sTblName)->Where($sFldName,$lRecIdNo)->update($aValue);
            return redirect()->back()->with('Success', 'Record deleted successfully...');
        }
        else
        {
            return redirect()->back()->with('Failed', 'Unauthorized access...');
        }
    }

    public function ChngStatus($lRecIdNo, $sTblName, $sFldName, $nStatus)
    {
        $lRecIdNo = base64_decode($lRecIdNo);
        $sTblName = base64_decode($sTblName);
        $sFldName = base64_decode($sFldName);
        $nStatus  = base64_decode($nStatus);

        $oGetRec  = DB::table($sTblName)->Where($sFldName,$lRecIdNo)->first();
        if(isset($oGetRec) && !empty($oGetRec->$sFldName))
        {
            $aValue = array(
                "nBlk_UnBlk"   => $nStatus,
            );
            DB::table($sTblName)->Where($sFldName,$lRecIdNo)->update($aValue);
            return redirect()->back()->with('Success', 'Status changed successfully...');
        }
        else
        {
            return redirect()->back()->with('Failed', 'Unauthorized access...');
        }
    }
}
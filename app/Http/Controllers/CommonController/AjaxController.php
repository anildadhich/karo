<?php

namespace App\Http\Controllers\CommonController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Parameter;
use DB;

class AjaxController extends Controller
{
    public function __construct()
    {
        $this->Parameter = new Parameter;
    }

    public function GetPrmtr($lTstIdNo)
    {
        $lTstIdNo    = base64_decode($lTstIdNo);
        $oGetPrmtr   = $this->Parameter->PrmtrLst($lTstIdNo);
        return $oGetPrmtr;
    }
}
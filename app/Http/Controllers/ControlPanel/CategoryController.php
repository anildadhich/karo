<?php

namespace App\Http\Controllers\ControlPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Administrator;
use App\Model\Category;
use App\Model\Test;

class CategoryController extends Controller
{
	public function __construct()
	{
		$this->Category = new Category;
		$this->Test 	= new Test;
		$this->middleware(Administrator::class);
	}

	public function FromCntrl($lRecIdNo = False)
	{
		$lRecIdNo 	= base64_decode($lRecIdNo);
		$oGetRec    = NULL;
		if(!empty($lRecIdNo))
		{
			$oGetRec = $this->Category->CatgDtl($lRecIdNo);
			if(!isset($oGetRec) && empty($oGetRec->lTst_IdNo))
			{
				return redirect('/admin_panel/category_list')->with('Failed', 'Unauthorized access...');	
			}
		}
		$oGetTst	= $this->Test->TstLst();
    	$sTitle	= "Manage Category";
    	$aData 	= compact('sTitle','oGetTst','oGetRec');
        return view('control_panel.category_module.FormView',$aData);
	}

	public function SaveCntrl(Request $request)
	{
		try
		{	
			$input			= $request['input'];
			$lCatgIdNo 		= base64_decode($input['lCatgIdNo']);
			$sIcnName		= $request->file('sIcnName');

			if(!empty($sIcnName))
			{
				$sStorePath 	= 'public/category_icon/';
				$sPicName 		= time().'.'.$sIcnName->getClientOriginalExtension();
				$sIcnName->move($sStorePath,$sPicName);
			}

			\DB::beginTransaction();
				$aHdArr	= $this->HdArr($request);
				if($lCatgIdNo == 0)
				{	
					$this->InsArr($aHdArr, $sPicName);
					$ySaveStatus 	= $this->Category->InsrtRecrd($aHdArr);
				}
				else
				{
					if(!empty($sIcnName))
					{
						$oGetPic	= $this->Category->CatgDtl($lCatgIdNo);
						if(isset($oGetPic) && !empty($oGetPic->sIcn_Name))
						{
							if(file_exists('public/category_icon/'.$oGetPic->sIcn_Name)) 
							{
		                    	@unlink('public/category_icon/'.$oGetPic->sIcn_Name);
		                	}
						}
						$this->UpDtArr($aHdArr, $sPicName);
					}
					$ySaveStatus 	= $this->Category->UpDtRecrd($aHdArr, $lCatgIdNo);	
				}
			\DB::commit();
			if($ySaveStatus)
			{
				return redirect('admin_panel/category_list')->with('Success', "Record managed successfully...");
			}
		}
		catch(\Exception $e)
		{
			\DB::rollback();
			return redirect()->back()->with('Failed', 'Record did not created due to some technocial issue...');
		}
	}

	public function LstCntrl()
	{
		$oGetRec = $this->Category->CatgList();
		$sTitle	= "Manage Category List";
    	$aData 	= compact('sTitle','oGetRec');
        return view('control_panel.category_module.ListView',$aData);
	}

	public function HdArr($request)
    {
    	$input 		= $request['input'];
        $aComnArr   = array(
            "sCatg_Name"    => $input['sCatgName'],
            "sTst_Ids"      => implode(",",$request['sTstIds']),
        );
        return $aComnArr;
    }

    public function InsArr(&$aHdArr, $sIcnName)
    {
        $aHdArr['sIcn_Name']    = $sIcnName;
        $aHdArr['nBlk_UnBlk']   = config('constant.STATUS.UNBLOCK');
        $aHdArr['nDel_Status']  = config('constant.DEL_STATUS.NON_DELETED');
    }

    public function UpDtArr(&$aHdArr, $sIcnName)
    {
        $aHdArr['sIcn_Name']    = $sIcnName;
    }
}
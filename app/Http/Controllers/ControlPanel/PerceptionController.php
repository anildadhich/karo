<?php

namespace App\Http\Controllers\ControlPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Administrator;
use App\Model\Prescription;

class PerceptionController extends Controller
{
	public function __construct()
	{
		$this->Prescription = new Prescription;
		//$this->middleware(Administrator::class);
	}

	public function LstCntrl()
	{
		$oGetRec = $this->Prescription->PresAll();
		$sTitle	= "Manage Prescription List";
    	$aData 	= compact('sTitle','oGetRec');
        return view('control_panel.prescription_module.ListView',$aData);
	}
}
<?php

namespace App\Http\Controllers\ControlPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Administrator;
use App\Model\Package;
use App\Model\Test;

class PackageController extends Controller
{
	public function __construct()
	{
		$this->Package 	= new Package;
		$this->Test 	= new Test;
		$this->middleware(Administrator::class);
	}

	public function FromCntrl($lRecIdNo = False)
	{
	    $oGetTst = $this->Test->TstLst(config('constant.STATUS.UNBLOCK'));
	    if(count($oGetTst) <= 0)
		{
			return redirect('/admin_panel/test')->with('Failed', 'First add test...');	
		}
		$lRecIdNo 	= base64_decode($lRecIdNo);
		$oGetRec    = NULL;
		if(!empty($lRecIdNo))
		{
			$oGetRec = $this->Package->PckgDtl($lRecIdNo);
			if(!isset($oGetRec) && empty($oGetRec->lPckg_IdNo))
			{
				return redirect('/admin_panel/package_list')->with('Failed', 'Unauthorized access...');	
			}
		}
		$sTitle	= "Manage Package";
    	$aData 	= compact('sTitle','oGetTst','oGetRec');
        return view('control_panel.package_module.FormView',$aData);
	}

	public function SaveCntrl(Request $request)
	{
		try
		{	
			$input		= $request['input'];
			$lPckgIdNo 	= base64_decode($input['lPckgIdNo']);
			\DB::beginTransaction();
				$aHdArr	= $this->HdArr($request);
				if($lPckgIdNo == 0)
				{	
					$this->InsArr($aHdArr);
					$ySaveStatus 	= $this->Package->InsrtRecrd($aHdArr);
				}
				else
				{
					$ySaveStatus 	= $this->Package->UpDtRecrd($aHdArr, $lPckgIdNo);	
				}
			\DB::commit();
			if($ySaveStatus)
			{
				return redirect('admin_panel/package_list')->with('Success', "Record managed successfully...");
			}
		}
		catch(\Exception $e)
		{
			\DB::rollback();
			return redirect()->back()->with('Failed', 'Record did not created due to some technocial issue...');
		}
	}

	public function LstCntrl()
	{
		$oGetRec = $this->Package->PckgAll();
		$sTitle	= "Manage Package List";
    	$aData 	= compact('sTitle','oGetRec');
        return view('control_panel.package_module.ListView',$aData);
	}

	public function HdArr($request)
    {
    	$input 		= $request['input'];
        $aComnArr   = array(
            "sPckg_Name"    => $input['sPckgName'],
            "nPckg_Gndr"    => $input['nPckgGndr'],
            "nAge_Frm"      => $input['nAgeFrm'],
            "nAge_To"       => $input['nAgeTo'],
            "sActul_Amo"    => $input['sActulAmo'],
            "sSale_Amo"     => $input['sSaleAmo'],
            "sPckg_Dtl"     => $input['sPckgDtl'],
            "sTst_Ids"      => implode(",", $request['sTstIds']),
        );
        return $aComnArr;
    }

    public function InsArr(&$aHdArr)
    {
        $aHdArr['nBlk_UnBlk']   = config('constant.STATUS.UNBLOCK');
        $aHdArr['nDel_Status']  = config('constant.DEL_STATUS.NON_DELETED');
    }
}
<?php

namespace App\Http\Controllers\ControlPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Administrator;
use App\Model\User;

class UserController extends Controller
{
	public function __construct()
	{
		$this->User = new User;
		$this->middleware(Administrator::class);
	}

	public function LstCntrl()
	{
		$oGetRec = $this->User->UserLst();
		$sTitle	= "Manage User List";
    	$aData 	= compact('sTitle','oGetRec');
        return view('control_panel.user_module.ListView',$aData);
	}
}
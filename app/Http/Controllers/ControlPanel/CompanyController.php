<?php

namespace App\Http\Controllers\ControlPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Administrator;
use App\Model\Company;

class CompanyController extends Controller
{
	public function __construct()
	{
		$this->Company = new Company;
		$this->middleware(Administrator::class);
	}

	public function FromCntrl($lRecIdNo = False)
	{
		$oGetRec = $this->Company->CompRec();
    	$sTitle	= "Manage Discount";
    	$aData 	= compact('sTitle','oGetRec');
        return view('control_panel.company_module.FormDiscount',$aData);
	}

	public function SaveCntrl(Request $request)
	{
		try
		{	
			$input			= $request['input'];
			\DB::beginTransaction();
				$aHdArr	= $this->HdArr($input);
				$ySaveStatus 	= $this->Company->UpDtRecrd($aHdArr);	
			\DB::commit();
			if($ySaveStatus)
			{
				return redirect()->back()->with('Success', "Discount updated successfully...");
			}
			else
			{
				return redirect()->back()->with('Failed', "No update found...");	
			}
		}
		catch(\Exception $e)
		{
			\DB::rollback();
			return redirect()->back()->with('Failed', 'Record did not created due to some technocial issue...');
		}
	}

	public function HdArr($input)
    {
        $aComnArr   = array(
            "nPoplr_Catg_Off"   => $input['nPoplrCatgOff'],
            "nCatg_Off"    		=> $input['nCatgOff'],
        );
        return $aComnArr;
    }
}
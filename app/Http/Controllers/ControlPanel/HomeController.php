<?php

namespace App\Http\Controllers\ControlPanel;

use Illuminate\Http\Request;
use Validator;
use Razorpay\Api\Api;
use App\Http\Controllers\Controller;
use App\Model\Company;
use App\Model\OrdHd;
use App\Model\User;
use Artisan;

class HomeController extends Controller
{
	public function __construct()
	{
		$this->Company 	= new Company;
		$this->OrdHd 	= new OrdHd;
		$this->User 	= new User;
	}
	
	public function HomePage()
	{
		if(empty(session('LCOMP_IDNO')))
    	{
    		$sTitle = "Control Panel Login";
	    	$aData 	= compact('sTitle');
	        return view('control_panel.login',$aData);	
    	}
    	else
    	{
    		$oOnOrd 	= $this->OrdHd->CntOrd(config('constant.ORD_STATUS.ONGOING'));
    		$oCmpOrd 	= $this->OrdHd->CntOrd(config('constant.ORD_STATUS.COMPLETE'));
    		$oCncOrd 	= $this->OrdHd->CntOrd(config('constant.ORD_STATUS.CANCELLED'));
    		$oGetUser 	= $this->User->UserLst();
	    	$sTitle		= "Dashbard";
	    	$aData 	= compact('sTitle','oOnOrd','oCmpOrd','oCncOrd','oGetUser');
	        return view('control_panel.dashboard',$aData);
    	}
	}

	public function CompLogin(Request $request)
	{
		try
		{
			$input 		= $request['input'];
	        if (empty($input['sCompEmail']) || empty($input['sCompPass']))
	        {
	            $aRes = array(
				     "Status" 	=> False,
				     "Message"  => 'Required field missing...'
				);
	        }
	        else
	        {
				$yLgnStatus = $this->Company->CompLogin($input['sCompEmail'], $input['sCompPass'], $lCompIdNo);
				if($yLgnStatus)
				{
					session(['LCOMP_IDNO' => $lCompIdNo]);
					$aRes = array(
						"Status"	=> True,
						"Message"	=> "Login success, redirecting..."
					);		
				}
				else
				{
					$aRes = array(
						"Status"	=> False,
						"Message"	=> "we could not found any account with that info..."
					);
				}
			}
		}
		catch(\Exception $e)
		{
			$aRes = array(
				"Status"	=> False,
				"Message"	=> "Login failed due to technical issue, try again..."
			);
		}
		return json_encode($aRes, JSON_PRETTY_PRINT);
	}

	public function CompLogout()
    {
        session()->flush();
        return redirect('admin_panel');
    }
}
<?php

namespace App\Http\Controllers\ControlPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Administrator;
use App\Model\Test;
use App\Model\Parameter;

class TestController extends Controller
{
	public function __construct()
	{
		$this->Test 		= new Test;
		$this->Parameter 	= new Parameter;
		$this->middleware(Administrator::class);
	}

	public function FromCntrl($lRecIdNo = False)
	{
		$lRecIdNo 	= base64_decode($lRecIdNo);
		$oGetRec    = NULL;
		if(!empty($lRecIdNo))
		{
			$oGetRec = $this->Test->TstDtl($lRecIdNo);
			if(!isset($oGetRec) && empty($oGetRec->lTst_IdNo))
			{
				return redirect('/admin_panel/test/list')->with('Failed', 'Unauthorized access...');	
			}
		}
    	$sTitle	= "Manage Test";
    	$aData 	= compact('sTitle','oGetRec');
        return view('control_panel.test_module.FormView',$aData);
	}

	public function SaveCntrl(Request $request)
	{
		try
		{	
			$input			= $request['input'];
			$lTstIdNo 		= base64_decode($input['lTstIdNo']);
			$sTstPic		= $request->file('sTstPic');
			if(!empty($sTstPic))
			{
				$sStorePath 	= 'public/test_pic/';
				$sPicName 		= time().'.'.$sTstPic->getClientOriginalExtension();
				$sTstPic->move($sStorePath,$sPicName);
			}

	        \DB::beginTransaction();
				$aHdArr	= $this->HdArr($input);
				if($lTstIdNo == 0)
				{	
					$this->InsArr($aHdArr, $sPicName);
					$ySaveStatus 	= $this->Test->InsrtRecrd($aHdArr, $lTstIdNo);
				}
				else
				{
					if(!empty($sTstPic))
					{
						$oGetPic	= $this->Test->TstDtl($lTstIdNo);
						if(isset($oGetPic) && !empty($oGetPic->sTst_Pic))
						{
							if(file_exists('public/test_pic/'.$oGetPic->sTst_Pic)) 
							{
		                    	@unlink('public/test_pic/'.$oGetPic->sTst_Pic);
		                	}
						}
						$this->UpDtArr($aHdArr, $sPicName);
					}
					$this->Parameter->PrmtDel($lTstIdNo);
					$ySaveStatus 	= $this->Test->UpDtRecrd($aHdArr, $lTstIdNo);	
				}

				$i=1;
		        for($i==1; $i<=$input['toalrec']; $i++) 
		        { 

		            if(!empty($input['sPrmtrName_'.$i]) && !empty($input['sPrmtrDtl_'.$i]))
		            {
		                $aArrDt = array(
		                    'lTst_IdNo'     => $lTstIdNo,
		                    'sPrmtr_Name'   => $input['sPrmtrName_'.$i],
		                    'sPrmtr_Dtl'    => $input['sPrmtrDtl_'.$i],
		                );
		                $this->Parameter->InsrtRecrd($aArrDt);
		            }
		        }
			\DB::commit();
			if($ySaveStatus)
			{
				return redirect('admin_panel/test_list')->with('Success', "Record managed successfully...");
			}
		}
		catch(\Exception $e)
		{
			\DB::rollback();
			return redirect()->back()->with('Failed', 'Record did not created due to some technocial issue...');
		}
	}

	public function LstCntrl(Request $request)
	{
		$sTstName 	= $request['sTstName'];
		$oGetRec 	= $this->Test->TstAll($sTstName);
		$sTitle		= "Manage Test List";
    	$aData 		= compact('sTitle','oGetRec','request');
        return view('control_panel.test_module.ListView',$aData);
	}

	public function HdArr($input)
    {
        $aComnArr   = array(
            "sTst_Name"     => $input['sTstName'],
            "nTst_Gndr"     => $input['nTstGndr'],
            "nAge_Frm"      => $input['nAgeFrm'],
            "nAge_To"       => $input['nAgeTo'],
            "sActul_Amo"    => $input['sActulAmo'],
            "sSale_Amo"     => $input['sSaleAmo'],
            "sTst_Dtl"      => $input['sTstDtl'],
            "nTst_Catg"     => $input['nTstCatg'],
            "sTst_Type"     => $input['sTstType'],
            "sTst_Info"     => $input['sTstInfo'],
            "sRpt_Delv"     => $input['sRptDelv'],
        );
        return $aComnArr;
    }

    public function InsArr(&$aHdArr, $sPicName)
    {
    	$aHdArr['sTst_Pic']   	= $sPicName;
        $aHdArr['nBlk_UnBlk']   = config('constant.STATUS.UNBLOCK');
        $aHdArr['nDel_Status']  = config('constant.DEL_STATUS.NON_DELETED');
    }

    public function UpDtArr(&$aHdArr, $sPicName)
    {
    	$aHdArr['sTst_Pic']   	= $sPicName;
    }
}
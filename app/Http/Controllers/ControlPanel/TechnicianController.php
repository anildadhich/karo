<?php

namespace App\Http\Controllers\ControlPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Administrator;
use App\Model\Technician;

class TechnicianController extends Controller
{
	public function __construct()
	{
		$this->Technician = new Technician;
		$this->middleware(Administrator::class);
	}

	public function FromCntrl($lRecIdNo = False)
	{
		$lRecIdNo 	= base64_decode($lRecIdNo);
		$oGetRec    = NULL;
		if(!empty($lRecIdNo))
		{
			$oGetRec = $this->Technician->Profile($lRecIdNo);
			if(!isset($oGetRec) && empty($oGetRec->lTst_IdNo))
			{
				return redirect('/admin_panel/technician_list')->with('Failed', 'Unauthorized access...');	
			}
		}
    	$sTitle	= "Manage Technician";
    	$aData 	= compact('sTitle','oGetRec');
        return view('control_panel.technician_module.FormView',$aData);
	}

	public function SaveCntrl(Request $request)
	{
		$lTechIdNo 		= base64_decode($request['lTechIdNo']);
		$rules = [
	        'sTechName' 	=> 'required|min:3|max:30|regex:/^[\pL\s]+$/u',
            'nTechGndr' 	=> 'required',
            'sTechMobile' 	=> 'required|unique:tech_mst,sTech_Mobile,'.$lTechIdNo.',lTech_IdNo|digits:10',
            'sTechEmail' 		=> 'required|unique:tech_mst,sTech_Email,'.$lTechIdNo.',lTech_IdNo|max:50|regex:^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$^',
            'sTechDob'		=> 'required',
            'sAdrsLine'		=> 'required|min:6|max:150',
            'sAreaName'		=> 'required|min:3|max:30',
            'sCityName'		=> 'required|min:3|max:30',
            'sStateName'	=> 'required|min:3|max:30',
            'sPinCod'		=> 'required|digits:6',
	    ];

	    if($lTechIdNo == 0) 
	    {
	    	$rules['sTechPic']	= 'required|mimes:jpeg,jpg,png|max:2000';
	    	$rules['sProfFrnt']	= 'required|mimes:jpeg,jpg,png|max:2000';
            $rules['sProfBck']	= 'required|mimes:jpeg,jpg,png|max:2000';
	    }
	    else
	    {
	    	if(!empty($request['sTechPic'])) {
	    		$rules['sTechPic']	= 'mimes:jpeg,jpg,png|max:2000';
	    	}
	    	if(!empty($request['sTechPic'])) {
	    		$rules['sProfFrnt']	= 'mimes:jpeg,jpg,png|max:2000';
	    	}
	    	if(!empty($request['sTechPic'])) {
            	$rules['sProfBck']	= 'mimes:jpeg,jpg,png|max:2000';	
            }
	    }


	    $this->validate($request, $rules, config('constant.VLDT_MSG'));
		try
		{	
			$sTechPic		= $request->file('sTechPic');
			$sProfFrnt		= $request->file('sProfFrnt');
			$sProfBck		= $request->file('sProfBck');

			\DB::beginTransaction();
				$aHdArr	= $this->HdArr($request);
				if($lTechIdNo == 0)
				{	
					$this->InsArr($aHdArr);
					$lTechIdNo 	= $this->Technician->InsrtRecrd($aHdArr);
					if(!empty($lTechIdNo) && $lTechIdNo > 0)
					{
						$this->UpldPic($sTechPic, $lTechIdNo);
						$this->UpldFrnt($sProfFrnt, $lTechIdNo);
						$this->UpldBck($sProfBck, $lTechIdNo);
					}
				}
				else
				{
					if(!empty($sTechPic))
					{
						$this->UpldPic($sTechPic, $lTechIdNo);
					}
					if(!empty($sProfFrnt))
					{
						$this->UpldFrnt($sTechPic, $lTechIdNo);
					}
					if(!empty($sProfBck))
					{
						$this->UpldBck($sTechPic, $lTechIdNo);
					}
					$this->Technician->UpDtRecrd($aHdArr, $lTechIdNo);	
				}
			\DB::commit();
			return redirect('admin_panel/technician_list')->with('Success', "Record managed successfully...");
		}
		catch(\Exception $e)
		{
			\DB::rollback();
			return redirect()->back()->with('Failed', 'Record did not created due to some technocial issue...');
		}
	}

	public function LstCntrl()
	{
		$oGetRec = $this->Technician->TechList();
		$sTitle	= "Manage Technician List";
    	$aData 	= compact('sTitle','oGetRec');
        return view('control_panel.technician_module.ListView',$aData);
	}

	public function HdArr($request)
    {
        $aComnArr   = array(
            "sTech_Name"	=> $request['sTechName'],
            "sTech_Mobile"  => $request['sTechMobile'],
            "sTech_Email"   => $request['sTechEmail'],
            "nTech_Gndr"    => $request['nTechGndr'],
            "sTech_Dob"    	=> $request['sTechDob'],
            "sAdrs_Line"    => $request['sAdrsLine'],
            "sState_Name"   => $request['sStateName'],
            "sCity_Name"    => $request['sCityName'],
            "sArea_Name"    => $request['sAreaName'],
            "sPin_Cod"    	=> $request['sPinCod'],
        );
        return $aComnArr;
    }

    public function InsArr(&$aHdArr)
    {
        $aHdArr['nTech_Status']   	= config('constant.TECH_STATUS.ONLINE');
        $aHdArr['nBlk_UnBlk']   	= config('constant.STATUS.UNBLOCK');
        $aHdArr['nDel_Status']  	= config('constant.DEL_STATUS.NON_DELETED');
    }

    public function UpldPic($sPic, $lTechIdNo)
    {
    	$oGetPic		= $this->Technician->PicData($lTechIdNo);
    	if(!empty($oGetPic->sTech_Pic))
    	{
    		if(file_exists(config('constant.PUBLIC_URL').'/technician_pic/'.$oGetPic->sTech_Pic))
    		{
    			@unlink(config('constant.PUBLIC_URL').'/technician_pic/'.$oGetPic->sTech_Pic);
    		}
    	}
    	$sStorePath 	= 'public/technician_pic/';
		$sPicName 		= time().'.'.$sPic->getClientOriginalExtension();
		$sPic->move($sStorePath,$sPicName);
		$aValue = array(
			"sTech_Pic"	=> $sPicName,
		);
		$this->Technician->UpDtRecrd($aValue, $lTechIdNo);
    }

    public function UpldFrnt($sPic, $lTechIdNo)
    {
    	$oGetPic		= $this->Technician->PicData($lTechIdNo);
    	if(!empty($oGetPic->sProf_Frnt))
    	{
    		if(file_exists(config('constant.PUBLIC_URL').'/technician_proof/'.$oGetPic->sProf_Frnt))
    		{
    			@unlink(config('constant.PUBLIC_URL').'/technician_proof/'.$oGetPic->sProf_Frnt);
    		}
    	}
    	$sStorePath 	= 'public/technician_proof/';
		$sPicName 		= 'Front_'.time().'.'.$sPic->getClientOriginalExtension();
		$sPic->move($sStorePath,$sPicName);
		$aValue = array(
			"sProf_Frnt"	=> $sPicName,
		);
		$this->Technician->UpDtRecrd($aValue, $lTechIdNo);
    }

    public function UpldBck($sPic, $lTechIdNo)
    {
    	$oGetPic		= $this->Technician->PicData($lTechIdNo);
    	if(!empty($oGetPic->sProf_Bck))
    	{
    		if(file_exists(config('constant.PUBLIC_URL').'/technician_proof/'.$oGetPic->sProf_Bck))
    		{
    			@unlink(config('constant.PUBLIC_URL').'/technician_proof/'.$oGetPic->sProf_Bck);
    		}
    	}
    	$sStorePath 	= 'public/technician_proof/';
		$sPicName 		= 'Back_'.time().'.'.$sPic->getClientOriginalExtension();
		$sPic->move($sStorePath,$sPicName);
		$aValue = array(
			"sProf_Bck"	=> $sPicName,
		);
		$this->Technician->UpDtRecrd($aValue, $lTechIdNo);
    }

    public function UpDtArr(&$aHdArr, $sIcnName)
    {
        $aHdArr['sIcn_Name']    = $sIcnName;
    }
}
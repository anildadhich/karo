<?php

namespace App\Http\Controllers\ControlPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Administrator;
use Razorpay\Api\Api;
use App\Model\OrdHd;
use App\Model\OrdDt;
use App\Model\Test;
use App\Model\Parameter;
use App\Model\Technician;
use App\Model\TechOrder;
use App\Model\Report;
use DB;

class OrderController extends Controller
{
	public function __construct()
	{
		$this->OrdHd 		= new OrdHd;
		$this->OrdDt 		= new OrdDt;
		$this->Test 		= new Test;
		$this->Parameter 	= new Parameter;
		$this->Technician 	= new Technician;
		$this->TechOrder 	= new TechOrder;
		$this->Report 		= new Report;
		$this->middleware(Administrator::class);
	}

	public function PndgOrd()
	{
		$oGetRec 	= $this->OrdHd->OrdLst('', config('constant.ORD_STATUS.ONGOING'));
		$oGetTech 	= $this->Technician->ActvTech();
		$sTitle	= "Manage New Order List";
    	$aData 	= compact('sTitle','oGetRec','oGetTech');
        return view('control_panel.order_module.ListView',$aData);
	}

	public function CmpltOrd()
	{
		$oGetRec 	= $this->OrdHd->OrdLst('', config('constant.ORD_STATUS.COMPLETE'));
		$sTitle	= "Manage Completed Order List";
    	$aData 	= compact('sTitle','oGetRec');
        return view('control_panel.order_module.ListView1',$aData);
	}

	public function CnclCntrl()
	{
		$oGetRec 	= $this->OrdHd->OrdLst('', config('constant.ORD_STATUS.CANCELLED'));
		$sTitle	= "Manage Cancel Order List";
    	$aData 	= compact('sTitle','oGetRec');
        return view('control_panel.order_module.ListView2',$aData);
	}

	public function AlctOrd(Request $request)
	{
		$input 	= $request['input'];
		$aHdArr = $this->OrdArr($input);
		$nRow	= $this->TechOrder->InsrtRecrd($aHdArr);
		$oGetTech 	= $this->Technician->Profile($input['lTechIdNo']);
		$oGetOrd	= $this->OrdHd->OrdDtl(base64_decode($input['lOrdHdIdNo']));
		$sServerKey = 'AAAAwz7pLmk:APA91bHediOy53EbH3DTFmclD0kT_mWHfWr1AMLY2I5RiMeCmKpT9bwqPDWm11J8GylMoMDhF4Xl8M13xrYH2U-GPsZC68bi3Nk_WQxkg0CKCTAYW7qWPv6AToGcBi3AmDOECJVas1zT';
		$sTitle = 'New Order';
		$sBody	= 'You have an new order ('.$oGetOrd->sOrd_No.') for collect sample';
		Controller::SendNotification($oGetTech->sDvic_Tokn, $sTitle, $sBody, $sServerKey);
		return redirect('admin_panel/pending_order')->with('Success', 'Order allocated successfully...');
		
	}

	public function OrdArr($input)
	{
		$aAlctArr = array(
			"lOrd_Hd_IdNo"	=> base64_decode($input['lOrdHdIdNo']),
			"lTech_IdNo"	=> $input['lTechIdNo'],
			"sCmnt_Dtl"		=> $input['sCmntDtl'],
			"nCurr_Status"	=> config('constant.TECH_ORD_STATUS.PENDING'),
		);
		return $aAlctArr;
	}

	public function LstCntrlDtls($id)
	{
		$oGetDtlRec = $this->OrdDt->OrdDtLst($id);
		$oGetRec = $this->OrdHd->OrdDtl($id);
		$sTitle	= "Detail Order List";
		log::info($oGetDtlRec);
		log::info($oGetRec);
    	$aData 	= compact('sTitle','oGetDtlRec','oGetRec');
        return view('control_panel.order_module.ListDetailsView',$aData);
	}

	public function UpLdRprt(Request $request)
	{
		$sMime = $request->file('sFileName')->getMimeType();
		if($sMime == 'application/pdf')
		{
			$input = $request['input'];
			$sPic = $request->file('sFileName');
			$sStorePath 	= 'public/user_report/';
	        $sPicName 		= time().'.'.$sPic->getClientOriginalExtension();
	        $sPic->move($sStorePath,$sPicName);

	        $aPicArr = array(
	        	"sFile_Name"	=> $sPicName,
	        	"nOrd_Status"	=> config('constant.ORD_STATUS.COMPLETE'),
	        );
	        $nRow = $this->OrdHd->UpDtRecrd($aPicArr, base64_decode($input['lOrdHdIdNo']));
	        if($nRow > 0)
	        {
	        	$aRprtArr = array(
		        	"lOrd_Hd_IdNo"	=> base64_decode($input['lOrdHdIdNo']),
		        	"sRprt_File"	=> $sPicName,
		        );
	        	$this->Report->InsrtRecrd($aRprtArr);
		        $oGetOrd	= $this->OrdHd->OrdDtl(base64_decode($input['lOrdHdIdNo']));
		        $sServerKey = 'AAAAks8z1dE:APA91bGd7ILdZ2NMMblmhrOTkNVOjtt0rFYoVZpHgIr1OYdD3HYPV3fv8yUVjQSvPoa-b4mMuq-Wnoov6NQF86wy5z2qINmw4FSr5H2mIXa0S0dFODEB_6_3diP1wJ4SJRt_SQ_vVbTn';
		        $sTitle = 'Report uploaded';
				$sBody	= 'Your order ('.$oGetOrd->sOrd_No.') report uploaded, Check';
				Controller::SendNotification($oGetOrd->sDvic_Tokn, $sTitle, $sBody, $sServerKey);
			}
	        return redirect('admin_panel/pending_order')->with('Success', 'Report uploaded successfully...');
	    }
	    else
	    {
	    	return redirect('admin_panel/pending_order')->with('Failed', 'Only pdf file allowed...');
	    }
	}

	public function DtlCntrl(Request $request)
	{
		if(!isset($request['lRecIdNo']) && empty($request['lRecIdNo']))
		{
			return redirect('admin_panel/pending_order')->with('Failed', 'unauthorized access');
		}
		else
		{
			$lOrdHdIdNo = base64_decode($request['lRecIdNo']);
			$oGetOrd 	= $this->OrdHd->OrdDtl($lOrdHdIdNo);
			$oGetDtlRec = $this->OrdDt->OrdDtLst($lOrdHdIdNo);
			$sTitle	= "Manage Order Details";
	    	$aData 	= compact('sTitle','oGetOrd','oGetDtlRec');
	        return view('control_panel.order_module.DetailsView',$aData);
		}
	}
	
	public function CashCntrl()
	{
		$oGetRec = $this->TechOrder->CashClct();
		$sTitle	= "Manage Cash Collection";
    	$aData 	= compact('sTitle','oGetRec');
        return view('control_panel.order_module.CollectionView',$aData);
	}

	public function CnclOrd(Request $request)
	{
		try
		{
			$lOrdHdIdNo = base64_decode($request['lRecIdNo']);
			if(empty($lOrdHdIdNo))
			{
				return redirect()->back()->with('Failed', 'Unauthorized Access...');
			}
			else
			{
				$oGetOrd = $this->OrdHd->OrdDtl($lOrdHdIdNo);
				if(isset($oGetOrd) && !empty($oGetOrd->lOrd_Hd_IdNo))
				{
				    $sRfndId = NULL;
					if($oGetOrd->nOrd_Status == config('constant.ORD_STATUS.ONGOING'))
					{
						if($oGetOrd->nPay_Mod == config('constant.PAY_MTHD.ONLINE') && !empty($oGetOrd->sTrans_Id))
						{
							$api = new Api(config('services.razor.razor_key'), config('services.razor.razor_secret'));
				    		$aPayDtl = $api->payment->fetch($oGetOrd->sTrans_Id);
				    		if($aPayDtl['status'] != 'captured')
				    		{
				    			$payment = $api->payment->fetch($oGetOrd->sTrans_Id)->capture(array('amount'=>$oGetOrd->sTtl_Amo*100, 'currency'=>'INR'));
				    			$refund  = $api->refund->create(array('payment_id' => $oGetOrd->sTrans_Id));
				    			$sRfndId = $refund['id'];
				    		}
				    	}
				    	$aCnclArr = $this->CnclArr($oGetOrd->lOrd_Hd_IdNo, $sRfndId);
				    	$nRow = DB::table('cncl_ord')->insert($aCnclArr);
				    	if(isset($nRow) && $nRow > 0)
				    	{
				    		$this->OrdHd->UpDtRecrd(array('nOrd_Status' => config('constant.ORD_STATUS.CANCELLED')), $oGetOrd->lOrd_Hd_IdNo);
				    	}
				    	return redirect()->back()->with('Success', 'Order cancled successfully...');			
					}
					else
					{
						return redirect()->back()->with('Failed', 'Now you are not able to cancel this order...');			
					}
				}
				else
				{
					return redirect()->back()->with('Failed', 'Unauthorized Access...');		
				}
			}
		}
		catch(\Exception $e)
		{
			return redirect()->back()->with('Failed', $e->getMessage());		
		}
	}

	public function CnclArr($lOrdHdIdNo, $sRfndId)
	{
		$aCmnArr = array(
			"lOrd_Hd_IdNo" => $lOrdHdIdNo,
			"sRfnd_Id"	   => $sRfndId,
		);
		return $aCmnArr;
	}
}
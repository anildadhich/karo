<?php

namespace App\Http\Controllers\ControlPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Administrator;
use App\Model\Feedback;

class FeedbackController extends Controller
{
	public function __construct()
	{
		$this->Feedback = new Feedback;
		//$this->middleware(Administrator::class);
	}

	public function LstCntrl()
	{
		$oGetRec = $this->Feedback->FdbckAll();
		$sTitle	= "Manage Feedback List";
    	$aData 	= compact('sTitle','oGetRec');
        return view('control_panel.feedback_module.ListView',$aData);
	}
}
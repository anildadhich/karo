<?php

namespace App\Model;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use DB;

class Member extends Model
{
    public $timestamps  = false;
    protected $table   = 'mst_membr';

    public function InsrtRecrd($aHdArr)
    {
        try
        {
            $ySaveStatus    = False;
            $nRow           = Member::insert($aHdArr);
            if($nRow > 0)
            {
                $ySaveStatus = True;
            }
            return $ySaveStatus;
        }
        catch (\Exception $e)
        {
            return;
        }
    }

    public function UpDtRecrd($aHdArr, $lMembrIdNo)
    {
        try
        {
            $ySaveStatus    = False;
            $nRow           = Member::Where('lMembr_IdNo',$lMembrIdNo)->update($aHdArr);
            if($nRow >= 0)
            {
                $ySaveStatus = True;
            }
            return $ySaveStatus;
        }
        catch (\Exception $e)
        {
            return;
        }
    }

    public function DelMembr($lMembrIdNo)
    {
        try
        {
            $yDelStatus = False;
            $aValues = array(
                "nDel_Status"   => config('constant.DEL_STATUS.DELETED'),
            );
            $nRow = Member::Where('lMembr_IdNo',$lMembrIdNo)->update($aValues);
            if($nRow)
            {
                $yDelStatus = True;
            }
            return $yDelStatus;
        }
        catch (\Exception $e)
        {
            return;
        }
    }

    public function GetMembr($lMembrIdNo)
    {
        try
        {
            $oGetMembr = Member::Where('lMembr_IdNo',$lMembrIdNo)->first();
            return $oGetMembr;
        }
        catch (\Exception $e)
        {
            return;
        }
    }

    public function MembrList($lUserIdNo)
    {
        try
        {
            $oGetMembr = Member::Where('lUser_IdNo',$lUserIdNo)->Where('nDel_Status',config('constant.DEL_STATUS.NON_DELETED'))->get();
            return $oGetMembr;
        }
        catch (\Exception $e)
        {
            return;
        }
    }
}
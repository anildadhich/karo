<?php

namespace App\Model;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use DB;

class Parameter extends Model
{
    public $timestamps  = false;
    protected $table    = 'tst_prmtr';

    public function InsrtRecrd($aDtArr)
    {
        $ySaveStatus    = False;
        $nRow       = Parameter::insert($aDtArr);
        if($nRow > 0)
        {
            $ySaveStatus = True;
        }
        return $ySaveStatus;
    }

    public function PrmtrLst($lTstIdNo)
    {
        try
        {
            $oGetPrmtr = Parameter::Where('lTst_IdNo',$lTstIdNo)->get();
            return $oGetPrmtr;
        }
        catch (\Exception $e)
        {
            return;
        }
    }

    public function PrmtDel($lTstIdNo)
    {
        try
        {
            $oGetPrmtr = Parameter::Where('lTst_IdNo',$lTstIdNo)->delete();
            return;
        }
        catch (\Exception $e)
        {
            return;
        }   
    }
}

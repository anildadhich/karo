<?php

namespace App\Model;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use DB;

class OrdDt extends Model
{
    public $timestamps  = false;
    protected $table    = 'ord_dt';

    public function InsrtRecrd($aDtArr)
    {
        try
        {
            $ySaveStatus    = False;
            $lOrdDtIdNo     = OrdDt::insert($aDtArr);
            if(!empty($lOrdDtIdNo))
            {
                $ySaveStatus = True;
            }
            return $ySaveStatus;
        }
        catch (\Exception $e)
        {
            return;
        }
    }
    
    public function OrdDtLst($lOrdHdIdNo)
    {
        try
        {
            $oGetOrdDt = OrdDt::Where('lOrd_Hd_IdNo',$lOrdHdIdNo)->get();
            return $oGetOrdDt;
        }
        catch (\Exception $e)
        {
            return;
        }
    }

    public function OrdDtl($lOrdHdIdNo)
    {
        try
        {
            $oGetOrd = OrdDt::Where('lOrd_Hd_IdNo',$lOrdHdIdNo)->get();
            return $oGetOrd;
        }
        catch (\Exception $e)
        {
            return;
        }
    }
}

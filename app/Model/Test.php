<?php

namespace App\Model;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use DB;

class Test extends Model
{
    public $timestamps  = false;
    protected $table = 'mst_test';

    public function InsrtRecrd($aHdArr, &$lTstIdNo)
    {
        $ySaveStatus    = False;
        $lTstIdNo       = Test::insertGetId($aHdArr);
        if(!empty($lTstIdNo))
        {
            $ySaveStatus = True;
        }
        return $ySaveStatus;
    }

    public function UpDtRecrd($aHdArr, $lTstIdNo)
    {
        $ySaveStatus    = False;
        $nRow           = Test::Where('lTst_IdNo',$lTstIdNo)->update($aHdArr);
        if($nRow >= 0)
        {
            $ySaveStatus = True;
        }
        return $ySaveStatus;
    }

    public function TstDtl($lTstIdNo)
    {
    	try
    	{
    		$oGetTest = Test::Where('nDel_Status',config('constant.DEL_STATUS.NON_DELETED'))->where('lTst_IdNo', $lTstIdNo)->first();
    		return $oGetTest;
    	}
    	catch (\Exception $e)
    	{
    		return;
    	}
    }

    public function TstLst($nBlkUnBlk = '', $sTstCatg = '')
    {
        try
        {
            $oGetTest = Test::Where('nDel_Status',config('constant.DEL_STATUS.NON_DELETED'))
                        ->where(function($query) use ($sTstCatg) {
                            if (isset($sTstCatg) && !empty($sTstCatg)) {
                                $query->where('nTst_Catg',$sTstCatg);
                            }
                        })
                        ->where(function($query) use ($nBlkUnBlk) {
                            if (isset($nBlkUnBlk) && !empty($nBlkUnBlk)) {
                                $query->where('nBlk_UnBlk',$nBlkUnBlk);
                            }
                        })->OrderBy('sTst_Name','ASC')->get();
            return $oGetTest;
        }
        catch (\Exception $e)
        {
            return;
        }
    }

    public function SrchTst()
    {
        try
        {
            $oGetTest = Test::Where('nBlk_UnBlk', config('constant.STATUS.UNBLOCK'))->Where('nDel_Status',config('constant.DEL_STATUS.NON_DELETED'))->OrderBy('sTst_Name','ASC')->get();
            return $oGetTest;
        }
        catch (\Exception $e)
        {
            return;
        }   
    }

    public function TstAll($sTstName = '')
    {
        try
        {
            $oGetTest = Test::Where('nDel_Status',config('constant.DEL_STATUS.NON_DELETED'))
                        ->where(function($query) use ($sTstName) {
                            if (isset($sTstName) && !empty($sTstName)) {
                                $query->where('sTst_Name','LIKE', "%".$sTstName."%");
                            }
                        })->OrderBy('sTst_Name','ASC')->paginate(15);
            return $oGetTest;
        }
        catch (\Exception $e)
        {
            return;
        }
    }
}

<?php

namespace App\Model;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use DB;

class State extends Model
{
    protected $table = 'mst_state';

    public function GetState()
    {
    	try
    	{
    		$oGetState = State::Where('nBlk_UnBlk',config('constant.STATUS.UNBLOCK'))->Where('nDel_Status',config('constant.DEL_STATUS.NON_DELETED'))->get();
    		return $oGetState;
    	}
    	catch (\Exception $e)
    	{
    		return;
    	}
    }
}

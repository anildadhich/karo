<?php

namespace App\Model;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use DB;

class Feedback extends Model
{
    public $timestamps  = false;
    protected $table    = 'user_fdbck';

    public function InsrtRecrd($aHdArr)
    {
        $ySaveStatus    = False;
        $nRow           = Feedback::insert($aHdArr);
        if($nRow > 0)
        {
            $ySaveStatus = True;
        }
        return $ySaveStatus;
    }

    public function FdbckAll($nBlkUnBlk = '')
    {
        try
        {
            $oGetFdbck = Feedback::leftjoin('mst_user', 'mst_user.lUser_IdNo', '=', 'user_fdbck.lUser_IdNo')
                        ->where(function($query) use ($nBlkUnBlk) {
                            if (isset($nBlkUnBlk) && !empty($nBlkUnBlk)) {
                                $query->where('nBlk_UnBlk',$nBlkUnBlk);
                            }
                        })
                        ->OrderBy('user_fdbck.sCrt_DtTm','DESC')->get();
            return $oGetFdbck;
        }
        catch (\Exception $e)
        {
            return;
        }
    }    
}

<?php

namespace App\Model;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use DB;

class FeedbackTech extends Model
{
    public $timestamps  = false;
    protected $table    = 'tech_fdbck';

    public function InsrtRecrd($aHdArr)
    {
        $nRow           = FeedbackTech::insert($aHdArr);
        return $nRow;
    }
}

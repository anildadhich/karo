<?php

namespace App\Model;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use DB;

class Company extends Model
{
    public $timestamps  = false;
    protected $table    = 'mst_comp';

    public function CompRec()
    {
        $oGetComp = Company::Where('lComp_IdNo',1)->first();
        return $oGetComp;
    }

    public function CompLogin($sCompEmail, $sCompPass, &$lCompIdNo)
    {
    	$yLgnStatus = False;
    	$oGetComp = Company::Select('lComp_IdNo')->Where('sComp_Email',$sCompEmail)->Where('sComp_Pass',md5($sCompPass))->first();
    	if(isset($oGetComp) && !empty($oGetComp->lComp_IdNo))
    	{
    		$yLgnStatus = True;
    		$lCompIdNo  = $oGetComp->lComp_IdNo; 
    	}
    	return $yLgnStatus;
    }
    
    public function UpDtRecrd($aHdArr)
    {
        $ySaveStatus    = False;
        $nRow           = Company::Where('lComp_IdNo',1)->update($aHdArr);
        if($nRow > 0)
        {
            $ySaveStatus = True;
        }
        return $ySaveStatus;
    }
}

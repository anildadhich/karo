<?php

namespace App\Model;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use DB;

class Page extends Model
{
    public $timestamps  = false;
    protected $table    = 'page_dlt';

    public function PageDtl($lPageIdNo)
    {
        try
        {
            $oGetPage = Page::Where('lPage_IdNo',$lPageIdNo)->first();
            return $oGetPage;
        }
        catch (\Exception $e)
        {
            return;
        }
    }
}

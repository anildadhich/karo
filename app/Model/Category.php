<?php

namespace App\Model;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use DB;

class Category extends Model
{
    public $timestamps  = false;
    protected $table    = 'mst_catg';

    public function InsrtRecrd($aHdArr)
    {
        $ySaveStatus    = False;
        $nRow           = Category::insert($aHdArr);
        if($nRow > 0)
        {
            $ySaveStatus = True;
        }
        return $ySaveStatus;
    }

    public function UpDtRecrd($aHdArr, $lCatgIdNo)
    {
        $ySaveStatus    = False;
        $nRow           = Category::Where('lCatg_IdNo',$lCatgIdNo)->update($aHdArr);
        if($nRow >= 0)
        {
            $ySaveStatus = True;
        }
        return $ySaveStatus;
    }

    public function CatgList($nBlkUnBlk = '')
    {
    	try
    	{
    		$oGetCatg = Category::Where('nDel_Status',config('constant.DEL_STATUS.NON_DELETED'))
                        ->where(function($query) use ($nBlkUnBlk) {
                            if (isset($nBlkUnBlk) && !empty($nBlkUnBlk)) {
                                $query->where('nBlk_UnBlk',$nBlkUnBlk);
                            }
                        })
                        ->get();
    		return $oGetCatg;
    	}
    	catch (\Exception $e)
    	{
    		return;
    	}
    }

    public function CatgDtl($lCatgIdNo)
    {
        try
        {
            $oGetCatg = Category::Where('lCatg_IdNo',$lCatgIdNo)->Where('nDel_Status',config('constant.DEL_STATUS.NON_DELETED'))->first();
            return $oGetCatg;
        }
        catch (\Exception $e)
        {
            return;
        }
    }
}

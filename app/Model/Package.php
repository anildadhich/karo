<?php

namespace App\Model;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use DB;

class Package extends Model
{
    public $timestamps  = false;
    protected $table = 'mst_pckg';

    public function InsrtRecrd($aHdArr)
    {
        $ySaveStatus    = False;
        $nRow           = Package::insert($aHdArr);
        if($nRow > 0)
        {
            $ySaveStatus = True;
        }
        return $ySaveStatus;
    }

    public function UpDtRecrd($aHdArr, $lPckgIdNo)
    {
        $ySaveStatus    = False;
        $nRow           = Package::Where('lPckg_IdNo',$lPckgIdNo)->update($aHdArr);
        if($nRow >= 0)
        {
            $ySaveStatus = True;
        }
        return $ySaveStatus;
    }

    public function PckgList($nBlkUnBlk = '')
    {
    	try
    	{
    		$oGetPckg = Package::Where('nDel_Status',config('constant.DEL_STATUS.NON_DELETED'))
                        ->where(function($query) use ($nBlkUnBlk) {
                            if (isset($nBlkUnBlk) && !empty($nBlkUnBlk)) {
                                $query->where('nBlk_UnBlk',$nBlkUnBlk);
                            }
                        })->OrderBy('sPckg_Name')->get();
    		return $oGetPckg;
    	}
    	catch (\Exception $e)
    	{
    		return;
    	}
    }

    public function PckgDtl($lPckgIdNo)
    {
        try
        {
            $oGetPckg = Package::Where('lPckg_IdNo',$lPckgIdNo)->Where('nDel_Status',config('constant.DEL_STATUS.NON_DELETED'))->first();
            return $oGetPckg;
        }
        catch (\Exception $e)
        {
            return;
        }   
    }

    public function PckgAll()
    {
        try
        {
            $oGetPckg = Package::Where('nDel_Status',config('constant.DEL_STATUS.NON_DELETED'))->OrderBy('sPckg_Name')->get();
            return $oGetPckg;
        }
        catch (\Exception $e)
        {
            return;
        }
    }
}

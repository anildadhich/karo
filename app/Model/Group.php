<?php

namespace App\Model;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use DB;

class Group extends Model
{
    public $timestamps  = false;
    protected $table = 'mst_group';

    public function HdArr($input, $request)
    {
        $aComnArr   = array(
            "sGrp_Name"     => $input['sGrpName'],
            "sGrp_Dtl"      => $input['sGrpDtl'],
            "sTst_Ids"      => implode(",",$request['sTstIds']),
        );
        return $aComnArr;
    }

    public function InsArr(&$aHdArr)
    {
        $aHdArr['nBlk_UnBlk']   = config('constant.STATUS.UNBLOCK');
        $aHdArr['nDel_Status']  = config('constant.DEL_STATUS.NON_DELETED');
    }

    public function SaveRec($request)
    {
        $ySaveStatus    = False;
        $input          = $request['input'];
        $lGrpIdNo       = base64_decode($input['lGrpIdNo']);
        $aHdArr         = $this->HdArr($input, $request);
        if($lGrpIdNo == 0)
        {
            $this->InsArr($aHdArr);
            Group::insertGetId($aHdArr);
            $ySaveStatus = True;
        }
        else
        {
            Group::Where('lGrp_IdNo',$lGrpIdNo)->update($aHdArr);   
            $ySaveStatus = True;
        }
        return $ySaveStatus;
    }

    public function GroupDtl($lGrpIdNo)
    {
    	try
    	{
    		$oGetGrp = Group::Where('lGrp_IdNo', $lGrpIdNo)->Where('nBlk_UnBlk',config('constant.STATUS.UNBLOCK'))->Where('nDel_Status',config('constant.DEL_STATUS.NON_DELETED'))->first();
    		return $oGetGrp;
    	}
    	catch (\Exception $e)
    	{
    		return;
    	}
    }

    public function GroupAll()
    {
        try
        {
            $oGetGrp = Group::Where('nBlk_UnBlk',config('constant.STATUS.UNBLOCK'))->Where('nDel_Status',config('constant.DEL_STATUS.NON_DELETED'))->get();
            return $oGetGrp;
        }
        catch (\Exception $e)
        {
            return;
        }
    }
}

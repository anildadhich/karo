<?php

namespace App\Model;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use DB;

class City extends Model
{
    protected $table = 'mst_city';

    public function GetCity($lStateIdNo)
    {
    	try
    	{
    		$oGetCity = City::Where('lState_IdNo',$lStateIdNo)->Where('nBlk_UnBlk',config('constant.STATUS.UNBLOCK'))->Where('nDel_Status',config('constant.DEL_STATUS.NON_DELETED'))->get();
    		return $oGetCity;
    	}
    	catch (\Exception $e)
    	{
    		return;
    	}
    }
}

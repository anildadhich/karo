<?php

namespace App\Model;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use DB;

class Prescription extends Model
{
    protected $table    = 'mst_prscrpt';

    public function InsrtRecrd($aHdArr)
    {
        $ySaveStatus    = False;
        $nRow           = Prescription::insert($aHdArr);
        if($nRow > 0)
        {
            $ySaveStatus = True;
        }
        return $ySaveStatus;
    }

    public function PresLst($lUserIdNo)
    {
        try
        {
            $oGetPrp = Prescription::Where('lUser_IdNo',$lUserIdNo)->OrderBy('sCrt_DtTm','DESC')->get();
            return $oGetPrp;
        }
        catch (\Exception $e)
        {
            return;
        }
    }

    public function PresAll()
    {
        try
        {
            $oGetPrp = Prescription::leftjoin('mst_user', 'mst_user.lUser_IdNo', '=', 'mst_prscrpt.lUser_IdNo')->OrderBy('mst_prscrpt.sCrt_DtTm','DESC')->get();
            return $oGetPrp;
        }
        catch (\Exception $e)
        {
            return;
        }
    }
}

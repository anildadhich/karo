<?php

namespace App\Model;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use DB;

class User extends Model
{
    public $timestamps  = false;
    protected $table    = 'mst_user';

    public function ChkUser($request)
    {
    	$oGetUser = User::Select('lUser_IdNo')->Where('sUser_Mobile', $request['sUserMobile'])->first();
        return $oGetUser;
    }

    public function IsRefExst($sRefCode, &$lUserIdNo)
    {
        try
        {
            $yRefStatus = False;
            $oGetRef    = User::Select('lUser_IdNo')->Where('sRef_Code',$sRefCode)->first();
            if(isset($oGetRef) && !empty($oGetRef->lUser_IdNo))
            {
                $yRefStatus = True;
                $lUserIdNo  = $oGetRef->lUser_IdNo;
            }
            return $yRefStatus;
        }
        catch (\Exception $e)
        {
            return;
        }
    }

    public function IsEmailExst($sUserEmail, $lUserIdNo)
    {
        try
        {
            $yEmailStatus = False;
            $oGetUser = DB::table('mst_user')->Select('lUser_IdNo')->Where('sUser_Email', $sUserEmail)->Where('lUser_IdNo','!=',$lUserIdNo)->first();
            if(isset($oGetUser) && !empty($oGetUser->lUser_IdNo))
            {
                $yEmailStatus = True;
            }
            return $yEmailStatus;
        }
        catch (\Exception $e)
        {
            return;
        }
    }

    public function IsMobileExst($sUserMobile, $lUserIdNo)
    {
        try
        {
            $yMobileStatus = False;
            $oGetUser = DB::table('mst_user')->Select('lUser_IdNo')->Where('sUser_Mobile', $sUserMobile)->Where('lUser_IdNo','!=',$lUserIdNo)->first();
            if(isset($oGetUser) && !empty($oGetUser->lUser_IdNo))
            {
                $yMobileStatus = True;
            }
            return $yMobileStatus;
        }
        catch (\Exception $e)
        {
            return;
        }
    }

    public function InsrtRecrd($aHdArr, &$lUserIdNo)
    {
        try
        {
            $ySaveStatus    = False;
            $lUserIdNo      = User::insertGetId($aHdArr);
            if(!empty($lUserIdNo))
            {
                $ySaveStatus = True;
            }
            return $ySaveStatus;
        }
        catch (\Exception $e)
        {
            return;
        }
    }

    public function UpDtRecrd($aHdArr, $lUserIdNo)
    {
        try
        {
            $ySaveStatus    = False;
            $nRow           = User::Where('lUser_IdNo',$lUserIdNo)->update($aHdArr);
            if($nRow >= 0)
            {
                $ySaveStatus = True;
            }
            return $ySaveStatus;
        }
        catch (\Exception $e)
        {
            return;
        }
    }

    public function InsrtRefrl($aRefArr)
    {
        try
        {
            DB::table('refrl_user')->insert($aRefArr);
            return;
        }
        catch (\Exception $e)
        {
            return;
        }
    }

    public function UpDtPic($sPicName, $lUserIdNo)
    {
        $aValue = array(
            "sUser_Pic" => $sPicName,
        );
        return User::Where('lUser_IdNo',$lUserIdNo)->update($aValue);
    }

    public function Profile($lUserIdNo)
    {
        try
        {
            $oGetUser = User::Where('lUser_IdNo',$lUserIdNo)->first();

            $sUserPic = empty($oGetUser->sUser_Pic) ? 'default.png' : $oGetUser->sUser_Pic;
            $aProfileData = array(
                "sUserName"     => $oGetUser->sUser_Name, 
                "sUserEmail"    => $oGetUser->sUser_Email, 
                "sUserMobile"   => $oGetUser->sUser_Mobile, 
                "nUserGndr"     => $oGetUser->nUser_Gndr, 
                "sUserPic"      => config('constant.PUBLIC_URL').'/profile_pic/'.$sUserPic, 
                "sUserDob"      => $oGetUser->sUser_Dob,
                "sRefCode"      => $oGetUser->sRef_Code,
                "sAdrsLine"     => $oGetUser->sAdrs_Line,
                "sAdrsArea"     => $oGetUser->sAdrs_Area,
                "sCityName"     => $oGetUser->sCity_Name,
                "sStateName"    => $oGetUser->sState_Name,
                "sPinCod"       => $oGetUser->sPin_Cod,
            );
            return $aProfileData;
        }
        catch (\Exception $e)
        {
            return;
        }
    }

    public function UserPic($lUserIdNo)
    {
        try
        {
            $oGetUser = User::Select('sUser_Pic')->Where('mst_user.lUser_IdNo',$lUserIdNo)->first();
            return $oGetUser;
        }
        catch (\Exception $e)
        {
            return;
        }
    }

    public function GetUser($lUserIdNo)
    {
        $oGetUser = User::Where('lUser_IdNo',$lUserIdNo)->first();
        return $oGetUser;
    }

    public function GetWalt($lUserIdNo)
    {
        try
        {
            $oGetWalt = DB::Select("Select (Select IFNULL(SUM(sRef_Amo), 0) From refrl_user Where lRef_By_IdNo $lUserIdNo) As TtlWallet, (Select IFNULL(SUM(sWlt_Amo), 0) From ord_hd Where lUser_IdNo = $lUserIdNo) As TtlRedm");
            return $oGetWalt;
        }
        catch (\Exception $e)
        {
            return;
        }
    }

    public function UserLst()
    {
        try
        {
            $oGetUser = User::OrderBy('sUser_Name')->get();
            return $oGetUser;
        }
        catch (\Exception $e)
        {
            return;
        }
    } 
}

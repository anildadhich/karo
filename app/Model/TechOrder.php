<?php

namespace App\Model;

use Illuminate\Http\aOrdData;
use Illuminate\Database\Eloquent\Model;
use DB;
use Log;

class TechOrder extends Model
{
    public $timestamps  = false;
    protected $table    = 'tech_order';

    public function InsrtRecrd($aHdArr)
    {
        try
        {
            $nRow = TechOrder::insertGetId($aHdArr);
            return $nRow;
        }
        catch(\Exception $e)
        {
            return;
        }   
    }

    public function PndgOrd($lTechIdNo)
    {
        try
        {
            $oGetOrd = TechOrder::Select('tech_order.sCmnt_Dtl','tech_order.sCrt_DtTm','tech_order.lOrd_Hd_IdNo','ord_hd.sUser_Name','ord_hd.sUser_Adrs','ord_hd.sOrd_No')->Where('lTech_IdNo',$lTechIdNo)->leftjoin('ord_hd', 'ord_hd.lOrd_Hd_IdNo', '=', 'tech_order.lOrd_Hd_IdNo')->Where('nCurr_Status',config('constant.TECH_ORD_STATUS.PENDING'))->OrderBy('tech_order.sCrt_DtTm','DESC')->get();
            return $oGetOrd;
        }
        catch (\Exception $e)
        {
            return;
        }
    }

    public function UpDtOrd($aArr, $lOrdHdIdNo, $lTechIdNo)
    {
        try
        {
            $nUpDtRaw = TechOrder::Where('lOrd_Hd_IdNo',$lOrdHdIdNo)->Where('lTech_IdNo',$lTechIdNo)->update($aArr);
            return $nUpDtRaw;
        }
        catch (\Exception $e)
        {
            return;
        }
    }

    public function OngoigOrd($lTechIdNo)
    {
        try
        {
            $oGetOrd = TechOrder::Select('tech_order.sCmnt_Dtl','tech_order.nCurr_Status','tech_order.sCmnt_Dtl','tech_order.sCrt_DtTm','tech_order.lOrd_Hd_IdNo', 'ord_hd.sOrd_No', 'ord_hd.sUser_Name','ord_hd.sUser_Adrs')->Where('lTech_IdNo',$lTechIdNo)->leftjoin('ord_hd', 'ord_hd.lOrd_Hd_IdNo', '=', 'tech_order.lOrd_Hd_IdNo')->Where('nCurr_Status',config('constant.TECH_ORD_STATUS.ACCEPTED'))->OrWhere('nCurr_Status',config('constant.TECH_ORD_STATUS.COLLECTED'))->OrderBy('tech_order.sCrt_DtTm','DESC')->get();
            return $oGetOrd;
        }
        catch (\Exception $e)
        {
            return;
        }
    }

    public function DlvrdOrd($lTechIdNo)
    {
        try
        {
            $oGetOrd = TechOrder::Select('tech_order.sCmnt_Dtl','tech_order.sCmnt_Dtl','tech_order.sCrt_DtTm','tech_order.lOrd_Hd_IdNo', 'ord_hd.sOrd_No', 'ord_hd.sUser_Name','ord_hd.sUser_Adrs')->Where('lTech_IdNo',$lTechIdNo)->leftjoin('ord_hd', 'ord_hd.lOrd_Hd_IdNo', '=', 'tech_order.lOrd_Hd_IdNo')->Where('nCurr_Status',config('constant.TECH_ORD_STATUS.DELIVERED'))->OrderBy('tech_order.sCrt_DtTm','DESC')->get();
            return $oGetOrd;
        }
        catch (\Exception $e)
        {
            return;
        }
    }

    public function OrdDtl($lOrdHdIdNo, $lTechIdNo)
    {
        try
        {
            $oGetOrd = TechOrder::leftjoin('ord_hd', 'tech_order.lOrd_Hd_IdNo', '=', 'ord_hd.lOrd_Hd_IdNo')->Where('tech_order.lOrd_Hd_IdNo',$lOrdHdIdNo)->Where('lTech_IdNo', $lTechIdNo)->OrderBy('tech_order.sCrt_DtTm', 'DESC')->first();
            return $oGetOrd;
        }
        catch (\Exception $e)
        {
            return;
        }
    }

    public function TechOrdDtl($lOrdHdIdNo)
    {
        try
        {
            $oGetOrd = TechOrder::Where('lOrd_Hd_IdNo',$lOrdHdIdNo)->OrderBy('sCrt_DtTm','DESC')->first();
            return $oGetOrd;
        }
        catch(\Exception $e)
        {
            return;
        }
    }
    
    public function CashClct()
    {
        try
        {
            $oGetOrd = TechOrder::Select('ord_hd.sOrd_No','ord_hd.sTtl_Amo','tech_order.sClct_Amo','sClct_DtTm','tech_mst.sTech_Name')->leftjoin('ord_hd','ord_hd.lOrd_Hd_IdNo','=','tech_order.lOrd_Hd_IdNo')->leftjoin('tech_mst','tech_mst.lTech_IdNo','=','tech_order.lTech_IdNo')->Where('nCurr_Status',config('constant.TECH_ORD_STATUS.COLLECTED'))->OrWhere('nCurr_Status',config('constant.TECH_ORD_STATUS.DELIVERED'))->Where('nPay_Mod',config('constant.PAY_MTHD.CASH'))->OrderBy('sClct_DtTm','DESC')->get();
            return $oGetOrd;
        }
        catch(\Exception $e)
        {
            return;
        }   
    }
}

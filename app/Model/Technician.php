<?php

namespace App\Model;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use DB;

class Technician extends Model
{
    public $timestamps  = false;
    protected $table    = 'tech_mst';

    public function ChkUser($request)
    {
        try
        {
        	$oGetTech = Technician::Select('lTech_IdNo')->Where('sTech_Mobile', $request['sTechMobile'])->first();
            return $oGetTech;
        }
        catch(\Exception $e)
        {
            return;
        }
    } 

    public function Profile($lTechIdNo)
    {
        try
        {
            $oGetTech = Technician::Where('lTech_IdNo', $lTechIdNo)->first();
            return $oGetTech;   
        }
        catch(\Exception $e)
        {
            return;
        }
    }

    public function PicData($lTechIdNo)
    {
        try
        {
            $oGetPic = Technician::Select('sTech_Pic','sProf_Frnt','sProf_Bck')->Where('lTech_IdNo', $lTechIdNo)->first();
            return $oGetPic;   
        }
        catch(\Exception $e)
        {
            return;
        }
    }

    public function InsrtRecrd($aHdArr)
    {
        try
        {
            $lTechIdNo = Technician::insertGetId($aHdArr);
            return $lTechIdNo;
        }
        catch(\Exception $e)
        {
            return;
        }   
    }

    public function UpDtRecrd($aHdArr, $lTechIdNo)
    {
        try
        {
            $nUoDtRaw = Technician::Where('lTech_IdNo', $lTechIdNo)->update($aHdArr);
            return $nUoDtRaw;
        }
        catch(\Exception $e)
        {
            return;
        }  
    }
    public function TechList()
    {
        try
        {
            $oGetTech = Technician::Where('nDel_Status',config('constant.DEL_STATUS.NON_DELETED'))->OrderBy('sTech_Name')->get();
            return $oGetTech;
        }
        catch (\Exception $e)
        {
            return;
        }
    }

    public function ActvTech()
    {
        try
        {
            $aGetTech = Technician::Select('lTech_IdNo','sTech_Name')->Where('nTech_Status',config('constant.TECH_STATUS.ONLINE'))->Where('nBlk_UnBlk',config('constant.STATUS.UNBLOCK'))->Where('nDel_Status',config('constant.DEL_STATUS.NON_DELETED'))->WhereNotNull('sDvic_Tokn')->get();;
            return $aGetTech;
        }
        catch(\Exception $e)
        {
            return;
        }  
    }
}

<?php

namespace App\Model;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use DB;

class Report extends Model
{
    public $timestamps  = false;
    protected $table    = 'user_rprt';

    public function InsrtRecrd($aHdArr)
    {
        try
        {
            $ySaveStatus    = False;
            $lOrdHdIdNo      = Report::insertGetId($aHdArr);
            if(!empty($lOrdHdIdNo))
            {
                $ySaveStatus = True;
            }
            return $ySaveStatus;
        }
        catch (\Exception $e)
        {
            return;
        }
    }

    public function RptLst($lUserIdNo)
    {
        try
        {
            $oGetRpt = Report::Select('sOrd_No','ord_hd.lOrd_Hd_IdNo','ord_hd.sUser_Name','sRprt_File','user_rprt.sCrt_DtTm')->leftjoin('ord_hd', 'ord_hd.lOrd_Hd_IdNo', '=', 'user_rprt.lOrd_Hd_IdNo')->Where('lUser_IdNo',$lUserIdNo)->get();
            return $oGetRpt;
        }
        catch (\Exception $e)
        {
            return;
        }
    }
}

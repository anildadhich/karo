<?php

namespace App\Model;

use Illuminate\Http\aOrdData;
use Illuminate\Database\Eloquent\Model;
use DB;

class OrdHd extends Model
{
    public $timestamps  = false;
    protected $table    = 'ord_hd';

    public function InsrtRecrd($aHdArr, &$lOrdHdIdNo)
    {
        try
        {
            $ySaveStatus    = False;
            $lOrdHdIdNo      = OrdHd::insertGetId($aHdArr);
            if(!empty($lUserIdNo))
            {
                $ySaveStatus = True;
            }
            return $ySaveStatus;
        }
        catch (\Exception $e)
        {
            return;
        }
    }

    public function UpDtRecrd($aHdArr, $lOrdHdIdNo)
    {
        try
        {
            $nRow   = OrdHd::Where('lOrd_Hd_IdNo',$lOrdHdIdNo)->update($aHdArr);
            return $nRow;
        }
        catch (\Exception $e)
        {
            return;
        }
    }

    public function OrdLst($lUserIdNo = '', $nOrdStatus = '')
    {
        try
        {
            $oGetOrd = OrdHd::
                            where(function($query) use ($lUserIdNo) {
                                if (isset($lUserIdNo) && !empty($lUserIdNo)) {
                                    $query->where('lUser_IdNo',$lUserIdNo);
                                }
                            })
                            ->where(function($query) use ($nOrdStatus) {
                                if (isset($nOrdStatus) && !empty($nOrdStatus)) {
                                    $query->where('nOrd_Status',$nOrdStatus);
                                }
                            })
                            ->OrderBy('sCrt_DtTm','DESC')->get();
            return $oGetOrd;
        }
        catch (\Exception $e)
        {
            return;
        }
    }

    public function OrdDtl($lOrdHdIdNo)
    {
        try
        {
            $oGetOrd = OrdHd::Select('ord_hd.*','mst_user.sDvic_Tokn')->leftjoin('mst_user', 'mst_user.lUser_IdNo', '=', 'ord_hd.lUser_IdNo')->Where('lOrd_Hd_IdNo',$lOrdHdIdNo)->first();
            return $oGetOrd;
        }
        catch (\Exception $e)
        {
            return;
        }
    }

    public function OrdHdAll()
    {
        try
        {
            $oGetOrd = OrdHd::get();
            return $oGetOrd;
        }
        catch (\Exception $e)
        {
            return;
        }
    }

    public function CntOrd($nOrdStatus)
    {
        try
        {
            $nCntOrd = OrdHd::Where('nOrd_Status',$nOrdStatus)->get();
            return $nCntOrd;
        }
        catch (\Exception $e)
        {
            return;
        }       
    }
}

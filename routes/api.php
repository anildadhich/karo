<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace('ApiController')->group(function () {
	Route::post('home/data', 'HomeController@HomeRec');

	Route::post('user/login', 'UserController@UserLogin');
	Route::post('user/logout', 'UserController@UserLogout');
	Route::post('user/refrel/verify', 'UserController@VrfyRefrl');
	Route::post('user/save', 'UserController@SaveCntrl');
	Route::post('user/profile', 'UserController@UserProfile');
	Route::post('user/profile/update', 'UserController@UpDtProf');
	Route::post('user/picture/update', 'UserController@UpDtPic');
	Route::post('user/wallet', 'UserController@UserWalt');
	Route::post('user/resent_otp', 'UserController@RsndOtp');

	Route::get('relation/list', 'MemberController@RelList');
	Route::post('member/save', 'MemberController@SaveCntrl');
	Route::post('member/delete', 'MemberController@DelMembr');
	Route::post('member/detail', 'MemberController@GetMembr');
	Route::post('member/list', 'MemberController@MembrList');

	Route::get('state/list', 'LocationController@StateList');
	Route::post('city/list', 'LocationController@CityList');

	Route::get('category/list', 'CategoryController@CatgList');
	Route::post('category/test/list', 'CategoryController@TstList');


	Route::get('package/list', 'PackageController@PckgList');
	Route::post('package/detail', 'PackageController@PckgDlt');

	Route::get('popular/test/list', 'TestController@PopTstList');
	Route::get('test/search', 'TestController@SrchTst');
	Route::post('test/parameter', 'TestController@TstPrmtr');

	Route::post('prescription/save', 'PrescriptionController@SaveCntrl');
	Route::post('prescription/list', 'PrescriptionController@PresLst');

	Route::post('feedback/save', 'FeedbackController@SaveCntrl');
	Route::get('feedback/list', 'FeedbackController@FdbckLst');

	Route::get('company/record', 'CompanyController@CompRec');

	Route::post('order/save', 'OrderController@SaveCntrl');
	Route::post('order/list', 'OrderController@OrdLst');
	Route::post('order/history', 'OrderController@OrdHstry');

	Route::post('report/list', 'OrderController@RprtLst');
	Route::get('page/policy', 'PageController@PageDtl');
});


Route::namespace('TechnicianController')->group(function () {
	Route::post('technician/login', 'TechController@TechLogin');
	Route::post('technician/logout', 'TechController@TechLogout');
	Route::post('technician/profile', 'TechController@TechProfile');
	Route::post('technician/update/status', 'TechController@UpDtStatus');
	Route::post('technician/resent_otp', 'TechController@RsndOtp');

	Route::post('pending/order', 'OrderController@PndgOrd');
	Route::post('listing/order', 'OrderController@OrdLst');
	Route::post('action/order', 'OrderController@ActnOrd');
	Route::post('detail/order', 'OrderController@OrdDtl');

	Route::get('technician/policy', 'PageController@PageDtl');

	Route::post('technician/feedback/save', 'FeedbackController@SaveCntrl');
});